#!/bin/bash -e

usage() {
	cat << EOF

$0 -e|--env @yourenv -m|--method wp|eb -v|--verbose

EOF
}

# process args
options=$(getopt -o e:m:s:v --long env: --long method: --long verbose --long src-dir: -- "$@")
test $? -eq 0  || {
		usage
		exit 1
}
eval set -- "$options"
VERBOSE=0
METHOD=wp
ENVIRONMENT=@development
DEPLOY_SRC_DIR=./public
while true; do
		case "$1" in
		-v|--verbose)
				VERBOSE=1
				;;
		-e|--env)
				shift;
				ENVIRONMENT=$1
				;;
		-m|--method)
				shift;
				METHOD=$1
				;;
		-s|--src-dir)
				shift;
				DEPLOY_SRC_DIR=$1
				;;				
		--)
				shift
				break
				;;
		esac
		shift
done

set +e
grep "^$ENVIRONMENT:" wp-cli.yml > /dev/null
has_environment=$?
set -e

if test ! $has_environment -eq 0
then
	echo "Can't find $ENVIRONMENT in wp-cli.yml. Make sure you set it up."
	exit 2
fi

if test ! -d $DEPLOY_SRC_DIR
then
	echo "Can't find source deployment $DEPLOY_SRC_DIR"
	exit 3
fi

# Deploy using elastic beanstalk (WIP)
deploy_eb() {
	echo 'deploy EB is not implemented (yet)'
	exit 4
}

# deploy using wp-cli
#  usage:
#  deploy_wp @env
deploy_wp() {
	wp --allow-root sync local $ENVIRONMENT
}

DEPLOY_FUNCTION="deploy_${METHOD}"

if type "$DEPLOY_FUNCTION" &> /dev/null
then 
	echo "Deploying to '$ENVIRONMENT' using '$METHOD'"
else 
	echo "Unable to deploy using specified method. Available methods are 'wp' and 'eb'"
	exit 2
fi

$DEPLOY_FUNCTION
