#!/bin/bash -eux

n=0
until [ $n -ge 5 ]
do
	curl -sL 'http://local.leafcutter.com.au' && break
	echo 'retrying curl'
	n=$[$n+1]
		sleep 15
done

BROWSERS=`grep -v -e '^#' tests/e2e/Browsers.list | tr '\n' ',' | sed 's/[,]*$//g'`
testcafe "$BROWSERS" tests/e2e/**/*Fixture.ts tests/e2e/**/*.test.js --skip-js-errors
