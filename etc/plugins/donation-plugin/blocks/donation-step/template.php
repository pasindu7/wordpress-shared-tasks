<?php
$attributes = htmlspecialchars(json_encode($attributes ?? [], JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');
?>
<donation-step
	:step-attributes="<?php echo $attributes; ?>"
	:current-step="currentStep"
    :form-data="formData"
    :form-options="formOptions"
>
    <template slot="step" slot-scope="{formData}">
        <?php echo $content; ?>
    </template>
</donation-step>
<?php 
