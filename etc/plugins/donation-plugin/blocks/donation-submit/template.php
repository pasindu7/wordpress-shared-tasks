<?php

// $options = htmlspecialchars(json_encode($attributes['options'] ?? [], JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');
$className = $attributes['className'] ?? '';
$options = $attributes['options'];
$label = $options['label'] ?? '';
$type = $options['type'] ?? 'text';

printf(
    '<div class="donation-submit donation-submit-%s%s"%s>',
    $type,
    $className ? ' ' . $className : '',
    $type != 'submit' ? '' : ' v-if="!hideSubmit"'
);

switch ($type) {
	case 'submit':
		printf('<button type="submit">%s</button>', $label);
	break;
	case 'next':
	case 'back':
	default:
        printf('<button @click.stop.prevent="$emit(\'action_%s\')">%s</button>', $type, $label);
	break;
}

printf('</div>');

// echo json_encode($attributes, JSON_PRETTY_PRINT);
