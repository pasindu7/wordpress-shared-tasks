<?php
$options = $attributes['options'];
$options['validation'] = 'choice';
$options['className'] = $attributes['className'] ?? '';
$options['label'] = strval($attributes['label']);
$options_encoded = htmlspecialchars(json_encode($options ?? [], JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');
?>
<donation-input :input-options="<?php echo $options_encoded; ?>" :form-data="formData"></donation-input>
