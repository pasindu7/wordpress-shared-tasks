<?php

// // $options = htmlspecialchars(json_encode($attributes['options'] ?? [], JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');

// $options = $attributes['options'];

// $label = $options['label'] ?? '';
// $name = $options['name'] ?? '';
// $default = $options['default'] ?? '';
// $placeholder = $options['placeholder'] ?? '';
// $validation = $options['validation'] ?? 'text';
// $id = md5(uniqid());

// printf('<div class="donation-input donation-input-%s">', $name ?? $validation ?? 'default');

// switch ($validation) {
// 	case 'text':
// 	default:
// 		if ($label) {
// 			printf('<label for="%s">%s</label>', $id, $label);
// 		}
// 		printf(
// 		'<input type="text" id="%s" placeholder="%s"%s%s>',
// 		$id,
// 		htmlspecialchars($placeholder, ENT_QUOTES, 'UTF-8'),
//         $name ? sprintf(' v-model="formData.%1$s" :value="formData.%1$s"', $name) : '',
// 		$default ? sprintf(' data-default="%s"', $default) : ''
// 	);
// 	break;
// }

// printf('</div>');
$options = $attributes['options'] ?? [];
$options['className'] = $attributes['className'] ?? '';

foreach ([
    'extra_rules_validation_map',
    'select_options'
] as $option) {
    if ($options[$option]) {
        $options[$option] = @json_decode($options[$option]);
    }
}

$options_encoded = htmlspecialchars(json_encode($options, JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');

//echo json_encode($options, JSON_PRETTY_PRINT);
if(isset($options['validation']) && $options['validation'] == 'address' && $options['autocomplete_enabled'] === true){
    wp_enqueue_script('google-maps-api-script', 'https://maps.googleapis.com/maps/api/js?key='.$options['google_api_key'].'&libraries=places');
}
?>
<donation-input :input-options="<?php echo $options_encoded; ?>" :form-data="formData"></donation-input>

