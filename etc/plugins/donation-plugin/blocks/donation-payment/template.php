<?php

// $options = htmlspecialchars(json_encode($attributes['options'] ?? [], JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');

$options = $attributes['options'] ?? [];
$settings = gutenberg_donations_plugin_donation_payment_config();
if ($options['payment_stripe_enabled'] === true) {
    wp_register_script('stripe', 'https://js.stripe.com/v3/', false, null, true);
    wp_enqueue_script('stripe');
}
if ($options['payment_paypal_enabled'] === true) {
    if (empty($settings['paypal']['client_id'])) {
        error_log("PAYPAL ERROR: client_id not set in config");
    } else {
        wp_register_script('paypal', "https://www.paypal.com/sdk/js?client-id={$settings['paypal']['client_id']}&currency=AUD&intent=capture&vault=true", false, null, true);
        wp_enqueue_script('paypal');
    }
}

$className = $attributes['className'] ?? '';
$options['className'] = $attributes['className'] ?? '';

$options_encoded = htmlspecialchars(json_encode($options, JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');
$gateway_settings = htmlspecialchars(json_encode($settings, JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');
?>
<donation-payment
    :payment-options="<?php echo $options_encoded; ?>"
    :gateway-settings="<?php echo $gateway_settings; ?>"
    :form-data="formData"
    :form-options="formOptions">
    <template v-slot:extras>
        <?php echo $content; ?>
    </template>
</donation-payment>
