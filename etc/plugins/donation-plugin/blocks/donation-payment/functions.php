<?php
/**
 * This function exposes client-side config from machinepack
 */
if (!function_exists('gutenberg_donations_plugin_donation_payment_config')) {
    function gutenberg_donations_plugin_donation_payment_config() {
        require_once __DIR__ . '/../../vendor/autoload.php';
        $default = [
            'stripe' => [
                'key' => null
            ]
        ];
        $config = \WP_REST_Donations\Services\MachinePackFactory::config();
        if (empty($config)) {
            define('GUTENBERG_DONATIONS_PLUGIN_DONATION_PAYMENT_CONFIG', $default);
        } else {
            /** */
            $settings = [];
            $settings['stripe'] = [
                'key' => $config['handlers']['stripe']['settings'][$config['handlers']['stripe']['settings']['env']]['key'] ?? null
            ];
            $settings['paypal'] = [
                'client_id' => $config['handlers']['paypal']['settings'][$config['handlers']['paypal']['settings']['env']]['client_id'] ?? null
            ];
            $settings['direct_debit'] = [];
            define('GUTENBERG_DONATIONS_PLUGIN_DONATION_PAYMENT_CONFIG', $settings);
        }
        return GUTENBERG_DONATIONS_PLUGIN_DONATION_PAYMENT_CONFIG;
    }
}
