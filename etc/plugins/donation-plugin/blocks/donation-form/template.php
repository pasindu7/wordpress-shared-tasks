<?php

use WP_REST_Donations\Services\Transaction;

/**
 * Live preview sends true as 'true'!
 */
/*
$isEnabled = function ($key) use ($attributes) {
	return @$attributes['settings']["{$key}_enabled"] === 'true'
		|| @$attributes['settings']["{$key}_enabled"] === true
	;
};
<?php if ($isEnabled('navigation_top')): ?>
	<donation-navigation :on-top="true"></donation-navigation>
<?php endif; ?>
<?php echo $content; ?>
*/
global $post;
$options = htmlspecialchars(json_encode($attributes['options'] ?? [], JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');
$post_id = htmlspecialchars(json_encode($post->ID));
$form_id = $attributes['formId'] ?? '';
$form_id_js = htmlspecialchars(json_encode($form_id)) ?? 'undefined';
$endpoint_data = htmlspecialchars(json_encode(array(
    'root' => esc_url_raw( rest_url() ),
    'nonce' => wp_create_nonce( 'wp_rest' )
)));

$identifier = htmlspecialchars(json_encode(Transaction::init(
   array('status' => Transaction::STATUS_INITIAL)
)));

?>
<div class="donation-plugin-form-app-container">
    <donation-plugin-form :form-options="<?php echo $options; ?>"
        :post-id="<?php echo $post_id; ?>"
        :form-id="<?php echo $form_id_js; ?>"
        :endpoint-data="<?php echo $endpoint_data; ?>"
        :identifier="<?php echo $identifier; ?>"
        id="<?php echo "donation-form-$form_id"; ?>">
        <div class="donation-plugin-form-loading-container" v-cloak>
            <?php echo __($attributes['options']['form_loading_message'] ?? 'Loading ...'); ?>
        </div>
        <template slot="loading">
            <?php echo __($attributes['options']['form_loading_message'] ?? 'Loading ...'); ?>
        </template>
        <template slot="errors" slot-scope="{errorMessage}">
            <?php echo __($attributes['options']['form_failed_message'] . ' <br/>{{ errorMessage }}'); ?>
        </template>
        <template slot="processing">
            <?php echo __($attributes['options']['form_processing_message'] ?? 'Processing ...'); ?>
        </template>
        <template slot="form" slot-scope="{formData, currentStep, formOptions, hideSubmit}">
            <?php echo $content; ?>
        </template>
    </donation-plugin-form>
</div>
<noscript>
<?php echo $content; ?>
</noscript>
<?php
// live preview sends 'true'!
if (@$attributes['options']['debug_layout_enabled'] === true || @$attributes['options']['debug_layout_enabled'] === 'true'):
?>
<pre style="background-color: white; color: black; font-family: monospace; overflow: auto; font-size: 1rem">
<?php
 echo json_encode($options, JSON_PRETTY_PRINT);
 echo json_encode($attributes, JSON_PRETTY_PRINT);
echo htmlspecialchars($content);
?>
</pre>
<?php
endif;
