<?php
$className = $attributes['className'] ?? '';
$options = $attributes['options'] ?? [];
$options['className'] = $attributes['className'] ?? '';
$options_encoded = htmlspecialchars(json_encode($options, JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');
?>
<donation-choices :choice-options="<?php echo $options_encoded; ?>" :form-data="formData">
    <template slot="choices">
        <?php echo $content; ?>
    </template>
</donation-choices>
