<?php

$class_list = ['leafcutter-donation-tiles'];

$columns = $attributes['options']['columns'] ?? 3;
// error_log(json_encode($attributes));


$css_vars = "--tile-column-count: $columns;";
$css_vars .= '--tile-column-grid-cols-value: ' . trim(str_repeat('1fr ', $columns)) . ';';

/*
<pre>
	<?php echo json_encode($attributes, JSON_PRETTY_PRINT); ?>
</pre>
*/
?>
<div style="<?php echo $css_vars; ?>" class="donation-tiles-content-container">
<?php echo $content; ?>
</div>
