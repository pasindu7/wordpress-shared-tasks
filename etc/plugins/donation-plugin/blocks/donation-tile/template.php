<?php
$options = htmlspecialchars(json_encode($attributes['options'] ?? [], JSON_FORCE_OBJECT), ENT_QUOTES, 'UTF-8');
?>
<donation-tile :tile-settings="<?php echo $options; ?>" :form-data="formData">
<?php echo $content; ?>
</donation-tile>

