<?php
namespace WP_REST_Donations;

class Controller extends \WP_REST_Controller {

	/**
	 * Current API version
	 *
	 * @var string
	 */
	private $version = '1.0';

	/**
	 * Vendor prefix for all REST API hooks
	 *
	 * @var string
	 */
	private $vendor = 'leafcutter-donations';

	/**
	 * Register routes for WordPress
	 *
	 * @return void
	 */
	public function register_routes() {
		foreach (glob(__DIR__ . '/Actions/*.php') as $action_file) {
			$action_name = substr(basename($action_file), 0, -strlen('.php'));
			$class = "\\WP_REST_Donations\\Actions\\{$action_name}";
			try {
				$action = new $class($this);
			} catch (\Throwable $e) {
				error_log("$class not found, ignoring route registration");
				continue;
			}
			$prefix = "{$this->vendor}/{$this->version}";
			$route = $action->getRoute();

			// error_log("Registered $class for $prefix/$route");

			register_rest_route(
				$prefix,
				$route,
				[
					[
						'methods'  => $action->getMethods(),
						'callback' => [$action, 'execute'],
						'permission_callback' => [$action, 'permissionsCheck'],
						'args' => $action->getArgs()
					]
					// 'schema' => '' XXX
				]
			);
		}
	}

}
