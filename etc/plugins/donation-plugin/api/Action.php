<?php
namespace WP_REST_Donations;

abstract class Action {
	protected $controller;
	protected $route;
	protected $table_prefix;

	public function snakeCase($str) {
		return strtolower(preg_replace('/(.)(?=[A-Z])/u', '$1_', $str));
	}

	public function __construct(Controller $controller) {
		$this->controller = $controller;
		$this->route = $this->snakeCase((new \ReflectionClass($this))->getShortName());
		$this->table_prefix = require __DIR__ . '/../installer/table_prefix.php';
	}

	public function getRoute() {
		return $this->route;
	}

	public function getMethods() {
		return \WP_REST_Server::ALLMETHODS;
	}

	public function permissionsCheck() {
		return \current_user_can('read_private_pages');
	}

	public function getArgs() {
		return [];
	}

	public abstract function execute( \WP_REST_Request $request ): \WP_REST_Response;
}