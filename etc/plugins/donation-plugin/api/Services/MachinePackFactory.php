<?php
namespace WP_REST_Donations\Services;

/**
 * This should be moved to MachinePack, after reviewing
 * whether a full config should be saved in the plugin options or not
 */
final class MachinePackFactory {
    public static function init() {
        self::config();
    }
    public static function config() {
        $cfg_src = get_option('gutenberg_donations_plugin_machinepack_config');
        if (empty($cfg_src)) {
            return $cfg_src;
        }
        $cfg_tmp = tempnam(sys_get_temp_dir(), 'MPCFG');
        file_put_contents($cfg_tmp, $cfg_src);
        $config = \MachinePack\Core\MachinePack::config($cfg_tmp);
        unlink($cfg_tmp);

        return $config;
    }
}
