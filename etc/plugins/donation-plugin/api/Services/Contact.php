<?php
namespace WP_REST_Donations\Services;

final class Contact {
    const IDENTIFIER_FIELD = 'contact_email';

    public static function save(array $data) {
        $identifier = $data[Contact::IDENTIFIER_FIELD];
        DB::saveContact($data, $identifier);
        return self::find($identifier);
    }

    public static function find($identifier) {
        return DB::findContact($identifier);
    }
}
