<?php
namespace WP_REST_Donations\Services;

use Ramsey\Uuid\Uuid;

final class Transaction {
    const STATUS_INITIAL = 'initialized';
    const STATUS_PENDING = 'pending';
    const STATUS_SUCCESS = 'success';
    const STATUS_INCOMPLETE = 'incomplete';
    const STATUS_FAILED = 'failed';

    const STATUS_ERROR_MISSING_FIELDS = 'error_missing_fields';
    const STATUS_ERROR_INIT_MACHINEPACK = 'error_machinepack_init';
    const STATUS_ERROR_NO_MAPPER = 'error_no_mapper';
    const STATUS_ERROR_PROCESSING_GATEWAY = 'error_gateway';
    const STATUS_ERROR_PROCESSING_GATEWAY_NO_CARD = 'error_gateway_no_card';
    const STATUS_ERROR_PROCESSING_MACHINEPACK = 'error_machinepack';


    public static function init($data) {
        $uuid = Uuid::uuid4()->toString();
        Db::saveTransaction($data, $uuid);
        return $uuid;
    }

    public static function update($data, $uuid) {
        Db::saveTransaction($data, $uuid);
        return $uuid;
    }
}
