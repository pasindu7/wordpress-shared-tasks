<?php
namespace WP_REST_Donations\Services;

final class Db {
    /**
     * Save a record's details or replace existing one using $identifier
     *
     * @param string $table
     * @param mixed $data
     * @param string $identifier
     * @return void
     */
    private static function saveRecord($table, $data, $identifier = null)  {
        global $wpdb;
        $prefix = require __DIR__ . '/../../installer/table_prefix.php';
        $encoded = json_encode($data);

        if ($identifier) {
            $query = $wpdb->query($wpdb->prepare(
                "INSERT INTO `{$prefix}{$table}` (details, identifier) VALUES (%s, %s) ".
                "ON DUPLICATE KEY UPDATE details = %s",
                $encoded, $identifier, $encoded
            ));
        } else {
            $query = $wpdb->query($wpdb->prepare(
                "INSERT INTO `{$prefix}{$table}` (details) VALUES (%s) ",
                $encoded
            ));
        }
    }

    /**
     * List records in a table, processing details json column
     *
     * @param string $table table to list / search
     * @param string $where condition for where clause (including %s for prepare)
     * @param array $where_args arguments for where parameters
     * @param string $order whether to order (string with order clause)
     * @return array
     */
    private static function listRecords($table, string $where = '', array $where_args = [], $order = 'created_at DESC')  {
        global $wpdb;
        $prefix = require __DIR__ . '/../../installer/table_prefix.php';

		$results = $wpdb->get_results($wpdb->prepare(
            "SELECT * FROM {$prefix}{$table}".
            ($where ? " WHERE $where" : '').
            ($order ? " ORDER BY $order" : ''),
            $where_args
		), OBJECT);

		array_map(function (&$result) {
            if ($result->details) {
                $result->details = json_decode($result->details);
            }
		}, $results);

        return $results;
    }

    public static function saveGeneralLog($data, $identifier = null) {
        self::saveRecord('general_log', $data, $identifier);
    }

    public static function saveAuditLog($data, $transaction_id = null) {
        global $wpdb;
        $prefix = require __DIR__ . '/../../installer/table_prefix.php';
        $encoded = json_encode($data);
        $query = $wpdb->query($wpdb->prepare(
            "INSERT INTO `{$prefix}audit_log` (details, transaction_id) VALUES (%s, (SELECT id FROM {$prefix}transaction WHERE identifier = %s)) ",
            $encoded, $transaction_id
        ));
    }

    public static function saveContact($data, $identifier = null) {
        self::saveRecord('contact', $data, $identifier);
    }

    public static function findContact($identifier) {
        return current(self::listRecords('contact', 'identifier = %s', [$identifier]));
    }

    public static function findTransaction($identifier) {
        return current(self::listRecords('transaction', 'identifier = %s', [$identifier]));
    }

    public static function saveTransaction($data, $identifier = null) {
        self::saveRecord('transaction', $data, $identifier);
    }

    public static function listGeneralLog() {
        return self::listRecords('general_log');
    }
}
