<?php
namespace WP_REST_Donations\Mappers;

use MachinePack\Core\Result\Result;

abstract class Mapper {
    /**
     * Set up basic fields from form data
     *
     * @param array $data
     * @param string $transaction_id transaction_id
     * @return array
     */
    protected function prepareBaseFields($data, $transaction_id) {
        $out = [];
        $out['MonetaryAmount.value'] = $data['donation_amount'];
        $out['MonetaryAmount.currency'] = strtoupper($data['donation_amount_currency']);
        $out['Order.identifier'] = $transaction_id;
        $out['Order.recurring'] = strtoupper($data['donation_type']);

        $out['Person.givenName'] = $data['contact_first_name'];
        $out['Person.familyName'] = $data['contact_last_name'];

        $out['Person.telephone'] = $data['contact_phone'];

        $out['Person.email'] = $data['contact_email'];

        // $out['Person.ip'] = '127.0.0.1';
        $out['Person.notification'] = 'EMAIL';

        // FIXME after google auto fill
        $out['PostalAddress.streetAddress'] = $data['contact_address'];
        // $out['PostalAddress.addressLocality']      = 'Crows Nest';
        // $out['PostalAddress.addressRegion']        = 'NSW';
        // $out['PostalAddress.postalCode']           = '2065';
        // $out['PostalAddress.addressCountry']       = 'AU';


        // Only some gateways allow this
        // $out['CreditCard/CardDetails.name'] = 'John Doe';
        // $out['CreditCard/CardDetails.number'] = '4000000360000006';
        // $out['CreditCard/CardDetails.expiryMonth'] = '11';
        // $out['CreditCard/CardDetails.expiryYear'] = '2020';
        // $out['CreditCard/CardDetails.cvn'] = '123';
        // $out['CreditCard/CardDetails.type'] = 'Visa';

        return $out;
    }

    /**
     * This method takes form data and a transaction id,
     * returning:
     * [$machinepack_event_name, $event_data]
     *
     * @param array $data
     * @param string $transaction_id
     * @return array [$event_name, $data]
     */
    public abstract function process(array $data, array $options = array(), string $transaction_id): array;


    /**
     * Handle a machinepack error, this should convert this particular
     * handler's Result object into something the donation plugin can
     * understand
     *
     * @param array $data
     * @param string $transaction_id
     * @param Result $error or successs
     * @return array
     */
    public abstract function resultHandler(array $data, string $transaction_id, Result $result): array;

    /**
     * Prepare step for some mappers (before submit)
     *
     * @param array $data
     * @param string $transaction_id
     * @return array
     */
    public function prepare(array $data, string $transaction_id): array {
        return [];
    }

    /**
     * Convert the string to a date string.
     *
     * @param date $data    form data
     * @return string
     */
    public function getPaymentDate($data){
        preg_match('!\d+!', $data['payment_date'], $start_day);
        $day = isset($start_day[0])?$start_day[0]:date('d');
        $day = filter_var($day, FILTER_SANITIZE_NUMBER_INT);
        $today = date("d"); 
        $year = date("Y"); 
        if($today < $day){
            $month = date("m");
        }else{
            $month = date("m", strtotime("+1 month"));
        }
        return $year .'-'.$month .'-'. sprintf("%02d", $day);
    }
    /**
     * Number of days between two dates.
     *
     * @param date $dt1    First date
     * @param date $dt2    Second date
     * @return int
     */
    public function daysBetween($dt1, $dt2) {
        return date_diff(
            date_create($dt2),  
            date_create($dt1)
        )->format('%a');
    }
}
