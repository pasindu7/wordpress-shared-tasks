<?php
namespace WP_REST_Donations\Mappers;

use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Success;
use WP_REST_Donations\Services\Transaction;

class Salesforce extends Mapper {
    private $event_map = [
        'salesforce' => 'crmsync.create.salesforce',
    ];

    /**
     * Handle creating a query to search Salesforce instance
     *
     * @param mixed $data
     * @param json $query
     * @param Result $event
     */
    private function _createContactSearchQuery($data, $query){
        foreach($query as $key => $query_value){
            preg_match_all('/@\{([a-zA-Z0-9_|.\/]+)\}/', $query_value, $replace_values);
            if(!empty($replace_values)){
                $original = current($replace_values);
                $replace_value = end($replace_values);

                foreach($replace_value as $key => $contact_value){
                    $query_value = str_replace($original[$key], isset($data[$contact_value])?$data[$contact_value]:'', $query_value);
                }
            }
        }
 
        $event              = array();
        $event['method']    = 'GET';
        $event['query']     = $query_value;
        $event['Intangible/Event.payload'] = array();

        return $event;  
    }

    /**
     * Handle creating a contact array to submit to MachinePack
     *
     * @param mixed $data
     * @param json $contact
     * @param Result $event
     */
    private function _createContact($data, $contact){
        $phone_number = '';

        if(strtolower($data['address_country']) != 'australia'){
            $data['address_state'] = '';
        }
        if(isset($data['address_unit_no']) && $data['address_unit_no']){
            $data['address_unit_no'] = $data['address_unit_no'] . '/';
        }

        preg_match_all('/@\{([a-zA-Z0-9_|.\/]+)\}/', $contact, $replace_values, PREG_PATTERN_ORDER);
        if(!empty($replace_values)){
            $original = current($replace_values);
            $replace_value = end($replace_values);
            foreach($replace_value as $key => $contact_value){
                if(isset($data[$contact_value]) && is_array($data[$contact_value])){
                    $combined_data = '';
                    foreach($data[$contact_value] as $data_value){
                        $combined_data .= $data_value . ' ';
                    }
                    $contact = str_replace($original[$key], trim($combined_data), $contact);
                }else{
                    if(strpos($contact_value, 'phone') !== false){
                        if(!$phone_number){
                            $phone_number = $data[$contact_value];
                        }

                        if(strpos($contact_value, 'home') !== false && substr($phone_number, 0, 2) != '04'){
                            $contact = $this->str_replace_first($original[$key], $phone_number, $contact);
                            if(strpos($contact, '[preferred_phone]') !== false){
                                $contact = str_replace('[preferred_phone]', 'Home', $contact);
                            }
                        }else if(strpos($contact_value, 'home') === false && substr($phone_number, 0, 2) == '04'){
                            $contact = $this->str_replace_first($original[$key], $phone_number, $contact);
                            if(strpos($contact, '[preferred_phone]') !== false){
                                $contact = str_replace('[preferred_phone]', 'Mobile', $contact);
                            }
                        }
                        $contact = str_replace($original[$key], '', $contact);
                    }else if(strpos($contact_value, isset($data['payment_gateway_choice'])?$data['payment_gateway_choice']:'unknown') !== false){
                        $payment_gateway_field = explode('|', $contact_value);
                        $contact = str_replace($original[$key], $data[$data['payment_gateway_choice']][end($payment_gateway_field)], $contact);
                    }else{
                        $contact = str_replace($original[$key], isset($data[$contact_value])?$data[$contact_value]:'', $contact);
                    }
                }
            }
        }

        $event            = array();
        $event['method']  = 'POST';
        $event['sobject'] = 'Contact';
        $event['Intangible/Event.payload'] = (array)json_decode($contact);
        return $event;
    }

    /**
     * Handle creating a opportunity array to submit to MachinePack
     *
     * @param mixed $data
     * @param mixed $options
     * @param json $opportunity
     * @param Result $event
     */
    private function _createOpportunity($data, $options, $opportunity){
        $data['date'] = date('c');
        $data['name'] = 'Web Donation - ' . $data['date'];
        if(isset($data['payment_date'])){
            $start_date = parent::getPaymentDate($data);
            $data['payment_start_date'] = date('c', strtotime($start_date));
        }
        if(isset($data['account_bank']) && !$data['account_bank']){
            $data['account_bank'] = 'Bank';
        }
        if(isset($data['account_bsb']) && $data['account_bsb']){
            $data['account_bsb'] = implode("-", str_split($data['account_bsb'], 3));
        }
        $data['acknowledged_status'] = '';
        $data['stage_name'] = 'Open';
        if(isset($options['alternative_oneoff_record_id_amount']) && (int)$data['donation_amount'] >= (int)$options['alternative_oneoff_record_id_amount']){
            $data['stage_name'] = 'Negotiation (MG)';
        }
        if(isset($data['status']) && $data['status'] === 'success'){
            $data['acknowledged_status'] = 'To Be Acknowledged';
            $data['stage_name'] = 'Closed Won';
            if(isset($options['alternative_oneoff_record_id_amount']) && (int)$data['donation_amount'] >= (int)$options['alternative_oneoff_record_id_amount']){
                $data['stage_name'] = 'Successful';
            }
        }else if(isset($data['status']) && $data['status'] === 'failed'){
            $data['stage_name'] = 'Closed Lost';
            $data['lost_reason'] = 'Transaction Failed';
            $data['acknowledged_status'] = 'Do Not Acknowledge';
            if(isset($options['alternative_oneoff_record_id_amount']) && (int)$data['donation_amount'] >= (int)$options['alternative_oneoff_record_id_amount']){
                $data['stage_name'] = 'Unsuccessful';
            }
        }

        if(isset($data['donation_purposes']) && $data['donation_purposes'] != "I'd rather not say"){
            $data['type'] = 'In Memoriam';
        }else{
            $data['type'] = 'Once-off Gift';
            if(strpos($data['donation_type'], 'month') !== false){
                $data['type'] = 'Regular Gift';
                $data['PD_Sub_Frequency__c'] = 1;
                $data['PD_Sub_Interval__c'] = 'Month';
            }
        }

        $data['payment_method'] = 'Web';
        if(isset($data['payment_gateway_choice']) &&$data['payment_gateway_choice'] == 'paypal'){
            $data['payment_method'] = 'PayPal';
        }else if(isset($data['payment_gateway_choice']) && $data['payment_gateway_choice'] == 'direct_debit'){
            $data['payment_method'] = 'Direct Debit';
        }

        $data['PD_Sub_Frequency__c'] = 1;
        $data['PD_Sub_Interval__c'] = "Month";

        preg_match_all('/@\{([a-zA-Z0-9_|.\/]+)\}/', $opportunity, $replace_values, PREG_PATTERN_ORDER);
        if(!empty($replace_values)){
            $original = current($replace_values);
            $replace_value = end($replace_values);
            foreach($replace_value as $key => $opportunity_value){
                if(strpos($opportunity_value, 'campaign') !== false){
                    $opportunity = str_replace($original[$key], isset($options[$opportunity_value])?$options[$opportunity_value]:'', $opportunity);
                }else if(strpos($opportunity_value, 'record') !== false){
                    if(strpos($data['donation_type'], 'month') !== false){
                        $opportunity = str_replace($original[$key], isset($options['monthly_record_id'])?$options['monthly_record_id']:'', $opportunity);
                    }else{
                        if(isset($options['alternative_oneoff_record_id_amount']) && 
                            (int)$data['donation_amount'] >= (int)$options['alternative_oneoff_record_id_amount']){
                            $opportunity = str_replace($original[$key], isset($options['alternative_oneoff_record_id'])?$options['alternative_oneoff_record_id']:'', $opportunity);
                        }else{
                            $opportunity = str_replace($original[$key], isset($options['oneoff_record_id'])?$options['oneoff_record_id']:'', $opportunity);
                        }
                    }
                }else if(isset($data[$opportunity_value]) && is_array($data[$opportunity_value])){
                    $combined_data = '';
                    foreach($data[$opportunity_value] as $data_value){
                        $combined_data .= $data_value . ' ';
                    }
                    $opportunity = str_replace($original[$key], trim($combined_data), $opportunity);
                }else{
                   if(strpos($opportunity_value, isset($data['payment_gateway_choice'])?$data['payment_gateway_choice']:'unknown') !== false){
                        $payment_gateway_field = explode('|', $opportunity_value);
                        $opportunity = str_replace($original[$key], $data[$data['payment_gateway_choice']][end($payment_gateway_field)], $opportunity);
                    }else{
                        $opportunity = str_replace($original[$key], isset($data[$opportunity_value])?$data[$opportunity_value]:'', $opportunity);
                    }
                }
            }
        }

        $event            = array();
        $event['method']  = 'POST';
        $event['sobject'] = 'Opportunity';

        $event['Intangible/Event.payload'] = (array)json_decode($opportunity);

        return $event;
    }

    public function process(array $data, array $options = array(), string $transaction_id = ''): array {
        $query_event = array();
        $contact_event = array();
        $opportunity_event = array();

        $query_mapping = json_decode($options['salesforce_query_mapping']);
        $contact_mapping = $options['salesforce_contact_mapping'];
        $opportunity_mapping = $options['salesforce_opportunity_mapping'];
        if(strpos($data['donation_type'], 'month') !== false){
            $opportunity_mapping = $options['salesforce_opportunity_mapping_recurring'];
        }

        if(!empty($query_mapping)){
            $query_event = $this->_createContactSearchQuery($data, $query_mapping);
        }
        if(!empty($contact_mapping)){
            $contact_event = $this->_createContact($data, $contact_mapping);
        }
        if(!empty($opportunity_mapping)){
            $opportunity_event = $this->_createOpportunity($data, $options, $opportunity_mapping);
        }

        $event = array(
            "search" => $query_event, 
            "createContact" => $contact_event, 
            "createOpportunity" => $opportunity_event,
            "Intangible/Event.payload" => ''
        );

        $path = $this->event_map[strtolower($options['selected_crm_integration'])] ?? 'none';

        return [$path, $event];
    }

    public function str_replace_first($from, $to, $subject)
    {
        $from = '/'.preg_quote($from, '/').'/';

        return preg_replace($from, $to, $subject, 1);
    }

    /**
     * Handle a stripe result
     *
     * @param mixed $data
     * @param string $transaction_id
     * @param Result $result
     * @return [data, tid, status, message] last 2 if there was an error
     */
    public function resultHandler(array $data, string $transaction_id = '', Result $result): array {
        if ($result instanceof Success) {
            $salesforce_data = array(
                'Salesforce Contact' => $result->data['Person.identifier'],
                'Salesforce Account' => $result->data['Account.identifier'],
                'Salesforce Opportunity' => $result->data['Intangible/Opportunity']
            );
            return [$salesforce_data, null, null];
        }

        $output = $result->asHttpResponseData();
        $reason = $output['reason'] ?? 'unknown';

        $code = Transaction::STATUS_ERROR_PROCESSING_GATEWAY;
        if (preg_match('/You cannot use a Stripe token more than once/', $output['reason'])) {
            $code = Transaction::STATUS_ERROR_PROCESSING_GATEWAY_NO_CARD;
        }

        return [
            $data,
            $code,
            $reason
        ];
    }
}
