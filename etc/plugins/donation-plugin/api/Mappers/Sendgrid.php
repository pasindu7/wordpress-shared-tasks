<?php
namespace WP_REST_Donations\Mappers;

use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Success;
use WP_REST_Donations\Services\Email;
use WP_REST_Donations\Services\Contact;

class Sendgrid extends Mapper
{
    private $event_map = [
        'sendgrid' => 'crmsync.create.sendgrid',
    ];

    public function process(array $data, array $options = array(), string $transaction_id): array {
        $path = $this->event_map[strtolower($options['selected_email_integration'])] ?? 'none';
        $event = $this->_createMPEvent($data, $options);
        return [$path, $event];
    }

    public function resultHandler(array $data, string $transaction_id, Result $result): array {
        if ($result instanceof Success) {
            return [$data, null, null];
        }

        $output = $result->asHttpResponseData();
        $reason = $output['reason'] ?? 'unknown';
        $code = Email::STATUS_ERROR_EMAIL_SUBMISSION;

        return [
            $output,
            $code,
            $reason
        ];
    }

    private function _createMPEvent($data, $options)
    {
        $event = array();
        $event['Intangible/SendGrid.smart_id'] = $options['sendgrid_donation_acknowledgment_smart_email_id'];
        $event['EmailMessage.recipient.email'] = array (
            array (
                'email' => $data['contact_email'],
                "name"  => trim(implode(' ', [$data['contact_first_name'], $data['contact_last_name']]))
            )
        );
        $event['EmailMessage.sender.email']    = $options['email_integrations_sender_email'];
        $event['EmailMessage.sender.name']     = $options['email_integrations_sender_name'];

        $dataSetStructure                      = $options['sendgrid_donation_acknowledgment_datamap'];
        $dataSet                               = $this->_createDataSet($dataSetStructure, $data);
        $event['Intangible/Event.payload']     = $dataSet;

        return $event;
    }

    private function _createDataSet($structure, $data)
    {
        $dataSet = [];
        if (is_object(json_decode($structure))) {
            $dataMap = json_decode($structure, true);
            foreach($dataMap as $setKey => $dataKey) {
                $key = $this->_getDataKeyName($dataKey);
                if(!empty($key)) {
                    $dataSet[$setKey] = $data[$key] ?? null;
                }
            }
        }

        return $dataSet;
    }

    private function _getDataKeyName($dataKey)
    {
        return $this->_getStringBetweenTwoStrings($dataKey, "@{", "}");
    }

    private function _getStringBetweenTwoStrings($string, $start, $end){
    $r = explode($start, $string);
        if (isset($r[1])){
            $r = explode($end, $r[1]);
            return $r[0] ?? '';
        }
        
        return '';
    }
}
