<?php
namespace WP_REST_Donations\Mappers;

use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Success;
use WP_REST_Donations\Services\Transaction;

class Paypal extends Mapper {
    private $event_map = [
        'monthly' => 'subscription.create.paypal',
        'once' => 'payment.create.paypal',
        'one-off' => 'payment.create.paypal',
    ];

    private function _prepareNames($event, $data) {
        $event['Person.givenName'] = $data['paypal_payment_data']['payer']['name']['given_name'] ?? $event['Person.givenName'] ?? $event['cardholder_name'] ?? null;
        $event['Person.familyName'] = $data['paypal_payment_data']['payer']['name']['surname'] ?? $event['Person.familyName'] ?? null;
        $event['Person.email'] = $data['paypal_payment_data']['payer']['email_address'] ?? $event['Person.email'] ?? null;
        return $event;
    }

    public function process(array $data, array $options = array(), string $transaction_id): array {
        $event = $this->prepareBaseFields($data, $transaction_id);

        $event['Intangible/PayPal.authorizationId'] = $data['paypal_payment_data']['purchase_units'][0]['payments']['authorizations'][0]['id'] ?? null;
        $event['Intangible/PayPal.captureId'] = $data['paypal_payment_data']['purchase_units'][0]['payments']['captures'][0]['id'] ?? null;
        $event['Intangible/PayPal.paymentId'] = $data['paypal_payment_data']['id'] ?? null;

        if (!empty($data['paypal_subscription_data'])) {
            $event['Product/Subscription.duration'] = 0;
            $event['Product.productId'] = $data['product_product_id'];
            $event['Intangible/PayPal.subscriptionId'] = $data['paypal_subscription_data']['subscriptionID'] ?? null;
            $event['Intangible/PayPal.orderId'] = $data['paypal_subscription_data']['orderID'] ?? null;
        }

        if(isset($data['payment_date'])){
            $start_date = parent::getPaymentDate($data);
            $trail_period = parent::daysBetween(date(), $start_date);
            $event['Product/Subscription.start.date'] = $trail_period;
        }
        $event = $this->_prepareNames($event, $data);

        $path = $this->event_map[strtolower($data['donation_type'])] ?? 'none';
        return [$path, $event];
    }

    /**
     * Handle a stripe result
     *
     * @param mixed $data
     * @param string $transaction_id
     * @param Result $result
     * @return [data, tid, status, message] last 2 if there was an error
     */
    public function resultHandler(array $data, string $transaction_id, Result $result): array {
        $data['paypal_transaction'] = false;

        if((isset($data['paypal_subscription_data']) || isset($data['paypal_payment_data']) || $result instanceof Success)){
            $data['paypal_transaction'] = true;
            if(!empty($data['paypal_payment_data'])){
                $data['paypal']['payer_id'] = $data['paypal_payment_data']['payer']['payer_id'];
                $data['paypal']['transaction_id'] = $data['paypal_payment_data']['purchase_units'][0]['payments']['captures'][0]['id'];
                $data['transaction_id'] = $data['paypal_payment_data']['purchase_units'][0]['payments']['captures'][0]['id'];
            }
            if(!empty($data['paypal_subscription_data'])){
                $data['paypal']['payer_id'] = $data['paypal_subscription_data']['orderID'];
                $data['paypal']['transaction_id'] = $data['paypal_subscription_data']['subscriptionID'];
                $data['transaction_id'] = $data['paypal_subscription_data']['subscriptionID'];
            }

            return [$data, null, null];
        }

        $output = $result->asHttpResponseData();
        $reason = $output['reason'] ?? 'unknown';

        $code = Transaction::STATUS_ERROR_PROCESSING_GATEWAY;
        if (preg_match('/You cannot use a Stripe token more than once/', $output['reason'])) {
            $code = Transaction::STATUS_ERROR_PROCESSING_GATEWAY_NO_CARD;
        }

        return [
            $data,
            $code,
            $reason
        ];
    }

    /**
     * Prepare a paypal subscription)
     *
     * @param array $data
     * @return array
     */
    public function prepare(array $data, string $identifier): array {
        $event = $this->prepareBaseFields($data, $transaction_id);

        $event = $this->_prepareNames($event, $data);

        $event['Product/Subscription.duration'] = '';
        $event['Product.productId'] = $data['product_product_id'] ?? '';

        if(isset($data['payment_date'])){
            $start_date = parent::getPaymentDate($data);
            $trail_period = parent::daysBetween(date(), $start_date);
            $event['Product/Subscription.start.date'] = $trail_period;
        }

        $path = $this->event_map[strtolower($data['donation_type'])] ?? 'none';
        return [$path, $event];
    }
}
