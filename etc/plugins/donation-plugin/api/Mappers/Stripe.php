<?php
namespace WP_REST_Donations\Mappers;

use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Handler\Payment\Stripe as StripeHandler;
use WP_REST_Donations\Services\Transaction;
use WP_REST_Donations\Services\Contact;

class Stripe extends Mapper {
    const STRIPE_CUSTOMER_ID_FIELD = 'stripe_customer_id';
    const MACHINEPACK_CUSTOMER_ID = 'Intangible/Customer.id';

    private $event_map = [
        'monthly' => 'subscription.create.stripe',
        'once' => 'payment.create.stripe',
        'one-off' => 'payment.create.stripe',
    ];

    public function process(array $data, array $options = array(), string $transaction_id): array {
        $event = $this->prepareBaseFields($data, $transaction_id);

        if (!empty($data['stripe_token_data']['token']['id'])) {
            $event['Intangible/StripePayment.token'] = $data['stripe_token_data']['token']['id'];
        }
        if (!empty($data['stripe_token_data']['token']['client_ip'])) {
            $event['Person.ip'] = $data['stripe_token_data']['token']['client_ip'];
        }
        if (!empty($data['payment_stripe_product_id'])) {
            $event['Product.productId'] = $data['payment_stripe_product_id'];
        }
        $event['Product/Subscription.duration'] = 0;

        $event['Person.givenName'] = $event['Person.givenName'] ?? $event['cardholder_name'] ?? null;

        $path = $this->event_map[strtolower($data['donation_type'])] ?? 'none';

        $existing_contact = Contact::find($data[Contact::IDENTIFIER_FIELD]);

        if(isset($data['payment_date'])){
            $event['Product/Subscription.start.date'] = strtotime(parent::getPaymentDate($data));
        }
        
        if (!$existing_contact) {
            Contact::save($data);
        } else if ($existing_contact->details && $existing_contact->details->{self::STRIPE_CUSTOMER_ID_FIELD}) {
            $event[self::MACHINEPACK_CUSTOMER_ID] = $existing_contact->details->{self::STRIPE_CUSTOMER_ID_FIELD};
        }

        return [$path, $event];
    }

    /**
     * Handle a stripe result
     *
     * @param mixed $data
     * @param string $transaction_id
     * @param Result $result
     * @return [data, tid, status, message] last 2 if there was an error
     */
    public function resultHandler(array $data, string $transaction_id, Result $result): array {
        if ($result instanceof Success) {
            $data['stripe'] = $result->data;
            $data['transaction_id'] = $result->data['MoneyTransfer.identifier'];

            try {
                Contact::save(array_merge($data, [
                    self::STRIPE_CUSTOMER_ID_FIELD => $data['stripe']['customer']
                ]));
            } catch (\Exception $e) {
                // unable to save -> next transaction creates new user, no problem
            }
            return [$data, null, null];
        }

        $output = $result->asHttpResponseData();
        $reason = $output['reason'] ?? 'unknown';

        $code = Transaction::STATUS_ERROR_PROCESSING_GATEWAY;
        if (preg_match('/You cannot use a Stripe token more than once/', $output['reason'])) {
            $code = Transaction::STATUS_ERROR_PROCESSING_GATEWAY_NO_CARD;
        }

        return [
            $output,
            $code,
            $reason
        ];
    }
}
