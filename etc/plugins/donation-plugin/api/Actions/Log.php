<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;
use WP_REST_Donations\Services\Db;

class Log extends Action {
    /**
     * Save anything to an audit log.
     * Usually 'event' => your_key at least is good
     *
     * @param \WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function execute( \WP_REST_Request $request ): \WP_REST_Response {
        $data = $request->get_json_params();

        $message = 'saved';
        try {
            Db::saveGeneralLog($data);
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new \WP_REST_Response($message);
    }

    /**
     * Logs can be done by anyone
     *
     * @return void
     */
    public function permissionsCheck() {
		return true;
	}
}
