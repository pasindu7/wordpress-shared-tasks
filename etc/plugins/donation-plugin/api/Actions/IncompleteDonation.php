<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;
use WP_REST_Donations\Services\Db;
use WP_REST_Donations\Services\Transaction;

class IncompleteDonation extends Action {
	public function execute( \WP_REST_Request $request ): \WP_REST_Response {

		$data = $request->get_json_params();
		$message = 'Updated incomplete record';
		$code = 200;
        try {
			if(isset($data['identifier']) && $data['identifier']){
				$data['status'] = 'incomplete';
				$transaction_id = Transaction::update($data, $data['identifier']);
			}else{
				$message = 'Message not updated due to missing transaction id';
				return new \WP_REST_Response($message, 404);
			}
        } catch (\Exception $e) {
			$message = $e->getMessage();
			$code = 500;
        }

		return new \WP_REST_Response($message, $code);
	}

    /**
     * This action is public
     *
     * @return void
     */
	public function permissionsCheck() {
		return true;
	}
}
