<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;
use WP_REST_Donations\Services\Db;
use WP_REST_Donations\Services\MachinePackFactory;
use WP_REST_Donations\Services\Transaction;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;

class Submit extends Action {
    private $_required = [
        'donation_type',
        'payment_gateway_choice',
        'donation_amount'
    ];

    /**
     * Save a logging entry before and after processing
     *
     * @param array $data
     * @return void
     * TODO add wpdb inserts
     */
    private function log($data, $identifier = null) {
        try {
            Db::saveAuditLog($data, $identifier);
        } catch (\Exception $e) {
        }
    }

    /**
     * This action is public
     *
     * @return void
     */
	public function permissionsCheck() {
		return true;
	}

    /**
     * Basic error handler for logging + transaction + response
     *
     * @param array $data
     * @param string $transaction_id
     * @param string $status
     * @param string $message
     * @param int $code http code
     * @return void
     */
    private function errorHandler($data, $transaction_id, $status, $message, $code = 400) {
        try {
            $tdata = array_merge($data, [
                'status' => $status,
                'error' => $status,
                'error_message' => $message
            ]);
            Db::saveTransaction($err, $transaction_id);
            $this->log($tdata, $transaction_id);
        } catch (\Exception $e) {
            error_log ('Error logger error ' . $e->getMessage());
        }
        return new \WP_REST_Response( [
            'error' => $status,
            'error_message' => $message
        ], $code);
    }

    /**
     * Main payment processing step
     *
     * @param [type] $data
     * @return void
     */
    private function process($data, $options = null, $transaction_id) {
        $new_data = array();
        $payment_error = '';

        $transaction_id = Transaction::update(array_merge($data, [
            'status' => Transaction::STATUS_PENDING
        ]), $transaction_id);

        // 1. validation
        foreach ($this->_required as $k) {
            if (empty($data[$k])) {
                return $this->errorHandler($data, $transaction_id, Transaction::STATUS_ERROR_MISSING_FIELDS, __( sprintf(
                    'You must include at least "%s" to submit a donation (missing "%s")',
                    implode('", "', $this->_required), $k
                ) ));
            }
        }

        // 2. initialise machinepack and report errors
        try {
            MachinePackFactory::init();
        } catch (\Exception $e) {
            return $this->errorHandler($data, $transaction_id, Transaction::STATUS_ERROR_INIT_MACHINEPACK, __( sprintf(
                'MachinePack internal error: %s', $e->getMessage()
            ) ), 500);
        }

        $payment_skip_key = $data['payment_gateway_choice'] .'_payment_skip_disabled';
        if(!isset($data[$payment_skip_key])){
            $data[$payment_skip_key] = false;
        }


        if(!$data[$payment_skip_key]){
            // 3. attempt to create a mapper for this gateway
            try {
                $class = sprintf('\WP_REST_Donations\Mappers\%s', ucfirst($data['payment_gateway_choice']));
                $mapper = new $class();
            } catch (\Error $e) {
                return $this->errorHandler($data, $transaction_id, Transaction::STATUS_ERROR_NO_MAPPER, __( sprintf(
                    'Mapper for gateway can\'t be created: %s', $e->getMessage()
                ) ), 500);
            }

            // 4. attempt to process form into MachinePack. Using the first result as the 'status' but that's arbitrary
            try {
                list($event_name, $event_data) = $mapper->process($data, $options, $transaction_id);
                list($result) = MachinePack::send($event_name, $event_data);
            } catch (\Exception $e) {
                return $this->errorHandler($data, $transaction_id, Transaction::STATUS_ERROR_PROCESSING_MACHINEPACK, __( sprintf(
                    'MachinePack processing error: %s', $e->getMessage()
                ) ), 400);
            }

            list ($new_data, $error_status, $error_message) = $mapper->resultHandler($data, $transaction_id, $result);
            if ($error_status) {
                $payment_error = $this->errorHandler($new_data, $transaction_id, $error_status, __($error_message), $result->asHttpCode());
            }
            
            if(!empty($payment_error) && !isset($new_data['paypal_transaction']))
            {
                $transaction_data = array_merge($new_data, [
                    'status' => Transaction::STATUS_FAILED
                ]);
            }else{
                $transaction_data = array_merge($new_data, [
                    'status' => Transaction::STATUS_SUCCESS
                ]);
            }
        }else{
            $transaction_data = array_merge($data, [
                'status' => Transaction::STATUS_SUCCESS
            ]);
        }

        // 5. attempt to process crm sync into MachinePack.
        try {
            if(isset($options['crm_integration_enabled']) && ($options['crm_integration_enabled'] === true ||
            $options['crm_integration_enabled'] === 'true')){
                $class = sprintf('\WP_REST_Donations\Mappers\%s', ucfirst($options['selected_crm_integration']));
                $mapper =  new $class();
                list($event_name, $event_data) = $mapper->process($transaction_data, $options, $transaction_id);
                list($result) = MachinePack::send($event_name, $event_data);
            }
        } catch (\Exception $e) {
            return $this->errorHandler($data, $transaction_id, Transaction::STATUS_ERROR_PROCESSING_MACHINEPACK, __( sprintf(
                'MachinePack processing error: %s', $e->getMessage()
            ) ), 400);
        }

        list ($new_data, $error_status, $error_message) = $mapper->resultHandler($data, $transaction_id, $result);

        if ($error_status) {
            return $this->errorHandler($new_data, $transaction_id, $error_status, __($error_message), $result->asHttpCode());
        }

        $transaction_data = array_merge($transaction_data, $new_data);

        Db::saveTransaction($transaction_data, $transaction_id);

        //Due to PayPal been processed using client side code (Paypal.vue) we skip the payment error check as it will always return an error.
        if(!empty($payment_error) && !isset($new_data['paypal_transaction'])){
            return $payment_error;
        }

        //6. attempt to process email into MachinePack.
        try {
            if(isset($options['email_integration_enabled']) && ($options['email_integration_enabled'] === true ||
                    $options['email_integration_enabled'] === 'true')){
                $class = sprintf('\WP_REST_Donations\Mappers\%s', ucfirst($options['selected_email_integration']));
                $mapper =  new $class();
                list($event_name, $event_data) = $mapper->process($data, $options, $transaction_id);
                list($result) = MachinePack::send($event_name, $event_data);
            }
        } catch (\Exception $e) {
            return $this->errorHandler($data, $transaction_id, Transaction::STATUS_ERROR_PROCESSING_MACHINEPACK, __( sprintf(
                'MachinePack processing error: %s', $e->getMessage()
            ) ), 400);
        }

        list ($email_response_data, $error_status, $error_message) = $mapper->resultHandler($data, $transaction_id, $result);
        if ($error_status) {
            return $this->errorHandler($email_response_data, $error_status, __($error_message), $result->asHttpCode());
        }

        return new \WP_REST_Response($transaction_data);
    }

    /**
     * Submit a payment form
     *
     * @param \WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function execute( \WP_REST_Request $request ): \WP_REST_Response {
        $data = $request->get_json_params();
        $options = array();
        if(is_array($data)){
            $form_data = $data[0];
            $options = $data[1];
            $transaction_id = $data[2];
        }
        $result = $this->process($form_data, $options, $transaction_id);
        return $result;
    }
}
