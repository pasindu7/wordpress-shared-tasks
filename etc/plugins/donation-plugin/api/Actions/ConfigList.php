<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;

class ConfigList extends Action {
    private $visibleOptions = [
        'gutenberg_donations_plugin_machinepack_config',
        'gutenberg_donations_plugin_enable_incomplete_crm_sync',
        'gutenberg_donations_plugin_transaction_field_list_settings'
    ];

	public function execute( \WP_REST_Request $request ): \WP_REST_Response {
		global $wpdb;

        $output = [];

        foreach ($this->visibleOptions as $option) {
            $value = get_option($option);
            $output[$option] = $value;
        }

		return new \WP_REST_Response($output);
	}
}
