<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;

class DonationList extends Action {

    /**
     * This action is public
     *
     * @return void
     */
	public function permissionsCheck() {
		return true;
    }
    
    /**
     * Show list of transactions, filtered by settings
     *
     * @param \WP_REST_Request $request {"filter": true|false, "page"?: number }
     * @return \WP_REST_Response
     */
    public function execute( \WP_REST_Request $request ): \WP_REST_Response {
        global $wpdb;
        $data = $request->get_json_params();

        $orderBy = "ORDER BY created_at DESC";

        if ($data['identifier']) {
            $query = "SELECT * FROM {$this->table_prefix}transaction WHERE identifier = %s $orderBy";
            $args = [$data['identifier']];
        } else {
            if(\current_user_can('read_private_pages')){
                $query = "SELECT * FROM {$this->table_prefix}transaction $orderBy";
                $args = [];
            }else{
                return new \WP_REST_Response('Forbidden', 401);
            }
        }

        $results = $wpdb->get_results($wpdb->prepare($query, $args), OBJECT);

        $filter = @\json_decode(\get_option('gutenberg_donations_plugin_transaction_field_list_settings'));
        if ($data['filter'] && !empty($filter)) {
            $columns = array_flip(array_merge(['details'], $filter));
        }

        array_map(function (&$result) use ($columns) {
            $result->details = json_decode($result->details);
            if ($columns) {
                foreach ($result as $prop => $val) {
                    if (!isset($columns[$prop])) {
                        unset($result->$prop);
                    }
                    if ($prop == 'details' && $val) {
                        foreach ($result->details as $sprop => $sval) {
                            if (!isset($columns[$sprop])) {
                                unset($result->details->$sprop);
                            }
                        }
                    }
                }
            }
        }, $results);

        return new \WP_REST_Response($results);
    }
}
