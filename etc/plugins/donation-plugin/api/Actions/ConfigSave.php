<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;

class ConfigSave extends Action {
    private $visibleOptions = [
        'gutenberg_donations_plugin_machinepack_config',
        'gutenberg_donations_plugin_enable_incomplete_crm_sync',
        'gutenberg_donations_plugin_transaction_field_list_settings'
    ];

	public function execute( \WP_REST_Request $request ): \WP_REST_Response {
        $data = $request->get_json_params();
        $saved = [];

        foreach ($this->visibleOptions as $option) {
            $saved[$option] = false;
            if (!empty($data[$option])) {
                $saved[$option] = update_option($option, strval($data[$option]));
            }
        }

        return new \WP_REST_Response($saved);
    }
}
