<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;
use WP_REST_Donations\Services\Db;
use WP_REST_Donations\Services\MachinePackFactory;
use WP_REST_Donations\Services\Transaction;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;
use PayPalHttp\HttpRequest;

class PreparePayment extends Action {

    /**
     * This action is public
     *
     * @return void
     */
	public function permissionsCheck() {
		return true;
	}

    /**
     * Submit a payment form
     *
     * @param \WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function execute( \WP_REST_Request $request ): \WP_REST_Response {
        try {
            $data = $request->get_json_params();
            list ($form_data, $identifier) = $data;
            $class = sprintf('\WP_REST_Donations\Mappers\%s', ucfirst($form_data['payment_gateway_choice']));
            $mapper = new $class();
            MachinePackFactory::init();
        } catch (\Error $e) {
            return new \WP_REST_Response(__( sprintf(
                'Failed initialising prepare: %s', $e->getMessage()
            ) ), 500);
        }
        try {
            list($event_name, $event_data) = $mapper->prepare((array) $form_data, $identifier);
            if (!$event_name) {
                return new \WP_REST_Response(__('Nothing to prepare'), 204);
            }
            list($result) = MachinePack::send($event_name, $event_data);
        } catch (\Exception $e) {
            return new \WP_REST_Response(__( sprintf(
                'Processing prepare error: %s', $e->getMessage()
            ) ), 400);
        }

        return new \WP_REST_Response($result);
    }
}
