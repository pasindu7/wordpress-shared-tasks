<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;

class TableFieldList extends Action {
    /**
     * Allowed tables for this API
     *
     * @var array
     */
    private $visibleTables = [
        'transaction',
    ];

    /**
     * List all columns available for a given table.
     * This will include all fields ever saved into this table in `details`
     *
     * @param \WP_REST_Request $request expecting at leas {"table":"<table name>"}
     * @return \WP_REST_Response
     */
    public function execute( \WP_REST_Request $request ): \WP_REST_Response {
        global $wpdb;

        $data = $request->get_json_params();

        if (!$data['table'] || !in_array($data['table'], $this->visibleTables)) {
            return new \WP_REST_Response(__( sprintf(
                'Please submit {table: X}, where X is one of: %s', implode($this->visibleTables)
            ) ), 400);
        }

        $table = $data['table'];

        $output = [];

        $dynamic = $wpdb->get_col($wpdb->prepare("
            SELECT DISTINCT JSON_KEYS(details) `cols`
            FROM {$this->table_prefix}{$table}
            HAVING `cols` IS NOT NULL
        "));
        $static = $wpdb->get_col($wpdb->prepare("
            SELECT CONCAT('[\"', GROUP_CONCAT(column_name SEPARATOR '\",\"'), '\"]') cols
            FROM information_schema.columns
            WHERE table_name = %s and column_name <> 'details'
        ", "{$this->table_prefix}{$table}"));

        $parsed = array_reduce(array_merge($dynamic, $static), function ($unique, $row) {
            $cols = (array) json_decode($row);
            return array_unique(array_merge($unique, $cols));
        }, []);

        sort($parsed);

        return new \WP_REST_Response($parsed);
    }
}
