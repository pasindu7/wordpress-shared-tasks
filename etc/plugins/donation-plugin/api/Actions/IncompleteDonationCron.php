<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Services\MachinePackFactory;
use WP_REST_Donations\Services\Db;
use WP_REST_Donations\Action;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;

class IncompleteDonationCron extends Action {
	public function execute( \WP_REST_Request $request ): \WP_REST_Response {
		global $wpdb;

		$code = 204;
		$results = $wpdb->get_results($wpdb->prepare(
			"SELECT * FROM {$this->table_prefix}transaction 
			WHERE details->'$.status' = %s
			ORDER BY created_at DESC"
		, 'incomplete'), ARRAY_A);

		foreach($results as $result){
			if(isset($result['details']) && $result['details']){
				$result['details'] = json_decode($result['details']);
			}else{
				continue;
			}
			$code = 200;
			$form_data = isset($result['details']->data)?(array)$result['details']->data:'';
			$options = isset($result['details']->options)?(array)$result['details']->options:'';

			if(isset($form_data['contact_email']) && $form_data['contact_email']){
				$identifier = $result['details']->identifier;
				$form_data['status'] = $result['details']->status;

				// 1. initialise machinepack and report errors
				try {
					MachinePackFactory::init();
				} catch (\Exception $e) {
					return new \WP_REST_Response(__(sprintf(
						'MachinePack internal error: %s', $e->getMessage()
					)), 500);
				}

				// 2. attempt to create a mapper for this gateway
				try {
					$class = sprintf('\WP_REST_Donations\Mappers\%s', ucfirst($options['selected_crm_integration']));
					$mapper =  new $class();
				} catch (\Error $e) {
					return new \WP_REST_Response(__(sprintf(
						'Mapper for gateway can\'t be created: %s', $e->getMessage()
					)), 500);
				}

				// 5. attempt to process crm sync into MachinePack.
				try {
					list($event_name, $event_data) = $mapper->process($form_data, $options);
					list($response) = MachinePack::send($event_name, $event_data);
				} catch (\Exception $e) {
					return new \WP_REST_Response(__( sprintf(
						'MachinePack processing error: %s', $e->getMessage()
					)), 500);
				}

				$form_data['status'] = 'CRM Synced';
				list ($new_data, $error_status, $error_message) = $mapper->resultHandler($form_data, '', $response);
				if ($error_status) {
					$form_data['status'] = 'CRM Sync failed';
					$form_data['error'] = $error_message;
					//return new \WP_REST_Response($error_message, 500);
				}

        		$transaction_data = array_merge($form_data, $new_data);

				if($response instanceof Success){
        			Db::saveTransaction($transaction_data, $identifier);
				}
			}
		}

		return new \WP_REST_Response('Completed', $code);
	}
	
    /**
     * This action is public
     *
     * @return void
     */
	public function permissionsCheck() {
		return true;
	}
}
