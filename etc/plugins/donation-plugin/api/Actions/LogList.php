<?php
namespace WP_REST_Donations\Actions;

use WP_REST_Donations\Action;
use WP_REST_Donations\Services\Db;

class LogList extends Action {
    /**
     * Save anything to an audit log.
     * Usually 'event' => your_key at least is good
     *
     * @param \WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function execute( \WP_REST_Request $request ): \WP_REST_Response {
		global $wpdb;
        $results = Db::listGeneralLog();
		return new \WP_REST_Response($results);
    }
}
