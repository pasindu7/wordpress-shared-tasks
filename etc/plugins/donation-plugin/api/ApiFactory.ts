/**
	* Basic API calls to WP admin
	*/
export const ApiFactory = apiSettings => ({
	/**
		* Simple fetch json <-> json
	 */
	async run(name, data = null) {

		let opts: any = {}

		if (apiSettings) {
			opts.headers = {
				"X-WP-Nonce": apiSettings.nonce
			}
		}

		if (data) {
			opts.method = 'post'
			opts.body = JSON.stringify(data)
			opts.headers['Content-Type'] = 'application/json'
		}

		let failure = false

		const response = await fetch(`${apiSettings.root}leafcutter-donations/1.0/${name}`, opts)
		failure = !response.ok
		const decoded = await response.json()

		if (failure) {
			throw decoded
		}

		return decoded
	},

	/**
		* 
		* @param event event to send for tracking
		* @param data data
		*/
	async logEvent(event: string, data: any) {
		return this.run('log', {
			event: event,
			...data
		})
	},

	/**
		* 
		* @param event event to send for tracking
		* @param data data
		*/
	async updateTransaction(data: any) {
		return this.run('incomplete_donation', {
			...data
		});
	}
})
