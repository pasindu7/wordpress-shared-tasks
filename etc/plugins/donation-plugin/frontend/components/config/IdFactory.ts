/**
	* Generate random IDs for html elements
 */
export default prefix => `${prefix}${Math.random() * 8999999 + 1000000 | 0}`
