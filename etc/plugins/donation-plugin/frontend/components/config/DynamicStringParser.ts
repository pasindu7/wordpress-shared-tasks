import Vue from "vue";

/**
	* Compile a string using Vue's internal syntax
	* @see https://vuejs.org/v2/api/#Vue-compile
 */
export default (src: string, data: any = {}): string => {
	try {
		const res = Vue.compile(`<span>${src}</span>`)
		const app = new Vue({
			data: data,
			render: res.render,
			staticRenderFns: res.staticRenderFns
		})
		const out = app.$mount().$el.innerText
		return out
	} catch (error) {
		return `your source "${src}" didn't compile: ${error}`
	}
}


