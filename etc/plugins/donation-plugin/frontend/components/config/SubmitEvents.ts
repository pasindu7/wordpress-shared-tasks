export default {
	VALIDATE: "submit_validate",
	BEFORE_PAYMENT: "submit_beforePayment",
	PAYMENT: "submit_payment"
};
