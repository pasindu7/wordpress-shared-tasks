export default {
	required: 'This field is required',
	numeric: 'This field must be numeric',
	phone: 'Please enter a valid phone number',
	email: 'Please enter a valid email'
};
