export default () => ({
	index: 0,
	skipped: new Map(),
	skip: function () {
		this.skipped.set(this.index, true);
		this.next()
	},
	next: function () {
		this.index += 1;
	},
	prev: function () {
		let prev = this.index - 1;
		while (this.skipped.has(prev) && prev >= 0) {
				prev = prev - 1;
		}
		this.index = prev;

		if (this.index == 0) {
			this.skipped.clear()
		}
		console.log(`idx: ${this.index} `, this.skipped)
	}
})
