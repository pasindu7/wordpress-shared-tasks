export default {
	TRIGGER_SUBMIT: 'input_trigger_submit',
	PROCESSING: 'input_trigger_processing',
	CRASHED: 'input_trigger_error',
	HIDE_SUBMIT: 'input_hide_submit',
	SHOW_SUBMIT: 'input_show_submit',
	USER_CANCELLED: 'input_user_cancelled',
	INPUT_VALUE_PATCH: 'input_value_patch'
}
