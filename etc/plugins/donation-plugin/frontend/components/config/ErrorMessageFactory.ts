export default (failedRules, errors, inputOptions) => {
	let def = errors;
	if (errors) {
		def = errors[0].replace("_", " ");
	}
	if (inputOptions) {
		for (const k of Object.keys(failedRules)) {
			if (
				inputOptions.extra_rules_validation_map &&
				inputOptions.extra_rules_validation_map[k]
			) {
				return inputOptions.extra_rules_validation_map[k]
			}

			if (inputOptions[`validation_message_${k}`]) {
				return inputOptions[`validation_message_${k}`];
			}
		}
	}
	return def;
}
