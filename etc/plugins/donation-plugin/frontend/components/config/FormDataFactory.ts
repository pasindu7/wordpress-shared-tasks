/**
	* @see https://wikipedia.org/wiki/UTM_parameters
	*/
const utm_fields = [
	'utm_source',
	'utm_medium',
	'utm_campaign',
	'utm_term',
	'utm_content',
]

/**
	* TODO add localStorage support here
	* @see LIP-90 UTM field support
 */
export default () => {
	// base data
	let data = {
		steps: []
	}

	// UTM fields
	try {
		const params = new URLSearchParams(window.location.search);
		for (const field of utm_fields) {
			try {
				const val = params.get(field)
				if (val) {
					data[field] = val
				}
			} catch (error) { }
		}
	} catch (error) { }

	return data
}
