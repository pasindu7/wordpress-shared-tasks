export default {
	MOUNTED: "form_loaded",
	SUBMITTED: "form_submitted",
	STEP_INVALID: "form_step_invalid",
	STEP_NEXT: "form_step_next",
	CANCELLED: "form_submit_cancelled"
};
