/**
	* Simple general class generation for 
	* - wordpress classes (className from advanced)
	* - custom prefix
	* - extra classes to be added
 */
export default (prefix, inputOptions, classes) => {
	let out = {};
	if (inputOptions) {
		if (inputOptions.name) {
			out[`${prefix}-${inputOptions.name}`] = true;
		}
		if (inputOptions.key) {
			out[`${prefix}-key-${inputOptions.key}`] = true;
		}
		if (inputOptions.className) {
			out[inputOptions.className] = true;
		}
	}
	return {
		...classes,
		...out
	};
}
