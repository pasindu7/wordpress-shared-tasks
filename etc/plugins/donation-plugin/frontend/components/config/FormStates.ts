export default {
	READY: "ready",
	ERROR: "error",
	PROCESSING: "processing",
	LOADING: "loading"
};
