/**
	* @param value any value to format as amount
	* @param showCents whether to include cents (decimal places)
 */
export default (value: any, showCents: boolean = true) => {
	if (isNaN(Number(value))) {
		return ''
	}
	if (showCents) {
		return (Math.round(value * 100) / 100).toFixed(2);
	} else {
		return Math.round(value)
	}
}
