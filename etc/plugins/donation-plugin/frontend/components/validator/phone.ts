import PhoneNumber from 'awesome-phonenumber'

export default (extend, messageProvider) => {
	extend('phone', {
		message(fieldName) {
			return messageProvider(fieldName) || `${fieldName} is not a valid phone number`;
		},
		validate(value) {
			return new Promise(resolve => {
				let local = new PhoneNumber(value, 'AU');
				let international = new PhoneNumber(value)
				resolve({ valid: local.isValid() || international.isValid() })
			});
		}
	});
}
