export default (extend, messageProvider) => {
	extend('custom_input', {
		message(fieldName) {
			fieldName = fieldName[0].toUpperCase() + fieldName.slice(1).toLowerCase();
			return messageProvider(fieldName) || `${fieldName} is not a valid input. Please make sure you remove any special characters`;
		},
		validate(value) {
			return new Promise(resolve => {
				const regex = /^[a-zA-Z0-9,. ]*$/gm;
				let valid = new RegExp(regex, 'g').test(value)
				resolve({ valid: valid })
			});
		}
	});
}
