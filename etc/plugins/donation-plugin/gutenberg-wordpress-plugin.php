<?php
/**
 * Plugin Name:     Gutenberg Donations plugin
 * Plugin URI:      https://marlincommunications.com
 * Description:     Gutenberg-based donations plugin
 * Author:          Marlin Team
 * Author URI:      https://marlincommunications.com
 * Text Domain:     gutenberg-donation-plugin
 * Domain Path:     /
 * Version:         1.3.58-b186
 *
 * @package         Machinepack
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Autoloader
 */
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Auto-register blocks using snippet
 */
add_action( 'init', leafcutter_blocks_autoregister([
	'base_path' => realpath( __DIR__ ),
	'base_url' => plugin_dir_url(__FILE__),
	'dist_suffix' => '',
	'dependency_var' => 'donation_plugin_blocks_whitelist'
]) );

/*
	ADD CRM SYNC CRON
*/
add_action('init', function(){
	if(get_option('gutenberg_donations_plugin_enable_incomplete_crm_sync') == 'true'){
		if (! wp_next_scheduled ( 'crm_sync_incomplete_transaction' )) {
			wp_schedule_event(time(), 'hourly', 'crm_sync_incomplete_transaction');
		}
		add_action('crm_sync_incomplete_transaction', 'startIncompleteSync', 10);
	}else{
		$timestamp = wp_next_scheduled( 'crm_sync_incomplete_transaction' );
		wp_unschedule_event( $timestamp, 'crm_sync_incomplete_transaction' );
	}
});

function startIncompleteSync(){
	$scheduled = file_get_contents(home_url() . '/wp-json/leafcutter-donations/1.0/incomplete_donation_cron');
}

/**
 * Add admin pages
 */
add_action( 'admin_menu', function () {
	$donation_list = add_menu_page('Donations', 'Donations', 'edit_pages', 'donation-list', function () {
		require_once __DIR__ . '/admin/DonationList.php';
	}, 'dashicons-money', 45);

	$donation_options = add_options_page('Donations', 'Donations', 'activate_plugins', 'donations-plugin', function () {
		require_once __DIR__ . '/admin/DonationOptions.php';
	}, 45);

	$assets = function () {
		wp_register_style( 'donation-plugin-admin-styles', plugin_dir_url(__FILE__) . '/admin.css', [], filemtime(realpath(__DIR__ . '/admin.css')) );
		wp_register_script( 'donation-plugin-admin-scripts', plugin_dir_url(__FILE__) . '/admin.js', [], filemtime(realpath(__DIR__ . '/admin.css')), true );

		wp_localize_script( 'donation-plugin-admin-scripts', 'donationPluginApiSettings', array(
			'root' => esc_url_raw( rest_url() ),
			'nonce' => wp_create_nonce( 'wp_rest' )
		));

		wp_enqueue_style( 'donation-plugin-admin-styles' );
		wp_enqueue_script( 'donation-plugin-admin-scripts' );
	};
	add_action( "load-$donation_list", $assets);
	add_action( "load-$donation_options", $assets);
});

/**
 * Installer
 */
register_activation_hook( __FILE__, function() {
	require_once __DIR__ . '/installer/installer.php';
} );

/**
 * API
 */
add_action(
	'rest_api_init',
	function () {
		$mp = new WP_REST_Donations\Controller();
		$mp->register_routes();
	}
);

add_action(
	'init',
	function () {
		wp_register_style( 'donation-plugin-frontend-styles', plugin_dir_url(__FILE__) . 'frontend.css', [], filemtime(realpath(__DIR__ . '/frontend.css')) );
		wp_register_script( 'donation-plugin-frontend-scripts', plugin_dir_url(__FILE__) . 'frontend.js', [], filemtime(realpath(__DIR__ . '/frontend.css')), true );

		wp_localize_script( 'donation-plugin-frontend-scripts', 'donationPluginApiSettings', array(
			'root' => esc_url_raw( rest_url() ),
			'nonce' => wp_create_nonce( 'wp_rest' )
		));

		wp_enqueue_style( 'donation-plugin-frontend-styles' );
		wp_enqueue_script( 'donation-plugin-frontend-scripts' );
	}
);


// /**
//  * API class encapsulating MachinePack functionality
//  */
// class MachinePack_WordPress_API extends WP_REST_Controller {

// 	/**
// 	 * Current API version
// 	 *
// 	 * @var string
// 	 */
// 	private $version = '1.0';

// 	/**
// 	 * Vendor prefix for all REST API hooks
// 	 *
// 	 * @var string
// 	 */
// 	private $vendor = 'machinepack';

// 	/**
// 	 * Register routes for WordPress
// 	 *
// 	 * @return void
// 	 */
// 	public function register_routes() {
// 		foreach ( [
// 			'webhook',
// 			'send',
// 			'jobstatus',
// 		] as $route ) {
// 			register_rest_route(
// 				"{$this->vendor}/{$this->version}",
// 				$route,
// 				[
// 					'methods'  => WP_REST_Server::ALLMETHODS,
// 					'callback' => [ $this, 'execute_' . $route ],
// 				]
// 			);
// 		}
// 	}
