# Automatically register your blocks

## Install

If you are using plain wordpress, clone this repo into your plugin / theme, then include it.
If you are using composer, follow these steps:

### Add repository and dependency:

```json
"require": {
  "leafcutter/blocks-autoregister": "dev-master"
},
...
"repositories": [{
  "type": "vcs",
    "url": "git@bitbucket.org:snippets/leafcuttercreativedigital/Eb7Kg8/automated-gutenberg-block-registration.git",
    "vendor-alias": "leafcutter"
}]
```

### Add the function including any registration options (see available options below)
```php
add_action( 'init', leafcutter_blocks_autoregister([
  'base_url' => plugin_dir_url(__FILE__),
  'dist_suffix' => ''
]));
```

## Options

| Option  | Default  | Description |
| --------|----------|-------------|
| `block_namespace` | `leafcutter` | register blocks under this namespace |
| `base_path` | `__DIR__` | path where assets are built (e.g., `blocks.js`) |
| `block_path` | `$base_path/blocks` | path where block logic lives (e.g., `template.php`) |
| `base_url` | `get_stylesheet_directory_uri() . '/blocks'` | URL for fetching assets. `plugin_dir_url(__FILE__)` for plugins, or this default value for themes. |
| `dist_suffix` | `/dist` | optional folder to save compiled assets |
| `dependency_var` | null | optional variable to define with all blocks that this plugin needs. this is useful for projects using whitelists on blocks. set it to for example `'MY_PLUGIN_BLOCKS_WHITELIST'` then use `MY_PLUGIN_BLOCKS_WHITELIST` in your `allowed_block_types` hook |

## Building / Development

While you're free to use any build tools you'd like to generate the blocks assets, here's an example config using Parcel:

```json
...
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "npm run watch",
    "watch": "concurrently --raw \"npm:watch_php\" \"npm:watch_blocks\" \"npm:watch_admin\"",
    "watch_php": "nodemon -w plugin/ -e php -x bin/build.sh php",
    "watch_blocks": "nodemon -w plugin/blocks/ -e js,jsx,scss -x \"npm run build_blocks\"",
    "watch_admin": "nodemon -w plugin/admin/ -e js,vue,scss -x \"npm run build_admin\"",
    "build": "concurrently --raw -c \"cyan,blue,green,yellow\" --names \"blocks, admin\"  \"npm:build_blocks\" \"npm:build_admin\"",
    "build_blocks": "concurrently --raw -c \"cyan,blue,green,yellow\" --names \"scripts, styles, editor styles\"  \"npm:build_blocks_scripts\" \"npm:build_blocks_styles\" \"npm:build_blocks_editor_styles\"",
    "build_blocks_scripts": "parcel build --out-file blocks.js -d dist/ plugin/blocks/index.js",
    "build_blocks_styles": "parcel build --out-file blocks.css -d dist/ plugin/blocks/style.scss",
    "build_blocks_editor_styles": "parcel build --out-file blocks-editor.css -d dist/ plugin/blocks/editor.scss",
    "build_admin": "parcel build --out-file admin.js -d dist/ plugin/admin/index.js"
  },
  ...
    "dependencies": {
    	"concurrently": "^5.2.0",
    	"nodemon": "^2.0.3",
    	"parcel-bundler": "^1.12.4",
  ...
```
		
		