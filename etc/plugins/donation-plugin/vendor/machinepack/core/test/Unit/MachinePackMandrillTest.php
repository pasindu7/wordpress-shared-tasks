<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackMandrill extends TestCase
{
    /**
     * Test : Send transaction smart email
     */
    public function testTransactionSmartEmail()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createTransactionSmartEmail();

        $results = MachinePack::send(
            'crmsync.create.mandrill',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    private function _createTransactionSmartEmail()
    {
        $event = array();
        
        $event['EmailMessage.template'] = 'Thank you for signing the petition';

        $event['EmailMessage.recipient.to'] =  array(
                                                    array(
                                                        'email' => 'dev@leafcutter.com.au',
                                                        'name' => 'John Doe'
                                                    ),
                                                    array(
                                                        'email' => 'dev@leafcutter.com.au',
                                                        'name' => 'John Doe 2'
                                                    )
                                                );

        $event['EmailMessage.cc']           = 'dev@leafcutter.com.au';
        $event['EmailMessage.cc.name']      = 'CC Name';
        $event['EmailMessage.bcc']          = 'dev@leafcutter.com.au';
        $event['EmailMessage.bcc.name']     = 'BCC Name';
        $event['EmailMessage.subject']      = 'Thank you for your donation';
        $event['EmailMessage.sender.email'] = 'noreply@bilbiesnotbunnies.com';
        $event['EmailMessage.replyto']      = 'dev@leafcutter.com.au';
        $event['EmailMessage.sender.name']  = 'Dont Dump the Bilby';
        
        $event['Intangible/Event.payload'] = [
            'first_name' => 'Test User',
            'amount'=>'550'
        ];

        return $event;
    }
}
