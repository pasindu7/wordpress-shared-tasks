<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackSendGridTest extends TestCase
{
    /**
     * Test : Send transaction smart email
     */
    public function testTransactionSmartEmail()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createTransactionSmartEmail();

        $results = MachinePack::send(
            'crmsync.create.sendgrid',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    private function _createTransactionSmartEmail()
    {
        $event = array();
        $event['Intangible/SendGrid.smart_id'] = 'd-959ebfad228b400fb6c4bc8b83e02a8a';
        $event['EmailMessage.recipient.email'] = array (
            array (
                'email' => 'adrian@leafcutter.com.au',
                "name" => "John Doe"
            )
        );
        $event['EmailMessage.sender.email']    = 'dev@leafcutter.com.au';
        $event['EmailMessage.sender.name']     = 'Marlin';
        $event['Intangible/Event.payload']     = [
            'emailTitle'=>'Machine Pack Test Email',
            'DonorFirstName' => 'Test User'
        ];

        return $event;
    }
}
