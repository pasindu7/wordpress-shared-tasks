<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackTest extends TestCase
{
    /**
     * Basic event subscription / emission support
     */
    public function testEventSubscriptionWithCallback()
    {
        $value   = null;
        $handler = function ($event) use (&$value) {
            if ($event instanceof Payment) {
                $value = $event['MonetaryAmount.value'];
                return new Success;
            }
            return new Ignored;
        };

        MachinePack::subscribe('payment.success', $handler);

        MachinePack::send(
            'payment.success',
            [
                'Person.givenName' => 'John Doe',
                'MonetaryAmount.currency' => 'AUD',
                'MonetaryAmount.value' => '1.00'
            ]
        );

        $this->assertEquals('1.00', $value);
    }

    /**
     * validate handlers return Result instances correctly
     */
    public function testEventSubscriptionWithResult()
    {
        $result = MachinePack::send(
            'payment.success',
            [
                'Person.givenName' => 'John Doe',
                'MonetaryAmount.currency' => 'AUD',
                'MonetaryAmount.value' => '1.00'
            ]
        );

        $this->assertEquals(1, count($result));
        $this->assertInstanceOf(Success::class, $result[0]);
        // $this->assertEquals('1.00', $value);
    }

    /**
     * Test required fields work
     */
    public function testRequiredSendFields()
    {
        $this->expectException(MissingArgumentsException::class);
        MachinePack::send('payment.success', []);
    }
}
