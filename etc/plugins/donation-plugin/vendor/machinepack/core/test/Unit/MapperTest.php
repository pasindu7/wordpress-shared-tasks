<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Ignored;

/**
 * Basic emailer tests
 */
final class MapperTest extends TestCase
{
    protected function setUp()
    {
        MachinePack::init(__DIR__ . '/MapperTest.yml');
    }

    /**
     * Validates email templating works, with mail() disabled
     *
     * @return void
     */
    public function testTemplating()
    {
        list(
            $output
        ) = MachinePack::send(
            'data.inbound',
            []
        );

        $processed = json_decode($output->data['message'], true);

        $this->assertEquals(
            [
                'Person.name' => 'testing lookup twig',
                'Intangible/Event.payload' => [
                    'nested' => [
                        'data_1' => 'val',
                        'data' => [
                            'property' => [
                                'val 1',
                                'val 2',
                            ],
                        ],
                    ],
                ],
            ],
            $processed
        );
    }

    /**
     * Handle ResultCollection
     *
     * @return void
     */
    public function testCollection()
    {
        $results = MachinePack::send(
            'data.inbound.multi',
            []
        );

        for ($i = 0; $i < 3; $i++) {
            $this->assertEquals(
                $results[0][$i]->data['message'],
                $results[1][$i]->data['message']
            );
        }
    }
}
