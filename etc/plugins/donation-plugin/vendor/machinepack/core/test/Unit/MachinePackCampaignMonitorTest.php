<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackCampaignMonitorTest extends TestCase
{
    /**
     * Test : Import contacts to subscriber list
     */
    public function testSubscribersImport()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $event = $this->_createSubscribersListEvent();

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test : Send transaction smart email
     */
    public function testTransactionSmartEmail()
    {
        MachinePack::init(__DIR__ . '/MachinePackConfigTest.crm.yml');

        $event = $this->_createTransactionSmartEmail();

        $results = MachinePack::send(
            'crmsync.create.campaignmonitor',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Creates a valid contact event
     */
    private function _createSubscribersListEvent()
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']     = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id']   = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/Event.action']                = 'subscribersList';
        $event['Intangible/Event.batch_limit']           = 100;
        $event['Intangible/CampaignMonitor.list_id']     = 'bcfc31e3faf8e42904e9d96f62d1db3d';
        $event['Intangible/CampaignMonitor.resubscribe'] = false;

        $testSubscribersList = [];
        for ($i = 1; $i <= 4; $i++) {
            $testSubscribersList[] = [
                'EmailAddress' => 'mptest' . $i . '@leafcutter.com.au',
                'Name' => 'Leafcutter Test ' . $i,
                'CustomFields' => [
                    ['Key' => 'testDate', 'Value' => date('d/m/Y H:i:s')]
                ],
                'ConsentToTrack' => 'Yes',
                'Resubscribe' => true
            ];
        }

        $event['Intangible/CampaignMonitor.subscribers'] = $testSubscribersList;
        $event['Intangible/Event.payload']               = [];

        return $event;
    }

    private function _createTransactionSmartEmail()
    {
        $event = array();
        $event['Intangible/CampaignMonitor.api_key']   = 'f30a882c3d4bb3b3128397a5fa65c1c4f2440a65bbd78613';
        $event['Intangible/CampaignMonitor.client_id'] = 'b342e3317aa0ea39cfb060c836455796';
        $event['Intangible/CampaignMonitor.smart_id']  = '2679fdab-5988-4a3a-bd78-f183a79ec4f5';
        $event['Intangible/Event.action']              = 'transactionSmartEmail';
        $event['EmailMessage.recipient.email']         = 'machinepack@leafcutter.com.au';
        $event['EmailMessage.cc']          = 'ccmachinepack@gmail.com';
        $event['EmailMessage.bcc']         = 'bccmachinepack@gmail.com';
        $event['Intangible/Event.payload'] = [
            'emailTitle'=>'Machine Pack Test Email'
        ];

        return $event;
    }
}
