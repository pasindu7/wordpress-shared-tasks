<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackSkyTest extends TestCase
{
    /**
     * Test : Send transaction smart email
     */
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /*  public function testUserDetailsEvent()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createUserDetailsEvent();

        $results = MachinePack::send(
            'crmsync.create.sky',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    private function _createUserDetailsEvent()
    {
        $event = array();
        $event['Intangible/Gift.appeal_id']   = '15';
        $event['Intangible/Gift.campaign_id'] = '50';
        $event['Intangible/Gift.fund_id']     = '4';
        $event['action'] = 'get_look_up';

        //$event['Intangible/Sky.code']     = 'dfe4dd5dde0340e68111e9f4dc9e995e';
        $event['Intangible/Sky.refresh_token'] = 'f47a5b8ed07b4f46bcef90cb8b41098a';
        // phpcs:disable
        $event['Intangible/Sky.access_token']  = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjREVjZzVkxIM0FtU1JTbUZqMk04Wm5wWHU3WSJ9.eyJuYW1laWQiOiJjOTAxMTIyMi0yODYxLTQ5ZGYtODM2YS03ZmJiYjJmMGJjODQiLCJhcHBsaWNhdGlvbmlkIjoiOTFkYzQ2MzgtMjk3Ny00N2Y1LWE2ZmQtZWI2YjNiOTQ5Y2Q4IiwiZW52aXJvbm1lbnRpZCI6InAtZklSdEFxNVM1MGlpZjV3WGNhY1h6QSIsImVudmlyb25tZW50bmFtZSI6IkdyZWF0IEJhcnJpZXIgUmVlZiBGb3VuZGF0aW9uIEVudmlyb25tZW50IDIiLCJsZWdhbGVudGl0eWlkIjoicC1QTTFOZEhXeWhFQ21kQXdWMUludm1BIiwibGVnYWxlbnRpdHluYW1lIjoiR3JlYXQgQmFycmllciBSZWVmIEZvdW5kYXRpb24iLCJ6b25lIjoicC1hdXMwMSIsImlzcyI6Imh0dHBzOi8vb2F1dGgyLnNreS5ibGFja2JhdWQuY29tLyIsImF1ZCI6ImJsYWNrYmF1ZCIsImV4cCI6MTU4ODU4MTQyNywibmJmIjoxNTg4NTc3ODI3fQ.KHaBdaYIuaha6ZawFqemeGVc_RXLeuWtf-9W6lYWayR3Ew4Sf7cG6zDg6K2VdHKwP1s2W0PAOg-DvgAZ_rn4khr7BIah5kgK4PiYRLj5AgHIB96tI6v-06s4wud0O05Ete54taXNPyZuMyUNwfhzaUe2tQ2c7F98cwSNU_KL4lwpTc0JueLCop1BiH1v5ugrLhTrD_gcisuiJhRC2dRqDbj2rn9oF9hq0KrGDyct1wcelk9NrvbZhNZ4xFY8a7uFjtH5Zs1Bx0piUfjdWaxVSIZ7jZT2EeaaYy31_Z3Im0XzF4tp0yQH9BsxsMxHPNGYcJ9HPClUa-PWl77UyEG5uQ';
        // phpcs:enable
        
        $event['Intangible/Event.payload'] = [
            // Type -> Individual or Organisation
            'type'                  => 'Individual',
            'address_first_name'    => 'John',
            'address_last_name'     => 'Doe',
            'address_email'         => 'testuser@gmail.com',
            'address_phone'         => '+610415415555',
            //Phone Type -> Business, Direct, Home, Mobile, Primary Voice, Voice
            'phone_type'            => 'Home',
            'address_street'        => '272 Pacific Highway',
            'address_suburb'        => 'Crows Nest',
            'address_state'         => 'NSW',
            'address_postcode'      => '2065',
            'address_country'       => 'Australia',
            //Installment frequency of the recurring gift to add.
            //Available values are WEEKLY, EVERY_TWO_WEEKS, EVERY_FOUR_WEEKS, MONTHLY, QUARTERLY, ANNUALLY.
            'donation_frequency'    => 'MONTHLY',
            'donation_amount'       => '121.23',
            'transaction_id'        => '123415624135',
            'reason'                => 'Approved',
            // Payment method -> Cash, Cheque, CreditCard, DebitCard, Other
            'gift_payment_method'   => 'CreditCard',
            // Payment Type "Donation" (OneTime), "RecurringGift", and "RecurringGiftPayment"
            // Not working for recurring gifts / Couldn't find any object in API to add CC details
            // Could be issue with Post date and credit card details
            'gift_payment_type'     => 'Donation',
            'gift_reference'        => 'Newly added gift',
            'payment_date'          => '2020-12-16',
            'lookup_id'
        ];

        return $event;
    }
}
