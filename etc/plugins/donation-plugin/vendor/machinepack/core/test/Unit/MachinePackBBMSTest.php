<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackBBMSTest extends TestCase
{
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /*public function testProcessPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createUserDetailsEvent();

        $event['action'] = 'process_payment';

        $results = MachinePack::send(
            'payment.create.bbms',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }*/

    private function _createUserDetailsEvent()
    {
        $event = array();
        $event['Intangible/Gift.appeal_id']   = '15';
        $event['Intangible/Gift.campaign_id'] = '50';
        $event['Intangible/Gift.fund_id']     = '4';
        $event['action'] = 'process_payment';

        $event['Intangible/Sky.code']          = '48d69baa3dc44e8ba2a190b561d93872';
        $event['Intangible/Sky.refresh_token'] = '80ce9cc1130c4266b47520a0e2b8783b';
        // phpcs:disable
        $event['Intangible/Sky.access_token']  = '1eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjREVjZzVkxIM0FtU1JTbUZqMk04Wm5wWHU3WSJ9.eyJuYW1laWQiOiIxMmQ4ZGMwZC0yM2QzLTQyMDctYTMwOS0wMDIwM2NhZTllYjkiLCJhcHBsaWNhdGlvbmlkIjoiZWFmZDYyZmQtOTA3Mi00ZmJkLWE4NDctZDBlN2FmZmNjYjYyIiwiZW52aXJvbm1lbnRpZCI6InAtbGlDTUxzSjAxa3lRWHhKdENZQ1hsUSIsImVudmlyb25tZW50bmFtZSI6IlByb3BlcnR5IEluZHVzdHJ5IEZvdW5kYXRpb24gRW52aXJvbm1lbnQgMSIsImxlZ2FsZW50aXR5aWQiOiJwLWE4RGt0c25qb0VDU2VLbmtOOF81dmciLCJsZWdhbGVudGl0eW5hbWUiOiJQcm9wZXJ0eSBJbmR1c3RyeSBGb3VuZGF0aW9uIiwiem9uZSI6InAtYXVzMDEiLCJpc3MiOiJodHRwczovL29hdXRoMi5za3kuYmxhY2tiYXVkLmNvbS8iLCJhdWQiOiJibGFja2JhdWQiLCJleHAiOjE1ODkyNjE5NjIsIm5iZiI6MTU4OTI1ODM2Mn0.B48B9fAM53VVdlBwO4Ls1qPNNIp2nET1QQsryurscqHCMf-kMCkgsXkyrkXbTTZegGXQHbWw9CInBb7JhyDNGUXKU_cO_43dcFZU4cUfyHXgK5s2cLYl31gVEfuD7ydie58wIj7Vjq1803rd-8LYOdY2GCDjhnXyMA2yrVwJ724pWgqJkobrhFX3mR-mD_cDQbF6iSDm9f0DrMjidmvEsWiZJURU2BuNnkqGj-zmPJ669HgcIIdoBrtrD8lg3_aZ6k_ZRKDJRIYmrivyhb-1qDXV3LxnkQA6D90CDQDqP-uProIEzRuHwcJbFrndIBD_bJ6l4vQhJIqgLs9vmdn5QA';
        // phpcs:enable

        $event['Person.givenName']              = 'Pradeep One Off';
        $event['Person.familyName']             = 'Doe';
        $event['Person.telephone']              = 0404000000;
        $event['Person.email']                  = 'test@leafcutter.com.au';
        $event['PostalAddress.streetAddress']   = '123 Nowhere St';
        $event['PostalAddress.addressLocality'] = 'Crows Nest';
        $event['PostalAddress.addressRegion']   = 'NSW';
        $event['PostalAddress.postalCode']      = '2065';
        $event['PostalAddress.addressCountry']  = 'au';
        $event['CreditCard/CardDetails.name']   = 'Pradeep W';
        $event['CreditCard/CardDetails.number'] = '4111111111111111';
        $event['CreditCard/CardDetails.expiryMonth'] = '12';
        $event['CreditCard/CardDetails.expiryYear']  = '2025';
        $event['CreditCard/CardDetails.cvn']         = '123';
        $event['MonetaryAmount.value']               = 150.00;
        $event['MonetaryAmount.currency']            = 'AUD';

        $event['Intangible/Event.payload'] = [
            // Type -> Individual or Organisation
            'type'                  => 'Individual',
            'address_first_name'    => 'John',
            'address_last_name'     => 'Doe',
            'address_email'         => 'testuser@gmail.com',
            'address_phone'         => '+610415415555',
            //Phone Type -> Business, Direct, Home, Mobile, Primary Voice, Voice
            'phone_type'            => 'Home',
            'address_street'        => '272 Pacific Highway',
            'address_suburb'        => 'Crows Nest',
            'address_state'         => 'NSW',
            'address_postcode'      => '2065',
            'address_country'       => 'Australia',
            //Installment frequency of the recurring gift to add.
            //Available values are WEEKLY, EVERY_TWO_WEEKS, EVERY_FOUR_WEEKS, MONTHLY, QUARTERLY, ANNUALLY.
            'donation_frequency'    => 'MONTHLY',
            'donation_amount'       => '121.23',
            'transaction_id'        => '123415624135',
            'reason'                => 'Approved',
            // Payment method -> Cash, Cheque, CreditCard, DebitCard, Other
            'gift_payment_method'   => 'CreditCard',
            // Payment Type "Donation" (OneTime), "RecurringGift", and "RecurringGiftPayment"
            // Not working for recurring gifts / Couldn't find any object in API to add CC details
            // Could be issue with Post date and credit card details
            'gift_payment_type'     => 'Donation',
            'gift_reference'        => 'Newly added gift',
            'payment_date'          => '2020-12-16',
            'lookup_id'
        ];

        return $event;
    }
}
