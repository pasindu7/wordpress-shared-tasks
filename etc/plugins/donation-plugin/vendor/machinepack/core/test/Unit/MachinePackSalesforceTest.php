<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackSalesforceTest extends TestCase
{
    /**
     * Test creating contact object
     */
    public function testSalesforceObjectCreation()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createSalesforceEvent();

        $results = MachinePack::send(
            'crmsync.create.salesforce',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Creates a valid contact event
     */
    private function _createSalesforceEvent()
    {
        $event = array();
        $event = array(
            "search" => array(
                'method'    => 'GET',
                'query'     => "Select+Id+From+Contact+Where+(npe01__HomeEmail__c='dev@leafcutter.com.au'
                +AND+FirstName='John'+AND+LastName='Doe')",
                'Intangible/Event.payload' => array()
            ),
            "createContact" => array(
                'method'  => 'POST',
                'sobject' => 'Contact',
                'Intangible/Event.payload' => array(
                    'FirstName'                     => 'John',
                    'LastName'                      => 'Doe',
                    'MobilePhone'                   => '0400000000',
                    'HomePhone'                     => '',
                    'npe01__PreferredPhone__c'      => 'Mobile',
                    'npe01__Primary_Address_Type__c'  => 'Home',
                    'MailingStreet'                   => '272 pacific highway crows nest',
                    'MailingCity'                   => 'Crows',
                    'MailingState'                  => 'NSW',
                    'MailingPostalCode'             => '2065',
                    'MailingCountry'                => 'Australia',
                    'npe01__HomeEmail__c'           => 'dev@leafcutter.com.au',
                    'npe01__Preferred_Email__c'     => 'Personal',
                    'Stripe_Customer_ID__c'         => '1234'
                )
            ),
            "createOpportunity" => array(
                'method'  => 'POST',
                'sobject' => 'Opportunity',
                'Intangible/Event.payload' => array(
                        'StageName'                         => 'Open',
                        'Type'                              => 'Once-Off Gift',
                        'Amount'                            => '1.00',
                        'PD_Payment_Method__c'              => 'Web',
                        'CampaignId'                        => '7012N000000IFyPQAW',
                        'RecordTypeId'                      => '01228000000b0OcAAI',
                        'Name'                              => 'Web Donation ' . date('d/m/Y'),
                        'CloseDate'                         => date('c'),
                        'Description'                       => 'This is the description',
                        'Platform_Donation_ID__c'           => 'ch_1GpthiHL0sT6xNX26X94QBFP',
                        'Stripe_Card_ID__c'                 => 'card_1GpthbHL0sT6xNX2mUQdSWDF',
                        'Stripe_Fingerprint__c'             => 'hA7GIo8NbK6YEzQa',
                )
            )
        );
        $event['Intangible/Event.payload'] = '';

        return $event;
    }
}
