<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackQuickStreamTest extends TestCase
{
    /**
     * Test a valid one-off credit card payment
     */
    public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');
        $event   = $this->_createValidPaymentEvent();
        $results = MachinePack::send(
            'purchase.create.quickstream',
            $event
        );
        
        $firstResult = array_pop($results);
        
        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test an invalid one-off credit card payment
     */
    public function testInvalidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['CreditCard/CardDetails.number'] = '3333333333333333';
        $event['MonetaryAmount.value']          = 151;

        $results = MachinePack::send(
            'purchase.create.quickstream',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Failure::class, $firstResult);
    }

    /**
     * Test a valid recurring credit card payment
     */
    public function testValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product/Subscription.duration'] = '31/12/2050';

        $results = MachinePack::send(
            'subscription.create.quickstream',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Creates a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array();
        $event['MonetaryAmount.value']        = 12;
        $event['MonetaryAmount.currency']     = 'AUD';
        $event['Order.identifier']            = uniqid();
        $event['Order.description']           = 'Test transaction';
        $event['Order.recurring']             = 'MONTHLY';
        $event['Person.givenName']            = 'John2';
        $event['Person.familyName']           = 'Doe';
        $event['Person.telephone']            = '0785214652';
        $event['Person.email']                = 'dev111@leafcutter.com.au';
        $event['Person.ip']                   = '127.0.0.1';
        $event['Person.notification']         = 'EMAIL';
        $event['PostalAddress.streetAddress'] = '272 Pacific Highway';
        $event['PostalAddress.addressLocality']      = 'Crows Nest';
        $event['PostalAddress.addressRegion']        = 'NSW';
        $event['PostalAddress.postalCode']           = '2065';
        $event['PostalAddress.addressCountry']       = 'AU';
        $event['CreditCard/CardDetails.name']        = 'John Doe';
        $event['CreditCard/CardDetails.number']      = '4111111111111111';
        $event['CreditCard/CardDetails.expiryMonth'] = '11';
        $event['CreditCard/CardDetails.expiryYear']  = '2020';
        $event['CreditCard/CardDetails.cvn']         = '123';
        $event['CreditCard/CardDetails.type']        = 'Visa';
        $event['Product.productId']                  = uniqid();
        
        return $event;
    }
}
