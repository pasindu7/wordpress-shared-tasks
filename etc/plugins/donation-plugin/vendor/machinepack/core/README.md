# MachinePack Core

This package contains all core functionality related to free MachinePacks.

Please visit our [wiki](https://bitbucket.org/leafcuttercreativedigital/machinepack-core/wiki/Home) for more information.

# How to run tests using the Docker image
```
docker run --rm -it -v $(pwd):/app/ machinepack/core-php bash
cd app
cp test/Unit/MachinePackTest.env.yml test/Unit/MachinePackTest.yml
composer install
apt update && apt install entr
bin/watch.sh
```

# How to run tests for your module only

```
find src/ test/ -name '*.php' | vendor/bin/phpunit --filter 'MachinePack\\Core\\Test\\Unit\\MachinePackEtapestryTest'
```

#How to run tests on Windows

```
Check /bin/win/test.php file for instructions
Run command php ./bin/win/test.php
```

# Updating the core test image

```
docker build bin/ -f ./Dockerfile -t machinepack/core-php:latest --pull \
&& docker push machinepack/core-php:latest
```
