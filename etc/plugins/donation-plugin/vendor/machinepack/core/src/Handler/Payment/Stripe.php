<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class Stripe extends Handler
{
    const TOKEN_FIELD             = 'Intangible/StripePayment.token';
    const EMAIL_FIELD             = 'Person.email';
    const PRODUCT_ID_FIELD        = 'Product.productId';
    const MACHINEPACK_CUSTOMER_ID = 'Intangible/Customer.id';

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        if (empty($event[Stripe::TOKEN_FIELD])) {
            return new Failure(
                sprintf(
                    'Missing payment token. Please send "%s" with your approved payment token.',
                    Stripe::TOKEN_FIELD
                )
            );
        }

        $config = $this->settings[$this->settings['env']];

        if (empty($config['secret'])) {
            return new Failure(
                'Please add handler settings for Stripe secret key. Full path should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                key: "pk_...",
                                secret: "sk_..."
                            }
                        }
                    }
                '
            );
        }

        try {
            \Stripe\Stripe::setApiKey($config['secret']);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(sprintf('Unable to initialise Stripe ("%s")', $e));
        }

        try {
            $customer = $this->_createStripeCustomer($event);
            if ($customer instanceof Failure) {
                return $customer;
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create Customer record with email "%s" ("%s")',
                    $event[Stripe::EMAIL_FIELD],
                    $e
                )
            );
        }

        if ($event instanceof Subscription) {
            return $this->_processSubscription($event, $customer);
        }

        return $this->_processPayment($event, $customer);
    }

    /**
     * Set up a Stripe client
     * @param  Payment $event event data
     * @return \Stripe\Customer
     */
    private function _createStripeCustomer(Payment $event)
    {
        if (!$event[Stripe::MACHINEPACK_CUSTOMER_ID]) {
            try {
                $customer = \Stripe\Customer::create(
                    [
                    'description' => sprintf(
                        "%s %s <%s>",
                        $event['Person.givenName'],
                        $event['Person.familyName'] ?? '',
                        $event[Stripe::EMAIL_FIELD]
                    ),
                    'email' => $event[Stripe::EMAIL_FIELD],
                    'source'  => $event[Stripe::TOKEN_FIELD],
                    'name' => sprintf("%s %s", $event['Person.givenName'], $event['Person.familyName'] ?? ''),
                    'phone' => $event['Person.telephone'] ?? ''
                    ]
                );
                return $customer;
            } catch (\Exception $e) {
                return new Failure('Error: ' . $e->getMessage());
            }
        } else {
            $customer_id = $event[Stripe::MACHINEPACK_CUSTOMER_ID];
        }
    }

    /**
     * handle a single payment request
     * @param  Payment $event    payment information
     * @param  \Stripe\Customer  $customer stripe customer data
     * @return Success|Failure
     */
    private function _processPayment(Payment $event, $customer)
    {
        try {
            $charge = \Stripe\Charge::create(
                [
                    'customer' => $customer->id,
                    'amount'   => round($event['MonetaryAmount.value'] * 100.0),
                    'currency' => $event['MonetaryAmount.currency'],
                ]
            );
            if (isset($charge->status) && $charge->status === 'succeeded') {
                return new Success(
                    [
                        'MoneyTransfer.identifier'                      => $charge->id,
                        'MoneyTransfer.token'                           => $event[Stripe::TOKEN_FIELD],
                        'Intangible/APIResponse.APIResponseCode'        => $charge->status,
                        'Intangible/Customer.id'                        => $charge->customer,
                        'Intangible/Payment.id'                         => $charge->source->id,
                        'Intangible/Payment.card.last4'                 => $charge->source->last4,
                        'Intangible/Payment.card.fingerprint'           => $charge->source->fingerprint
                    ]
                );
            } else {
                return new Failure(
                    'Unable to charge customer',
                    [
                        $charge
                    ]
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to charge customer "%s" total of %.2f %s ("%s")',
                    $event[Stripe::EMAIL_FIELD],
                    $event['MonetaryAmount.value'],
                    $event['MonetaryAmount.currency'],
                    $e
                )
            );
        }
    }

    /**
     * handle a subscription. it must have a product id for this to work
     * @param  Subscription $event    subscription information
     * @param  \Stripe\Customer $customer stripe customer data
     * @return Success|Failure
     */
    private function _processSubscription(Subscription $event, $customer)
    {
        try {
            $subscription = \Stripe\Subscription::create(
                [
                    'customer' => $customer->id,
                    'items' => [['plan' => $event[Stripe::PRODUCT_ID_FIELD]]],
                ]
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to charge customer "%s" total of %.2f %s ("%s")',
                    $event[Stripe::EMAIL_FIELD],
                    $event['MonetaryAmount.value'],
                    $event['MonetaryAmount.currency'],
                    $e
                )
            );
        }
        return new Success(
            sprintf(
                'Successfully charged %s total of %.2f %s',
                $event[Stripe::EMAIL_FIELD],
                $event['MonetaryAmount.value'],
                $event['MonetaryAmount.currency']
            )
        );
    }
    
    
    /**
     * Create a card token for recurring payments
     * @param  Payment $event    payment information
     * @param  \Stripe\Token  $cardtoken stripe customer data
     * @return Success|Failure
     */
    private function _createCardToken(\MachinePack\Core\Event\Event $event)
    {
        
        try {
            $cardtoken = \Stripe\Token::create(
                [
                    'card' => [
                        'cardNumber' => $event['CreditCard/CardDetails.number'],
                        'expiryDateMonth' => $event['CreditCard/CardDetails.expiryMonth'],
                        'expiryDateYear' =>$event['CreditCard/CardDetails.expiryYear'],
                        'cvn' => $event['CreditCard/CardDetails.cvn']
                    ],
                ]
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                sprintf(
                    'Unable to create customer token with card ("%s")',
                    $e
                )
            );
        }
        return new Success(
            [
                'token' => $cardtoken
            ]
        );
    }

    public function getPaymentSettings()
    {
        return [
            'stripe' => $this->settings[$this->settings['env']]
        ];
    }
}
