<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;

class CampaignMonitor extends Handler
{
    private $_config;
    private $_clientId;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        if (empty($event['Intangible/CampaignMonitor.api_key'])
            || empty($event['Intangible/CampaignMonitor.client_id'])
        ) {
            //validate configuration variables
            if (empty($this->_config['apiKey']) || empty($this->_config['clientId'])
            ) {
                return new Failure(
                    'Please provide apiKey and ClientId or add settings for Campaign Monitor. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                apiKey:
                                clientId:
                            }
                        }
                    }
                '
                );
            } else {
                $this->_clientId = $this->_config['clientId'];
            }
        } else {
            $this->_clientId = $event['Intangible/CampaignMonitor.client_id'];
        }

        if (!isset($event['Intangible/Event.action'])) {
            return new Failure('Error:: Message - Event action not provided.');
        }

        if ($event['Intangible/Event.action'] == 'subscribersList') {
            return $this->_cmSubscribesList($event);
        }

        if ($event['Intangible/Event.action'] == 'transactionSmartEmail') {
            return $this->_cmTransactionSmartEmail($event);
        }

        return new Failure('Error:: Message - '.$event['Intangible/Event.action'].' action is not supported.');
    }

    /**
     * Submit the subscribe event payload to Campaign Monitor
     * @param Crmsync $event
     */
    private function _cmSubscribesList(Crmsync $event)
    {
        if (!isset($event['Intangible/CampaignMonitor.list_id'])) {
            return new Failure('Error:: Message - Incorrect CM list ID.');
        }

        if (!isset($event['Intangible/CampaignMonitor.subscribers'])) {
            return new Failure('Error:: Message - There are no subscribers for import.');
        }

        $batchLimit = 100;
        if (isset($event['Intangible/Event.batch_limit']) && $event['Intangible/Event.batch_limit'] > 0) {
            $batchLimit = $event['Intangible/Event.batch_limit'];
        }

        $batches            = ceil(count($event['Intangible/CampaignMonitor.subscribers']) / $batchLimit);
        $subscribeCmRequest = new \CS_REST_Subscribers(
            $event['Intangible/CampaignMonitor.list_id'],
            $this->_getAuthTokens($event)
        );
        if (isset($event['Intangible/CampaignMonitor.resubscribe'])) {
            $resuscribe = $event['Intangible/CampaignMonitor.resubscribe'];
        } else {
            $resuscribe = false;
        }

        $errors = [];
        foreach (range(1, $batches) as $batch) {
            $bData  = array_slice(
                $event['Intangible/CampaignMonitor.subscribers'],
                (($batch - 1) * $batchLimit),
                $batchLimit
            );
            $result = $subscribeCmRequest->import($bData, $resuscribe);
            if (!$result->was_successful()) {
                $errors[] = isset($result->response->Message) ? $result->response->Message : 'no-message';
            }
        }

        if (!empty($errors)) {
            return new Failure('Error:: Message - Error while processing some of the batches : '. json_encode($errors));
        } else {
            return new Success('All records imported successfully.');
        }
    }

    /**
     * Submit the transaction email event payload to Campaign Monitor
     * @param Crmsync $event
     */
    private function _cmTransactionSmartEmail(Crmsync $event)
    {
        if (!isset($event['Intangible/CampaignMonitor.smart_id'])) {
            return new Failure('Error:: Message - Incorrect Transaction Smart Email ID.');
        }

        if (!isset($event['EmailMessage.recipient.email'])
            || !filter_var($event['EmailMessage.recipient.email'], FILTER_VALIDATE_EMAIL)
        ) {
            return new Failure('Error:: Message - Incorrect recipient email.');
        }

        if (isset($event['Intangible/Event.payload'])
            && !is_array($event['Intangible/Event.payload'])
        ) {
            return new Failure('Error:: Message - variables must be in array.');
        }


        $smartId = $event['Intangible/CampaignMonitor.smart_id'];
        $request = new \CS_REST_Transactional_SmartEmail($smartId, $this->_getAuthTokens($event), $this->_clientId);

        $emailData = [
            'To' => $event['EmailMessage.recipient.email'],
            'Data' => $event['Intangible/Event.payload']
        ];

        if (isset($event['EmailMessage.cc']) && filter_var($event['EmailMessage.cc'], FILTER_VALIDATE_EMAIL)) {
            $emailData['CC'] = $event['EmailMessage.cc'];
        }

        if (isset($event['EmailMessage.bcc']) && filter_var($event['EmailMessage.bcc'], FILTER_VALIDATE_EMAIL)) {
            $emailData['BCC'] = $event['EmailMessage.bcc'];
        }

        $result = $request->send(
            $emailData,
            isset($event['Intangible/Event.consent_to_track']) ? $event['Intangible/Event.consent_to_track'] : 'No',
            isset($event['Intangible/Event.add_to_list']) ? $event['Intangible/Event.add_to_list'] : false
        );

        $errors = [];
        if (!$result->was_successful()) {
            $errors[] = isset($result->response->Message) ? $result->response->Message : 'no-message';
        }

        if (!empty($errors)) {
            //TODO TEMP SOLUTION
            return new Success('Valid response received but email has not been sent.');
            //return new Failure('Error:: Message - Error while processing request : '. json_encode($errors));
        } else {
            return new Success('Email has been successfully sent.');
        }
    }

    private function _getAuthTokens(Crmsync $event)
    {
        if (!empty($event['Intangible/CampaignMonitor.api_key'])) {
            return [
                'api_key' => $event['Intangible/CampaignMonitor.api_key']
            ];
        } else {
            return [
                'api_key' => $this->_config['apiKey']
            ];
        }
    }
}
