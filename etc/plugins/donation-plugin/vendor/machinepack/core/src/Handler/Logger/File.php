<?php
namespace MachinePack\Core\Handler\Logger;

use MachinePack\Core\Event\Event;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Exception\Settings as SettingsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Result;

class File extends Handler
{
    public function handleEvent(Event $event): Result
    {
        if (empty($this->settings['file'])) {
            throw new SettingsException('Please provide a log file in settings');
        }

        $fh = fopen($this->settings['file'], 'a+');

        if ($fh === false) {
            throw new SettingsException(
                sprintf(
                    'Log file "%s" is not writeable.',
                    $this->settings['file']
                )
            );
        }

        fprintf(
            $fh,
            "%s:\t[%s]\t[%s]%s",
            date('c'),
            get_class($event),
            strval($event),
            PHP_EOL
        );

        fclose($fh);

        return new Success();
    }
}
