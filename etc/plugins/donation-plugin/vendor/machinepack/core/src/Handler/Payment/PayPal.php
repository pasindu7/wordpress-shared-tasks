<?php

namespace MachinePack\Core\Handler\Payment;

use Exception;

use MachinePack\Core\Event\Event;
use MachinePack\Core\Event\Events\Subscription;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Success;

use PayPal\CoreComponentTypes\BasicAmountType;
use PayPal\EBLBaseComponents\BillingAgreementDetailsType;
use PayPal\EBLBaseComponents\BillingPeriodDetailsType;
use PayPal\EBLBaseComponents\CreateRecurringPaymentsProfileRequestDetailsType;
use PayPal\EBLBaseComponents\DoExpressCheckoutPaymentRequestDetailsType;
use PayPal\EBLBaseComponents\PaymentDetailsItemType;
use PayPal\EBLBaseComponents\PaymentDetailsType;
use PayPal\EBLBaseComponents\RecurringPaymentsProfileDetailsType;
use PayPal\EBLBaseComponents\ScheduleDetailsType;
use PayPal\EBLBaseComponents\ActivationDetailsType;
use PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType;

use PayPal\Exception\PPConfigurationException;
use PayPal\Exception\PPConnectionException;
use PayPal\Exception\PPInvalidCredentialException;
use PayPal\Exception\PPMissingCredentialException;

use PayPal\PayPalAPI\CreateRecurringPaymentsProfileReq;
use PayPal\PayPalAPI\CreateRecurringPaymentsProfileRequestType;
use PayPal\PayPalAPI\DoExpressCheckoutPaymentReq;
use PayPal\PayPalAPI\DoExpressCheckoutPaymentRequestType;
use PayPal\PayPalAPI\GetExpressCheckoutDetailsReq;
use PayPal\PayPalAPI\GetExpressCheckoutDetailsRequestType;
use PayPal\PayPalAPI\SetExpressCheckoutReq;
use PayPal\PayPalAPI\SetExpressCheckoutRequestType;

use PayPal\PayPalAPI\SetExpressCheckoutResponseType;
use PayPal\Service\PayPalAPIInterfaceServiceService;

class PayPal extends Handler
{
    private $_config;

    public function handleEvent(Event $event): Result
    {

        if (!$event instanceof Payment) {
            return new Ignored;
        }

        $this->_config = $this->settings[$this->settings['env']];

        if (empty($this->_config['clientId']) || empty($this->_config['clientUserName'])
            || empty($this->_config['clientPassword']) || empty($this->_config['clientSignature'])
        ) {
            return new Failure(
                'Please add handler settings for PayPal. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                clientId: "...",
                                clientUserName: "...",
                                clientPassword: "..."
                                clientSignature: "..."
                            }
                        }
                    }
                '
            );
        }

        if (!isset($event['Payment.identifier'])) {
            if ($event instanceof Subscription) {
                return $this->_processPayment($event, 'RecurringPayments');
            }
            return $this->_processPayment($event, 'MerchantInitiatedBilling');
        } else {
            try {
                return $this->_handleReturn($event);
            } catch (Exception $e) {
                return new Failure($e->getMessage());
            }
        }
    }

    /**
     * @param Event $event
     * @param $billingAgreementDetailsType
     * @param $billingAgreementDescription
     * @return Failure|Success|SetExpressCheckoutResponseType
     */
    private function _processPayment(Event $event, $billingAgreementDetailsType)
    {
        $service = $this->_returnAPIService();

        $paymentDetails = $this->_setupPaymentDetails($event);

        $setECReqDetails                    = new SetExpressCheckoutRequestDetailsType();
        $setECReqDetails->PaymentDetails[0] = $paymentDetails;
        $setECReqDetails->CancelURL         = $event['Intangible/PayPal.cancelUrl'];
        $setECReqDetails->ReturnURL         = $event['Intangible/PayPal.returnUrl'];
        $setECReqDetails->NoShipping        = $event['Intangible/PayPal.shipping'];

        if ($billingAgreementDetailsType == 'RecurringPayments') {
            $billingAgreementDetails = new BillingAgreementDetailsType($billingAgreementDetailsType);
            $billingAgreementDetails->BillingAgreementDescription = $event['Order.description'];

            $setECReqDetails->BillingAgreementDetails = array($billingAgreementDetails);
        }

        $setECReqDetails->BrandName = $event['Brand.name'];
        $setECReqDetails->AllowNote = $event['Intangible/PayPal.note'];

        $setECReqType = new SetExpressCheckoutRequestType();
        $setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;

        $setECReq = new SetExpressCheckoutReq();
        $setECReq->SetExpressCheckoutRequest = $setECReqType;

        try {
            $setECResponse = $service->SetExpressCheckout($setECReq);
        } catch (Exception $ex) {
            $ex_detailed_message = $ex->getMessage();
            if ($ex instanceof PPConnectionException) {
                $ex_detailed_message = "Error connecting to " . $ex->getUrl();
            } else {
                if ($ex instanceof PPMissingCredentialException || $ex instanceof PPInvalidCredentialException) {
                    $ex_detailed_message = $ex->errorMessage();
                } else {
                    if ($ex instanceof PPConfigurationException) {
                        $ex_detailed_message = "Invalid configuration. Please check your configuration file";
                    }
                }
            }
            return new Failure($ex_detailed_message);
        }

        if (isset($setECResponse->Errors) && count($setECResponse->Errors) > 0) {
            if ($setECResponse->Ack === "SuccessWithWarning") {
                MachinePack::log('Paypal Success Warning: ' . $setECResponse->Errors[0]->LongMessage, 'warning');
                return new Success('Paypal Exception: ' . $setECResponse->Errors[0]->LongMessage);
            } else {
                MachinePack::log('Paypal Exception: ' . $setECResponse->Errors[0]->LongMessage, 'error');
                return new Failure('Paypal Exception: ' . $setECResponse->Errors[0]->LongMessage);
            }
        }

        if (is_null($setECResponse->Token)) {
            MachinePack::log('Paypal Exception.', 'error');
            return new Failure('Paypal exception, no token');
        }

        return new Success($setECResponse);
    }

    private function _setupPaymentDetails($event)
    {
        $paymentDetails = new PaymentDetailsType();
        /**
         * Most currencies require two decimal places.
         * The decimal separator must be a period (.), and the optional thousands separator must be a comma (,).
         */
        $itemTotalValue = number_format($event['MonetaryAmount.value'], 2, '.', ',');

        $itemDetails         = new PaymentDetailsItemType();
        $itemDetails->Name   = $event['Order.name'];
        $itemDetails->Amount = new BasicAmountType($event['MonetaryAmount.currency'], $itemTotalValue);

        $paymentDetails->PaymentDetailsItem[] = $itemDetails;
        $paymentDetails->ItemTotal            = new BasicAmountType($event['MonetaryAmount.currency'], $itemTotalValue);
        $paymentDetails->OrderTotal           = new BasicAmountType($event['MonetaryAmount.currency'], $itemTotalValue);
        $paymentDetails->PaymentAction        = 'Sale';

        return $paymentDetails;
    }

    /**
     * @return PayPalAPIInterfaceServiceService
     */
    private function _returnAPIService()
    {
        return new PayPalAPIInterfaceServiceService(
            [
                'mode' => $this->_getPayPalMode(),
                'acct1.UserName' => $this->_config['clientUserName'],
                'acct1.Password' => $this->_config['clientPassword'],
                'acct1.Signature' => $this->_config['clientSignature'],
                'log.FileName' => $this->_config['logFile'],
                'log.LogLevel' => 'INFO',
                'log.LogEnabled' => $this->_getPayPalMode() == 'sandbox' ? true : false
            ]
        );
    }


    /**
     * @param $event
     * @return mixed
     * @throws Exception
     */
    private function _handleReturn($event)
    {
        try {
            $service = $this->_returnAPIService();

            $DoECResponse = null;

            if ($event['Payment.type'] == 'one-off') {
                $DoECReq      = $this->_buildOnceOff($event);
                $DoECResponse = $service->DoExpressCheckoutPayment($DoECReq);
            } elseif ($event['Payment.type'] == 'recurring') {
                $createRPProfileReq = $this->_buildRPProfileReq($event);
                $DoECResponse       = $service->CreateRecurringPaymentsProfile($createRPProfileReq);
            } else {
                return new Failure("Transaction type not set");
            }

            if (isset($DoECResponse->Errors) && count($DoECResponse->Errors) > 0) {
                if ($DoECResponse->Ack === "SuccessWithWarning") {
                    return new Failure(
                        'Paypal Success Warning: ' . $DoECResponse->Errors[0]->LongMessage,
                        ['response' => $DoECResponse]
                    );
                } else {
                    return new Failure('Paypal Exception: ' . $DoECResponse->Errors[0]->LongMessage);
                }
            }

            if ($DoECResponse->Ack === 'Success' || $DoECResponse->Ack === 'SuccessWithWarning') {
                return new Success($DoECResponse);
            }
        } catch (Exception $ex) {
            $ex_detailed_message = $ex->getMessage();
            if ($ex instanceof PPConnectionException) {
                $ex_detailed_message = "Error connecting to " . $ex->getUrl();
            } else {
                if ($ex instanceof PPMissingCredentialException || $ex instanceof PPInvalidCredentialException) {
                    $ex_detailed_message = $ex->errorMessage();
                } else {
                    if ($ex instanceof PPConfigurationException) {
                        $ex_detailed_message = "Invalid configuration. Please check your configuration file";
                    }
                }
            }
            return new Failure($ex_detailed_message);
        }
    }


    /**
     * @param $event
     * @return CreateRecurringPaymentsProfileReq
     */
    private function _buildRPProfileReq($event)
    {
        $RPProfileDetails                   = new RecurringPaymentsProfileDetailsType();
        $RPProfileDetails->BillingStartDate = date("Y-m-d\TH:i:s\Z", strtotime('+2 minute'));
        if (isset($event['startDate'])) {
            $RPProfileDetails->BillingStartDate = $event['startDate'];
        }

        $paymentBillingPeriod                   = new BillingPeriodDetailsType();
        $paymentBillingPeriod->BillingFrequency = $event['Billing.frequency'];
        $paymentBillingPeriod->BillingPeriod    = $event['Billing.period'];
        $paymentBillingPeriod->Amount           = new BasicAmountType(
            $event['MonetaryAmount.currency'],
            $event['MonetaryAmount.value']
        );

        $scheduleDetails                = new ScheduleDetailsType();
        $scheduleDetails->Description   = $event['Order.description'];
        $scheduleDetails->PaymentPeriod = $paymentBillingPeriod;

        if (isset($event['MonetaryAmount.initialAmount'])) {
            $activationDetails                  = new ActivationDetailsType();
            $activationDetails->InitialAmount   = new BasicAmountType(
                $event['MonetaryAmount.currency'],
                $event['MonetaryAmount.initialAmount']
            );
            $scheduleDetails->ActivationDetails = $activationDetails;
        }

        $createRPProfileRequestDetail                  = new CreateRecurringPaymentsProfileRequestDetailsType();
        $createRPProfileRequestDetail->Token           = $event['Order.token'];
        $createRPProfileRequestDetail->ScheduleDetails = $scheduleDetails;
        $createRPProfileRequestDetail->RecurringPaymentsProfileDetails = $RPProfileDetails;

        $createRPProfileRequest = new CreateRecurringPaymentsProfileRequestType();
        $createRPProfileRequest->CreateRecurringPaymentsProfileRequestDetails = $createRPProfileRequestDetail;

        $createRPProfileReq = new CreateRecurringPaymentsProfileReq();
        $createRPProfileReq->CreateRecurringPaymentsProfileRequest = $createRPProfileRequest;

        return $createRPProfileReq;
    }


    /**
     * @param $event
     * @return DoExpressCheckoutPaymentReq
     */
    private function _buildOnceOff($event)
    {
        $getExpressCheckoutDetailsRequest = new GetExpressCheckoutDetailsRequestType($event['Order.token']);

        $getExpressCheckoutReq = new GetExpressCheckoutDetailsReq();

        $getExpressCheckoutReq->GetExpressCheckoutDetailsRequest = $getExpressCheckoutDetailsRequest;

        $paymentDetails = new PaymentDetailsType();

        $paymentDetails->OrderTotal    = new BasicAmountType(
            $event['MonetaryAmount.currency'],
            $event['MonetaryAmount.value']
        );
        $paymentDetails->ItemTotal     = new BasicAmountType(
            $event['MonetaryAmount.currency'],
            $event['MonetaryAmount.value']
        );
        $paymentDetails->PaymentAction = 'Sale';

        $DoECRequestDetails                    = new DoExpressCheckoutPaymentRequestDetailsType();
        $DoECRequestDetails->PayerID           = $event['Payment.identifier'];
        $DoECRequestDetails->Token             = $event['Order.token'];
        $DoECRequestDetails->PaymentDetails[0] = $paymentDetails;

        $DoECRequest = new DoExpressCheckoutPaymentRequestType();

        $DoECRequest->DoExpressCheckoutPaymentRequestDetails = $DoECRequestDetails;

        $DoECReq = new DoExpressCheckoutPaymentReq();

        $DoECReq->DoExpressCheckoutPaymentRequest = $DoECRequest;

        return $DoECReq;
    }

    /**
     * @param
     * @return Boolean
     */
	private function _isTestMode(): bool
    {
		if ($this->settings['env'] !== "development") {
			return false;
		}
		return true;
	}

    /**
     * @param
     * @return Mode
     */
	private function _getPayPalMode()
    {
		if ($this->_isTestMode()) {
			return 'sandbox';
		}
		return 'live';
	}


    /**
     * @param
     * @return URL
     */
	private function _getPayPalUrl()
    {
		if ($this->_isTestMode()) {
			return 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		}
		return 'https://www.paypal.com/au/cgi-bin/webscr';
	}
}
