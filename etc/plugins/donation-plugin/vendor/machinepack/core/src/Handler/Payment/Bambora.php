<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class Bambora extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['username'])
            || empty($this->_config['password'])
            || empty($this->_config['accountNumber'])
            || empty($this->_config['dtsapiURL'])
            || empty($this->_config['sippapiURL'])
            || !array_key_exists('isProductionMode', $this->_config)
        ) {
            return new Failure(
                'Please add handler settings for Bambora. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                username: "...",
                                password: "...",
                                accountNumber: "...",
                                dtsapiURL: "...",
                                sippapiURL: "..."
                                isProductionMode: "..."
                            }
                        }
                    }
                '
            );
        }

        if (!$event['Order.identifier']) {
            return new Failure('Error ::: No valid order identifier supplier.');
        }

        if (isset($event['Intangible/Event.action']) && $event['Intangible/Event.action'] == 'store_card') {
            return $this->_storeCreditCard($event);
        }

        //depending on one-off or recurring, make payment
        if ($event instanceof Subscription) {
            return $this->_processSubscription($event);
        }

        return $this->_processPayment($event);
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _storeCreditCard($event)
    {
        try {
            $response = $this->_submitCreditCard($event);
            if ($response->ReturnValue->__toString() !== "0") {
                return new Failure(
                    json_encode($response)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'Intangible/Event.ReturnValue' => $response->ReturnValue->_toString(),
                'MoneyTransfer.token' => $response->Token->__toString(),
            ]
        );
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processSubscription($event)
    {
        try {
            $response = $this->_createSubscription($event);
            if ($response->ResponseCode->__toString() !== "0") {
                return new Failure(
                    json_encode($response)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'MoneyTransfer.identifier'                      => $response->ScheduleIdentifier->__toString(),
                'Intangible/APIResponse.APIResponseCode'        => $response->ResponseCode->__toString(),
            ]
        );
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processPayment(Payment $event)
    {
        try {
            $response = $this->_createPayment($event);
            if ($response->ResponseCode->__toString() !== "0") {
                return new Failure(
                    json_encode($response)
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'MoneyTransfer.identifier'                      => $response->Receipt->__toString(),
                'Intangible/APIResponse.APIResponseCode'        => $response->ResponseCode->__toString(),
             ]
        );
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _submitCreditCard($event)
    {
        $amount          = (float) ($event['MonetaryAmount.value']) * 100;
        $xml_post_string = require __DIR__ . '/xml/createCardTokenBambora.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://www.ippayments.com.au/interface/api/sipp/SubmitSinglePayment",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl('POST', $headers, $xml_post_string, true);

        // user $parser to get your data out of XML response and to display it.
        return simplexml_load_string($parser->TokeniseCreditCardResponse->TokeniseCreditCardResult);
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _createPayment($event)
    {
        $amount          = (float) ($event['MonetaryAmount.value']) * 100;
        $xml_post_string = require __DIR__ . '/xml/createPaymentBambora.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://www.ippayments.com.au/interface/api/dts/SubmitSinglePayment",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl('POST', $headers, $xml_post_string);

        // user $parser to get your data out of XML response and to display it.
        return simplexml_load_string($parser->SubmitSinglePaymentResponse->SubmitSinglePaymentResult);
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _createSubscription($event)
    {
        $amount          = (float) ($event['MonetaryAmount.value']) * 100;
        $date            = isset($event['Product/Subscription.date']) && $event['Product/Subscription.date']?
        $event['Product/Subscription.date'] : date('Y-m-d');
        $xml_post_string = require __DIR__ . '/xml/createSubscriptionBambora.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://www.ippayments.com.au/interface/api/dts/SubmitPaymentSchedule",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl('POST', $headers, $xml_post_string);

        // user $parser to get your data out of XML response and to display it.
        return simplexml_load_string($parser->SubmitPaymentScheduleResponse->SubmitPaymentScheduleResult);
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _curl($method, $headers, $data = array(), $is_sipp = false)
    {
        $url = $this->_config['dtsapiURL'];
        if ($is_sipp) {
            $url = $this->_config['sippapiURL'];
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        }

        $response = curl_exec($curl);
        // converting
        $response1 = str_replace("<soap:Body>", "", $response);
        $response2 = str_replace("</soap:Body>", "", $response1);

        // converting to XML
        $response = simplexml_load_string($response2);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $response;
    }
}
