<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;
use SoapClient;
use SoapHeader;

class SalesforceDynamic extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['clientSecret'])
            || empty($this->_config['clientId'])
            || empty($this->_config['username'])
            || empty($this->_config['password'])
            || empty($this->_config['apiUrl'])
        ) {
            return new Failure(
                'Please add handler settings for Saleforce. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                clientSecret:
                                clientId:
                                username:
                                password:
                                apiUrl:
                            }
                        }
                    }
                '
            );
        }

        if (isset($event['query']) && $event['query']) {
            $record = $this->_submitToSalesforceQuery($event);
        } else {
            $record = $this->_submitToSalesforce($event);
        }

        if (is_array($record)) {
            $error_code    = $record[0]->errorCode;
            $error_message = $record[0]->message;

            return new Failure(
                'Error:: Code - ' . $error_code . '. Message - ' . $error_message
            );
        } else {
            if (isset($record->id)) {
                return new Success(
                    [
                        'Intangible/Record.identifier' => $record->id
                    ]
                );
            }

            return new Success(
                [
                    'Intangible/Record' => $record
                ]
            );
        }
    }

    /**
     * Submit the event payload to salesforce
     * @param  Crmsync $event    payment information
     * @return record
     */
    private function _submitToSalesforce(Crmsync $event)
    {
        try {
            if (!empty($event['accessToken'])) {
                $json_token = $event['accessToken'];
            } else {
                $json_token = $this->_generateAccessToken();
            }
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;

            if ($instance_url && $token) {
                $url = '/services/data/v42.0/sobjects/'. $event["sobject"] . '/';

                if (isset($event['id']) && $event['id']) {
                    $url .= $event['id'];
                }

                $full_url = $instance_url . $url;

                $record = $this->_curl($event['method'], $full_url, $token, $event['Intangible/Event.payload']);
                $record = json_decode($record);

                return $record;
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    /**
     * Submit the event payload to salesforce as a query
     * @param  Crmsync $event    payment information
     * @return record
     */
    private function _submitToSalesforceQuery(Crmsync $event)
    {
        try {
            if (!empty($event['accessToken'])) {
                $json_token = $event['accessToken'];
            } else {
                $json_token = $this->_generateAccessToken();
            }
            $token        = $json_token->access_token;
            $instance_url = $json_token->instance_url;

            if ($instance_url && $token) {
                $url = '/services/data/v42.0/query/?q=';

                if (isset($event['query']) && $event['query']) {
                    $url .= $event['query'];
                }

                $full_url = $instance_url . $url;

                $record = $this->_curl($event['method'], $full_url, $token, $event['Intangible/Event.payload']);
                $record = json_decode($record);

                return $record;
            }

            return new Failure(
                'Access token invalid'
            );
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
            //TODO should log stacktrace
        }
    }

    /**
     * Generate an access token for Salesforce
     *
     * @return array $response
     */
    private function _generateAccessToken()
    {
        try {
            $params = array(
                "grant_type"    => "password",
                "client_secret" => $this->_config['clientSecret'],
                "client_id"     => $this->_config['clientId'],
                "username"      => $this->_config['username'],
                "password"      => $this->_config['password']
            );

            $json_response = $this->_curl('POST', '', false, $params);
            return json_decode($json_response);
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    /**
     * Make a curl request
     *
     * @param string $method    GET|POST
     * @param string $url       the url to call
     * @param string $bearer    the bearer token for authentication
     * @param array $data       request payload
     * @return void
     */
    private function _curl($method, $url, $bearer, $data = array())
    {
        try {
            $headers    = array();
            $input_data = '';

            if ($bearer) {
                $headers    = array(
                        "Content-Type: application/json",
                        "Authorization: Bearer " . $bearer
                    );
                $input_data = json_encode($data);
            } else {
                $params = http_build_query($data);
                $url    = $this->_config['apiUrl']."/services/oauth2/token?" . $params;
            }

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PATCH') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            } elseif ($method == 'DELETE') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            } else {
                curl_setopt($curl, CURLOPT_HEADER, 0);
            }

            $result = curl_exec($curl);
            curl_close($curl);

            return $result;
        } catch (Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }
}
