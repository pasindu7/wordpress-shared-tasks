<?php return <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<SecurePayMessage>
    <MerchantInfo>
        <merchantID>{$this->_config['merchantid']}</merchantID>
        <password>{$this->_config['password']}</password>
    </MerchantInfo>
    <RequestType>Payment</RequestType>
    <Payment>
        <TxnList count="1">
            <Txn ID="1">
                <txnType>0</txnType>
                <txnSource>23</txnSource>
                <amount>$amount</amount>
                <recurring>$recurring</recurring>
                <currency>{$event['MonetaryAmount.currency']}</currency>
                <purchaseOrderNo>{$event['Order.identifier']}</purchaseOrderNo>
                <CreditCardInfo>
                    <cardHolderName>{$event['CreditCard/CardDetails.name']}</cardHolderName>
                    <cardNumber>{$event['CreditCard/CardDetails.number']}</cardNumber>
                    <expiryDate>$expiry</expiryDate>
                    <cvv>{$event['CreditCard/CardDetails.cvn']}</cvv>
                </CreditCardInfo>
            </Txn>
        </TxnList>
    </Payment>
</SecurePayMessage>
XML;
