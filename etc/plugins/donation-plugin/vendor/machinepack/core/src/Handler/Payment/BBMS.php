<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class BBMS extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        //validate configuration variables
        if (empty($this->_config['subscription_key'])) {
            return new Failure(
                'Please provide subscription key SKY API integration. Full config should be:
                {
                    config: {
                        env: <someenv>,
                        <someenv>: {
                            subscription_key:
                        }
                    }
                }
            '
            );
        }
        if (!isset($event['action'])) {
            return new Failure('No valid action provided.');
        } else {
            if ($event['action'] == 'get_auth_url') {
                return !isset($event['Intangible/Sky.access_token']) ?
                    $this->_createAuthURL() :
                    new Failure('Creation of auth url prevented, Sky access token already exists.');
            } elseif ($event['action'] == 'get_access_token') {
                return isset($event['Intangible/Sky.code']) ?
                    $this->_getAccessTokenByCode($event) :
                    new Failure('Invalid Sky code, access token can not be generated.');
            } elseif ($event['action'] == 'get_auth_url') {
                return !isset($event['Intangible/Sky.access_token']) ?
                    $this->_createAuthURL() :
                    new Failure('Creation of auth url prevented, Sky access token already exists.');
            } elseif ($event['action'] == 'process_payment') {
                return isset($event['Intangible/Sky.access_token']) ?
                    $this->_processPayment($event) :
                    new Failure('Invalid Sky access token, payment can not be processed.');
            } else {
                return new Failure('Invalid action provided.');
            }
        }
    }

    private function _processPayment($event, $token = false)
    {
        try {
            $auth_key = $event['Intangible/Sky.access_token'];
            if (isset($token['access_token'])) {
                $auth_key = $token['access_token'];
            }

            $payment_config_response = $this->_getPaymentConfig($event, $auth_key);

            $payment_config_id = false;
            if (!($payment_config_response instanceof Failure)) {
                $payment_config_id = $this->_getPaymentConfigId($payment_config_response);
                if (!empty($payment_config_response['access_token'])) {
                    $auth_key = $payment_config_response['access_token'];
                }
                if (!empty($payment_config_response['refresh_token'])) {
                    $refresh_token = $payment_config_response['refresh_token'];
                }
            } else {
                return new Failure($payment_config_response->reason, $payment_config_response->data);
            }

            if ($payment_config_id) {
                // Create Payment Data
                if ($event['Payment.type'] == 'recurring') {
                    $cardToken = $this->_createCardToken($event, $auth_key);
                    if (!$cardToken instanceof Failure) {
                        $data = $this->_createRecurringPaymentData($event, $payment_config_id, $cardToken);
                    } else {
                        return new Failure($cardToken->reason, $cardToken->data);
                    }
                } else {
                    $data = $this->_createOneOffPaymentData($event, $payment_config_id);
                }

                $headers = [
                    "Content-Type: application/json",
                    "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                    "Authorization: Bearer " . $auth_key
                ];

                $method = 'POST';
                $url    = 'https://api.sky.blackbaud.com/payments/v1/transactions';

                $payment_response = json_decode(
                    $this->_skyApiCall(
                        $url,
                        $method,
                        $headers,
                        $data
                    )
                );

                if (!empty($payment_response->id)) {
                    $data = [
                        'Payment.identifier'           => $payment_response->id,
                        'Intangible/Sky.refresh_token' => $refresh_token,
                        'Intangible/Sky.access_token'  => $auth_key,
                    ];

                    if ($event['Payment.type'] == 'recurring') {
                        $data['Payment.config_id']  = $payment_config_id;
                        $data['Payment.card_token'] = $cardToken;
                    }

                    return new Success($data);
                } else {
                    return new Failure($payment_response[0]->message ?? 'Error while create transaction');
                }
            } else {
                return new Failure($payment_config_response->reason . ' ' . $payment_config_response->data);
            }
        } catch (\Exception $ex) {
            $exception_msg = $ex->getMessage();
            $return_array  = ['Errors' => true, 'message' => $exception_msg];
            return json_encode($return_array);
            //TODO should log stacktrace
        }
    }

    private function _createOneOffPaymentData($event, $payment_config_id)
    {
        $data                = $this->_getCommonPaymentData($event);
        $data['credit_card'] = [
            'exp_month' => $event['CreditCard/CardDetails.expiryMonth'],
            'exp_year' => $event['CreditCard/CardDetails.expiryYear'],
            'name' => $event['CreditCard/CardDetails.name'],
            'number' => $event['CreditCard/CardDetails.number'],
        ];
        $data['payment_configuration_id'] = $payment_config_id;
        $data['tokenize']                 = false;
        return (object) $data;
    }

    private function _createRecurringPaymentData($event, $payment_config_id, $card_token)
    {
        $data               = $this->_getCommonPaymentData($event);
        $data['card_token'] = $card_token;
        $data['payment_configuration_id'] = $payment_config_id;
        $data['tokenize']                 = false;
        return (object) $data;
    }

    private function _getCommonPaymentData($event)
    {
        return [
            'amount' => $event['MonetaryAmount.value']*100,
            'billing_contact' => [
                'first_name' => $event['Person.givenName'],
                'last_name' => $event['Person.familyName'],
                'address' => $event['PostalAddress.streetAddress'] ,
                'city' => $event['PostalAddress.addressLocality'],
                'country' => $event['PostalAddress.addressCountry'],
                'post_code' => $event['PostalAddress.addressCountry'],
                'state' => $event['PostalAddress.addressRegion']
            ],
            'phone' => $event['Person.telephone'],
            'email' => $event['Person.email'],
            'csc' => $event['CreditCard/CardDetails.cvn']
        ];
    }

    private function _createCardTokenData($event)
    {
        return (object) [
            'exp_month' => $event['CreditCard/CardDetails.expiryMonth'],
            'exp_year' => $event['CreditCard/CardDetails.expiryYear'],
            'issue_number' => $event['CreditCard/CardDetails.cvn'],
            'name' => $event['CreditCard/CardDetails.name'],
            'number' => $event['CreditCard/CardDetails.number']
        ];
    }

    private function _createCardToken($event, $auth_key)
    {
        $url     = 'https://api.sky.blackbaud.com/payments/v1/cardtokens';
        $method  = 'POST';
        $headers = [
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        ];
        $data    = $this->_createCardTokenData($event);

        $response = json_decode(
            $this->_skyApiCall(
                $url,
                $method,
                $headers,
                $data
            )
        );

        if (isset($response->card_token) && !empty($response->card_token)) {
            return $response->card_token;
        } else {
            return new Failure('Unable to create Card Token', $response->message ?? '');
        }
    }

    private function _getPaymentConfig($event, $auth_key)
    {
        $headers = [
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        ];

        $method = 'GET';
        $url    = 'https://api.sky.blackbaud.com/payments/v1/paymentconfigurations';

        $response                = $this->_skyApiCall($url, $method, $headers, array(), true);
        $payment_config_response = json_decode($response, true);

        if (isset($payment_config_response['value']) && ! empty($payment_config_response['value'])) {
            // has require key index
            return $payment_config_response;
        }

        if (isset($payment_config_response['statusCode']) && $payment_config_response['statusCode'] == 401) {
            $refresh_token = $this->_getRefreshToken($event);

            if ($refresh_token instanceof Failure) {
                return $refresh_token;
            }
            $access_token = $refresh_token['access_token'];

            $payment_config_response = $this->_getPaymentConfig($event, $access_token);
            if ($payment_config_response instanceof Failure) {
                return $payment_config_response;
            }
            $payment_config_response['refresh_token'] = $refresh_token['refresh_token'];
            $payment_config_response['access_token']  = $refresh_token['access_token'];

            return $payment_config_response;
        } else {
            return new Failure('Unable to get Payment Configuration ID');
        }
    }
    private function _getPaymentConfigId($payment_config_response)
    {
        $payment_config_id = '';
        if (isset($payment_config_response['value']) && is_array($payment_config_response['value'])) {
            $get_payment_config = $payment_config_response['value'];

            if ($get_payment_config) {
                foreach ($get_payment_config as $payment_config) {
                    if ($payment_config['name'] == $this->_config['payment_config']) {
                        $payment_config_id = $payment_config['id'];
                        break;
                    }
                }
            }
        }
        return $payment_config_id;
    }

    private function _createAuthURL()
    {
        $auth_data = [
            'client_id' => $this->_config['client_id'],
            'response_type' => 'code',
            'redirect_uri' => $this->_config['redirect_uri']
        ];
        $auth_uri  = 'https://oauth2.sky.blackbaud.com/authorization?' . http_build_query($auth_data);

        return new Success(
            [
                'status' => 'URL Creation',
                'url'    => $auth_uri
            ]
        );
    }

    private function _getAccessTokenByCode($event)
    {
        $body = [
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->_config['redirect_uri'],
            'code' => $event['Intangible/Sky.code']
        ];

        $token = $this->_getToken($body);
        if (!($token instanceof Failure)) {
            return new Success(
                [
                    $token
                ]
            );
        }
        return $token;
    }

    private function _getRefreshToken($event)
    {
        $refresh_token = $event['Intangible/Sky.refresh_token'];
        $body          = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            'preserve_refresh_token' => true
        ];

        return $this->_getToken($body);
    }

    private function _getToken($body = array())
    {
        $headers = [
            'Content-type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . base64_encode($this->_config['client_id'] . ':' . $this->_config['client_secret'])
        ];

        $url = "https://oauth2.sky.blackbaud.com/token";

        $response = $this->_skyApiCall($url, 'POST', $headers, $body, true);
        $token    = json_decode($response, true);

        if (isset($token['error']) && $token['error']) {
            return new Failure(
                json_encode($token['error'])
            );
        }

        return $token;
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _skyApiCall($url, $method, $headers, $data, $auth = false)
    {
        $params = array();

        if ($auth) {
            $params = http_build_query($data);
        } else {
            $params = json_encode($data);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        } elseif ($method == "PATCH") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }

        $response = curl_exec($curl);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $response;
    }
}
