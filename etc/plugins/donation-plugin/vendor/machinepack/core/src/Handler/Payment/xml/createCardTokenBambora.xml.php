<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <TokeniseCreditCard xmlns="http://www.ippayments.com.au/interface/api/sipp">
      <tokeniseCreditCardXML>
            <![CDATA[
            <TokeniseCreditCard>
                <CustomerStorageNumber>{$event['Person.identifier']}</CustomerStorageNumber>  
                <CardNumber>{$event['CreditCard/CardDetails.number']}</CardNumber> 
                <ExpM>{$event['CreditCard/CardDetails.expiryMonth']}</ExpM>
                <ExpY>{$event['CreditCard/CardDetails.expiryYear']}</ExpY> 
                <TokeniseAlgorithmID>2</TokeniseAlgorithmID>
                <UserName>{$this->_config['username']}</UserName> 
                <Password>{$this->_config['password']}</Password>
            </TokeniseCreditCard>
            ]]>
        </tokeniseCreditCardXML>
    </TokeniseCreditCard>
  </soap12:Body>
</soap12:Envelope>
XML;
