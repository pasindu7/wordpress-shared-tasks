<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;
use SoapClient;
use SoapHeader;

class Eway extends Handler
{
    const EMAIL_FIELD = 'Person.email';

    const API_URL_RECURRING         = 'https://www.eway.com.au/gateway/rebill/manageRebill.asmx?WSDL';
    const API_URL_RECURRING_SANDBOX = 'https://www.eway.com.au/gateway/rebill/test/manageRebill_test.asmx?WSDL';

    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['apiKey'])
            || empty($this->_config['apiPassword'])
            || !array_key_exists('isProductionMode', $this->_config)
        ) {
            return new Failure(
                'Please add handler settings for eWay. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                apiKey: "...",
                                apiPassword: "...",
                                recurringUrl: "...",
                                recurringKey: "...",
                                recurringPassword: "...",
                                isProductionMode: "..."
                            }
                        }
                    }
                '
            );
        }

        //depending on one-off or recurring, make payment
        if ($event instanceof Subscription) {
            if (isset($event['action'])) {
                if ($event['action'] === 'customer_token') {
                    return $this->_createCustomerToken($event);
                } elseif ($event['action'] === 'update_customer_token') {
                    return $this->_updateCustomerToken($event);
                } elseif ($event['action'] === 'customer_token_charge') {
                    return $this->_chargeCustomerToken($event);
                }
            }

            return $this->_processSubscription($event);
        }

        return $this->_processPayment($event);
    }

    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processSubscription($event)
    {
        try {
            $client = new SOAPClient($this->_getRecurringApiUrl(), []);

            $headerOpts = [
                'eWAYCustomerID'=> $this->_config['recurringCustomerId'],
                'Username'=> $this->_config['recurringUsername'],
                'Password'=> $this->_config['recurringPassword']
            ];

            $header = new SOAPHeader('http://www.eway.com.au/gateway/rebill/manageRebill', 'eWAYHeader', $headerOpts);

            $customerId = $this->_createRebillCustomer($event, $client, $header);

            $response = $this->_createRebillEvent($event, $customerId, $client, $header);

            if ($response->CreateRebillEventResult->Result == 'Success') {
                return new Success(
                    [
                        'MoneyTransfer.identifier'=>$response->CreateRebillEventResult->RebillID,
                        'Person.identifier'=>$response->CreateRebillEventResult->RebillCustomerID
                    ]
                );
            } else {
                return new Failure(
                    'Could not create Eway Rebill Event'
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processPayment(Payment $event)
    {
        try {
            $client = \Eway\Rapid::createClient(
                $this->_config['apiKey'],
                $this->_config['apiPassword'],
                $this->_getMode()
            );

            $paymentRequest = $this->_createCaptureRequest($event);
            $response       = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $paymentRequest);

            if (empty($response->TransactionStatus)) {
                //if I dont get a status assume it went through correctly
                //TODO check that this is correct behaviour

                $errorArray    = $this->_handleCaptureFailure($response, $event);
                $errorMessages = implode(',', $errorArray['errors']);
                return new Failure(
                    $errorMessages
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'MoneyTransfer.identifier'=>$response->TransactionID
            ]
        );
    }

    /**
     * Create a customer token for recurring payments
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _createCustomerToken(\MachinePack\Core\Event\Event $event)
    {
        try {
            $client = \Eway\Rapid::createClient(
                $this->_config['apiKey'],
                $this->_config['apiPassword'],
                $this->_getMode()
            );

            $customer = $this->_createTokenCaptureRequest($event);
            $response = $client->createCustomer(\Eway\Rapid\Enum\ApiMethod::DIRECT, $customer);

            if (empty($response->Customer->TokenCustomerID)) {
                $errorArray    = $this->_handleCaptureFailure($response, $event);
                $errorMessages = implode(',', $errorArray['errors']);
                return new Failure(
                    $errorMessages
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'TokenCustomerID' => $response->Customer->TokenCustomerID
            ]
        );
    }

    /**
     * Update customer token details
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _updateCustomerToken(\MachinePack\Core\Event\Event $event)
    {
        try {
            $client = \Eway\Rapid::createClient(
                $this->_config['apiKey'],
                $this->_config['apiPassword'],
                $this->_getMode()
            );

            $customer = $this->_updateTokenCaptureRequest($event);
            $response = $client->updateCustomer(\Eway\Rapid\Enum\ApiMethod::DIRECT, $customer);

            if (empty($response->Customer->TokenCustomerID)) {
                $errorArray    = $this->_handleCaptureFailure($response, $event);
                $errorMessages = implode(',', $errorArray['errors']);
                return new Failure(
                    $errorMessages
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'TokenCustomerID' => $response->Customer->TokenCustomerID
            ]
        );
    }

    /**
     * Charge a customer token for all payments
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _chargeCustomerToken(\MachinePack\Core\Event\Event $event)
    {
        try {
            $client = \Eway\Rapid::createClient(
                $this->_config['apiKey'],
                $this->_config['apiPassword'],
                $this->_getMode()
            );

            $transaction = $this->_createChargeTokenCaptureRequest($event);
            $response    = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);

            if (empty($response->TransactionStatus)) {
                //if I dont get a status assume it went through correctly
                //TODO check that this is correct behaviour

                $errorArray    = $this->_handleCaptureFailure($response, $event);
                $errorMessages = implode(',', $errorArray['errors']);
                return new Failure(
                    $errorMessages
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success(
            [
                'MoneyTransfer.identifier'=>$response->TransactionID
            ]
        );
    }

    /**
     * Create a capture request to send to Eway
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _createCaptureRequest(\MachinePack\Core\Event\Event $event) : array
    {
        $txn = [
            'Customer' => [
                'Reference' => md5($event['Order.identifier']),
                'Title' => 'Mr',
                'FirstName' => $event['Person.givenName'],
                'LastName' => $event['Person.familyName'],
                'Phone' => $event['Person.telephone'],
                'Email' => $event[Eway::EMAIL_FIELD],
                'Street1' => $event['PostalAddress.streetAddress'],
                'City' => $event['PostalAddress.addressLocality'],
                'State' => $event['PostalAddress.addressRegion'],
                'PostalCode' => $event['PostalAddress.postalCode'],
                'Country' => $event['PostalAddress.addressCountry'],
                'CardDetails' => [
                    'Name' => $event['CreditCard/CardDetails.name'],
                    'Number' => $event['CreditCard/CardDetails.number'],
                    'ExpiryMonth' => $event['CreditCard/CardDetails.expiryMonth'],
                    'ExpiryYear' =>$event['CreditCard/CardDetails.expiryYear'],
                    'CVN' => $event['CreditCard/CardDetails.cvn'],
                ]
            ],
            'Payment' => [
                'TotalAmount' => round($event['MonetaryAmount.value'] * 100.0),
                    //no decimal points, must be to nearest cent e.g. $10.00 -> 1000. Assume AUD.
                'InvoiceReference' => $event['Order.description']
            ],
            'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
        ];


        return $txn;
    }


    /**
     * Create a token capture request to send to Eway
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _createTokenCaptureRequest(\MachinePack\Core\Event\Event $event) : array
    {
        $txn = [
            'Title' => 'Mr',
            'FirstName' => $event['Person.givenName'],
            'LastName' => $event['Person.familyName'],
            'Country' => $event['PostalAddress.addressCountry'],
            'CardDetails' => [
                'Name' => $event['CreditCard/CardDetails.name'],
                'Number' => $event['CreditCard/CardDetails.number'],
                'ExpiryMonth' => $event['CreditCard/CardDetails.expiryMonth'],
                'ExpiryYear' =>$event['CreditCard/CardDetails.expiryYear'],
                'CVN' => $event['CreditCard/CardDetails.cvn'],
            ]
        ];

        return $txn;
    }

    /**
     * Create a token capture request to send to Eway for updating token details
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _updateTokenCaptureRequest(\MachinePack\Core\Event\Event $event) : array
    {
        $txn = [
            'Title' => 'Mr',
            'FirstName' => $event['Person.givenName'],
            'LastName' => $event['Person.familyName'],
            'Country' => $event['PostalAddress.addressCountry'],
            'TokenCustomerID' => $event['TokenCustomerID'],
            'CardDetails' => [
                'Name' => $event['CreditCard/CardDetails.name'],
                'Number' => $event['CreditCard/CardDetails.number'],
                'ExpiryMonth' => $event['CreditCard/CardDetails.expiryMonth'],
                'ExpiryYear' =>$event['CreditCard/CardDetails.expiryYear'],
                'CVN' => $event['CreditCard/CardDetails.cvn'],
            ]
        ];

        return $txn;
    }


    /**
     * Create a token capture request to send to Eway
     * @param  Payment $event    subscription information
     * @return array
     */
    private function _createChargeTokenCaptureRequest(\MachinePack\Core\Event\Event $event) : array
    {
        $txn = [
            'Customer' => [
                'TokenCustomerID'  => $event['Intangible/eWAY.CustomerTokenID'],
            ],
            'Payment' => [
                'TotalAmount' => round($event['MonetaryAmount.value'] * 100.0),
            ],
            'TransactionType' => \Eway\Rapid\Enum\TransactionType::RECURRING,
        ];

        return $txn;
    }


    /**
     * Create a rebilling customer in eway
     * @param  Payment $event    payment information
     * @param  $client    eway client
     * @param  $header    soap headers
     * @return int $customerId
     */
    private function _createRebillCustomer($event, $client, $header):int
    {
        $customer = array(
            "CreateRebillCustomer" => array(
                'customerTitle' => '',
                'customerFirstName' => $event['Person.givenName'],
                'customerLastName' => $event['Person.familyName'],
                'customerAddress' => $event['PostalAddress.streetAddress'],
                'customerSuburb' => $event['PostalAddress.addressLocality'],
                'customerState' => $event['PostalAddress.addressRegion'],
                'customerCompany' => '',
                'customerPostCode' => $event['PostalAddress.postalCode'],
                'customerCountry' => $event['PostalAddress.addressCountry'],
                'customerEmail' => $event['Person.email'],
                'customerFax' => '',
                'customerPhone1' => $event['Person.telephone'],
                'customerPhone2' => '',
                'customerRef' => 'DON_' . date('Y') . '_MONTHLY',
                'customerJobDesc' => '',
                'customerComments' => '',
                'customerURL' => ''
            )
        );

        $response = $client->__soapCall("CreateRebillCustomer", $customer, null, $header);

        $result     = $response->CreateRebillCustomerResult->Result;
        $customerId = $response->CreateRebillCustomerResult->RebillCustomerID;

        if ($result=='Success' && $customerId) {
            if (!empty($response->CreateRebillCustomerResult->RebillCustomerID)) {
                return $response->CreateRebillCustomerResult->RebillCustomerID;
            }
        } else {
            $ErrorSeverity = $response->CreateRebillCustomerResult->ErrorSeverity;
            $ErrorDetails  = $response->CreateRebillCustomerResult->ErrorDetails;
        }

        throw new \Exception('Unable to create recurring profile for customer: ' . $ErrorDetails);
    }

    /**
     * Create a rebilling event (i.e. recurring billing schedule) in eway
     * @param  Payment $event    payment information
     * @param  $customerId    rebilling customer id
     * @param  $client    eway client
     * @param  $header    soap headers
     * @return int $customerId
     */
    private function _createRebillEvent($event, $customerId, $client, $header)
    {
        $event = array(
            "CreateRebillEvent " => array(
                "RebillCustomerID" => $customerId,
                "RebillInvRef" => "DONONLINE" . date('Y') . "_MONTHLY_PAYMENT_EVENT",
                "RebillInvDes" => $event['Order.description'],
                'RebillCCName' => $event['CreditCard/CardDetails.name'],
                'RebillCCNumber' => $event['CreditCard/CardDetails.number'],
                'RebillCCExpMonth' => $event['CreditCard/CardDetails.expiryMonth'],
                'RebillCCExpYear' => $event['CreditCard/CardDetails.expiryYear'],
                'RebillInitAmt' => (int) ($event['MonetaryAmount.value'] * 100),
                'RebillInitDate' => date('d/m/Y', strtotime('+1 day')),
                'RebillRecurAmt' => (int) ($event['MonetaryAmount.value'] * 100),
                'RebillStartDate' => date('d/m/Y', strtotime('+1 month')),
                'RebillInterval' => 1,
                'RebillIntervalType' => 3,
                'RebillEndDate' => '31/12/2050' //TODO this should probably be adjustable
            )
        );

        $response = $client->__soapCall("CreateRebillEvent", $event, null, $header);

        return $response;
    }

    /**
     * Get the Eway mode i.e. sandbox or production based on config variable
     * @return \Eway\Rapid\Client::MODE_PRODUCTION|\Eway\Rapid\Client::MODE_SANDBOX
     */
    private function _getMode()
    {
        if ($this->_config['isProductionMode']) {
            return \Eway\Rapid\Client::MODE_PRODUCTION;
        }

        return \Eway\Rapid\Client::MODE_SANDBOX;
    }

    /**
     * Strip and return error messages from eway response
     * @return array $error_messages
     */
    private function _handleCaptureFailure($response, $transaction)
    {
        $error_messages = [];

        if (! empty($response->Errors) || $response->TransactionStatus == false) {
            $errors = explode(',', $response->Errors);

            if ($response->TransactionStatus == false) {
                $errors[] = $response->ResponseMessage;
            }

            foreach ($errors as $error) {
                if (empty($error)) {
                    continue;
                }
                array_push($error_messages, $this->_messageByCode($error));
            }
        }

        $response = [
           'success' => false,
           'message' => 'Sorry, your donation was not processed',
           'errors' => $error_messages
        ];

        return $response;
    }

    /**
     * Map eway response code to message
     * @return string $errorMessage
     */
    private function _messageByCode($code)
    {
        $messages = [
            'A2000' => 'Transaction Approved',
            'A2008' => 'Honour With Identification',
            'A2010' => 'Approved For Partial Amount',
            'A2011' => 'Approved, VIP',
            'A2016' => 'Approved, Update Track 3',
            'D4401' => 'Refer to Issuer',
            'D4402' => 'Refer to Issuer, special',
            'D4403' => 'No Merchant',
            'D4404' => 'Pick Up Card',
            'D4405' => 'Do Not Honour',
            'D4406' => 'Error',
            'D4407' => 'Pick Up Card, Special',
            'D4409' => 'Request In Progress',
            'D4412' => 'Invalid Transaction',
            'D4413' => 'Invalid Amount',
            'D4414' => 'Invalid Card Number',
            'D4415' => 'No Issuer',
            'D4419' => 'Re-enter Last Transaction',
            'D4421' => 'No Action Taken',
            'D4422' => 'Suspected Malfunction',
            'D4423' => 'Unacceptable Transaction Fee',
            'D4425' => 'Unable to Locate Record On File',
            'D4430' => 'Format Error',
            'D4431' => 'Bank Not Supported By Switch',
            'D4433' => 'Expired Card, Capture',
            'D4434' => 'Suspected Fraud, Retain Card',
            'D4435' => 'Card Acceptor, Contact Acquirer, Retain Card',
            'D4436' => 'Restricted Card, Retain Card',
            'D4437' => 'Contact Acquirer Security Department, Retain Card',
            'D4438' => 'PIN Tries Exceeded, Capture',
            'D4439' => 'No Credit Account',
            'D4440' => 'Function Not Supported',
            'D4441' => 'Lost Card',
            'D4442' => 'No Universal Account',
            'D4443' => 'Stolen Card',
            'D4444' => 'No Investment Account',
            'D4451' => 'Insufficient Funds',
            'D4452' => 'No Cheque Account',
            'D4453' => 'No Savings Account',
            'D4454' => 'Expired Card',
            'D4455' => 'Incorrect PIN',
            'D4456' => 'No Card Record',
            'D4457' => 'Function Not Permitted to Cardholder',
            'D4458' => 'Function Not Permitted to Terminal',
            'D4459' => 'Suspected Fraud',
            'D4460' => 'Acceptor Contact Acquirer',
            'D4461' => 'Exceeds Withdrawal Limit',
            'D4462' => 'Restricted Card',
            'D4463' => 'Security Violation',
            'D4464' => 'Original Amount Incorrect',
            'D4466' => 'Acceptor Contact Acquirer, Security',
            'D4467' => 'Capture Card',
            'D4475' => 'PIN Tries Exceeded',
            'D4482' => 'CVV Validation Error',
            'D4490' => 'Cut off In Progress',
            'D4491' => 'Card Issuer Unavailable',
            'D4492' => 'Unable To Route Transaction',
            'D4493' => 'Cannot Complete, Violation Of The Law',
            'D4494' => 'Duplicate Transaction',
            'D4496' => 'System Error',
            'D4497' => 'MasterPass Error',
            'D4498' => 'PayPal Create Transaction Error',
            'D4499' => 'Invalid Transaction for Auth/Void',
            'F7000' => 'Undefined Fraud Error',
            'F7001' => 'Challenged Fraud',
            'F7002' => 'Country Match Fraud',
            'F7003' => 'High Risk Country Fraud',
            'F7004' => 'Anonymous Proxy Fraud',
            'F7005' => 'Transparent Proxy Fraud',
            'F7006' => 'Free Email Fraud',
            'F7007' => 'International Transaction Fraud',
            'F7008' => 'Risk Score Fraud',
            'F7009' => 'Denied Fraud',
            'F7010' => 'Denied by PayPal Fraud Rules',
            'F9010' => 'High Risk Billing Country',
            'F9011' => 'High Risk Credit Card Country',
            'F9012' => 'High Risk Customer IP Address',
            'F9013' => 'High Risk Email Address',
            'F9014' => 'High Risk Shipping Country',
            'F9015' => 'Multiple card numbers for single email address',
            'F9016' => 'Multiple card numbers for single location',
            'F9017' => 'Multiple email addresses for single card number',
            'F9018' => 'Multiple email addresses for single location',
            'F9019' => 'Multiple locations for single card number',
            'F9020' => 'Multiple locations for single email address',
            'F9021' => 'Suspicious Customer First Name',
            'F9022' => 'Suspicious Customer Last Name',
            'F9023' => 'Transaction Declined',
            'F9024' => 'Multiple transactions for same address with known credit card ',
            'F9025' => 'Multiple transactions for same address with new credit card',
            'F9026' => 'Multiple transactions for same email with new credit card',
            'F9027' => 'Multiple transactions for same email with known credit card',
            'F9028' => 'Multiple transactions for new credit card',
            'F9029' => 'Multiple transactions for known credit card',
            'F9030' => 'Multiple transactions for same email address',
            'F9031' => 'Multiple transactions for same credit card',
            'F9032' => 'Invalid Customer Last Name',
            'F9033' => 'Invalid Billing Street',
            'F9034' => 'Invalid Shipping Street',
            'F9037' => 'Suspicious Customer Email Address',
            'S5000' => 'System Error',
            'S5011' => 'PayPal Connection Error',
            'S5012' => 'PayPal Settings Error',
            'S5085' => 'Started 3dSecure',
            'S5086' => 'Routed 3dSecure',
            'S5087' => 'Completed 3dSecure',
            'S5088' => 'PayPal Transaction Created',
            'S5099' => 'Incomplete (Access Code in progress/incomplete)',
            'S5010' => 'Unknown error returned by gateway',
            'V6000' => 'Validation error',
            'V6001' => 'Invalid CustomerIP',
            'V6002' => 'Invalid DeviceID',
            'V6003' => 'Invalid Request PartnerID',
            'V6004' => 'Invalid Request Method',
            'V6010' => 'Invalid TransactionType, account not certified for eCome only MOTO or Recurring available',
            'V6011' => 'Invalid Payment TotalAmount',
            'V6012' => 'Invalid Payment InvoiceDescription',
            'V6013' => 'Invalid Payment InvoiceNumber',
            'V6014' => 'Invalid Payment InvoiceReference',
            'V6015' => 'Invalid Payment CurrencyCode',
            'V6016' => 'Payment Required',
            'V6017' => 'Payment CurrencyCode Required',
            'V6018' => 'Unknown Payment CurrencyCode',
            'V6021' => 'EWAY_CARDHOLDERNAME Required',
            'V6022' => 'EWAY_CARDNUMBER Required',
            'V6023' => 'EWAY_CARDCVN Required',
            'V6033' => 'Invalid Expiry Date',
            'V6034' => 'Invalid Issue Number',
            'V6035' => 'Invalid Valid From Date',
            'V6040' => 'Invalid TokenCustomerID',
            'V6041' => 'Customer Required',
            'V6042' => 'Customer FirstName Required',
            'V6043' => 'Customer LastName Required',
            'V6044' => 'Customer CountryCode Required',
            'V6045' => 'Customer Title Required',
            'V6046' => 'TokenCustomerID Required',
            'V6047' => 'RedirectURL Required',
            'V6048' => 'CheckoutURL Required when CheckoutPayment specified',
            '​V6049' => 'Invalid Checkout URL',
            'V6051' => 'Invalid Customer FirstName',
            'V6052' => 'Invalid Customer LastName',
            'V6053' => 'Invalid Customer CountryCode',
            'V6058' => 'Invalid Customer Title',
            'V6059' => 'Invalid RedirectURL',
            'V6060' => 'Invalid TokenCustomerID',
            'V6061' => 'Invalid Customer Reference',
            'V6062' => 'Invalid Customer CompanyName',
            'V6063' => 'Invalid Customer JobDescription',
            'V6064' => 'Invalid Customer Street1',
            'V6065' => 'Invalid Customer Street2',
            'V6066' => 'Invalid Customer City',
            'V6067' => 'Invalid Customer State',
            'V6068' => 'Invalid Customer PostalCode',
            'V6069' => 'Invalid Customer Email',
            'V6070' => 'Invalid Customer Phone',
            'V6071' => 'Invalid Customer Mobile',
            'V6072' => 'Invalid Customer Comments',
            'V6073' => 'Invalid Customer Fax',
            'V6074' => 'Invalid Customer URL',
            'V6075' => 'Invalid ShippingAddress FirstName',
            'V6076' => 'Invalid ShippingAddress LastName',
            'V6077' => 'Invalid ShippingAddress Street1',
            'V6078' => 'Invalid ShippingAddress Street2',
            'V6079' => 'Invalid ShippingAddress City',
            'V6080' => 'Invalid ShippingAddress State',
            'V6081' => 'Invalid ShippingAddress PostalCode',
            'V6082' => 'Invalid ShippingAddress Email',
            'V6083' => 'Invalid ShippingAddress Phone',
            'V6084' => 'Invalid ShippingAddress Country',
            'V6085' => 'Invalid ShippingAddress ShippingMethod',
            'V6086' => 'Invalid ShippingAddress Fax',
            'V6091' => 'Unknown Customer CountryCode',
            'V6092' => 'Unknown ShippingAddress CountryCode',
            'V6100' => 'Invalid EWAY_CARDNAME',
            'V6101' => 'Invalid EWAY_CARDEXPIRYMONTH',
            'V6102' => 'Invalid EWAY_CARDEXPIRYYEAR',
            'V6103' => 'Invalid EWAY_CARDSTARTMONTH',
            'V6104' => 'Invalid EWAY_CARDSTARTYEAR',
            'V6105' => 'Invalid EWAY_CARDISSUENUMBER',
            'V6106' => 'Invalid EWAY_CARDCVN',
            'V6107' => 'Invalid EWAY_ACCESSCODE',
            'V6108' => 'Invalid CustomerHostAddress',
            'V6109' => 'Invalid UserAgent',
            'V6110' => 'Invalid EWAY_CARDNUMBER',
            'V6111' => 'Unauthorised API Access, Account Not PCI Certified',
            'V6112' => 'Redundant card details other than expiry year and month',
            'V6113' => 'Invalid transaction for refund',
            'V6114' => 'Gateway validation error',
            'V6115' => 'Invalid DirectRefundRequest, Transaction ID',
            'V6116' => 'Invalid card data on original TransactionID',
            'V6117' => 'Invalid CreateAccessCodeSharedRequest, FooterText',
            'V6118' => 'Invalid CreateAccessCodeSharedRequest, HeaderText',
            'V6119' => 'Invalid CreateAccessCodeSharedRequest, Language',
            'V6120' => 'Invalid CreateAccessCodeSharedRequest, LogoUrl',
            'V6121' => 'Invalid TransactionSearch, Filter Match Type',
            'V6122' => 'Invalid TransactionSearch, Non numeric Transaction ID',
            'V6123' => 'Invalid TransactionSearch,no TransactionID or AccessCode specified',
            'V6124' => 'Invalid Line Items. The line items have been provided however the totals do not match the TotalAmount field', //phpcs:ignore
            'V6125' => 'Selected Payment Type not enabled',
            'V6126' => 'Invalid encrypted card number, decryption failed',
            'V6127' => 'Invalid encrypted cvn, decryption failed',
            'V6128' => 'Invalid Method for Payment Type',
            'V6129' => 'Transaction has not been authorised for Capture/Cancellation',
            'V6130' => 'Generic customer information error',
            'V6131' => 'Generic shipping information error',
            'V6132' => 'Transaction has already been completed or voided, operation not permitted',
            'V6133' => 'Checkout not available for Payment Type',
            'V6134' => 'Invalid Auth Transaction ID for Capture/Void',
            'V6135' => 'PayPal Error Processing Refund',
            'V6140' => 'Merchant account is suspended',
            'V6141' => 'Invalid PayPal account details or API signature',
            'V6142' => 'Authorise not available for Bank/Branch',
            'V6150' => 'Invalid Refund Amount',
            'V6151' => 'Refund amount greater than original transaction',
        ];

        return isset($messages[$code]) ? $messages[$code] : "Failed processing the transaction. Please try again.";
    }

    /**
     * Get recurring API URL based on isProductionMode
     * @return string self::API_URL_RECURRING|self::API_URL_RECURRING_SANDBOX
     */
    private function _getRecurringApiUrl():string
    {
        if ($this->_config['isProductionMode']) {
            return self::API_URL_RECURRING;
        } else {
            return self::API_URL_RECURRING_SANDBOX;
        }
    }

    /**
     * Get payment settings
     * @return array $settings
     */
    public function getPaymentSettings()
    {
        return [
            'eway' => $this->settings[$this->settings['env']]
        ];
    }
}
