<?php return <<<XML
<?xml version ="1.0" encoding="utf-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
xmlns:dts="http://www.ippayments.com.au/interface/api/dts">
<soapenv:Header/>
<soapenv:Body>
<dts:SubmitPaymentSchedule>
<dts:scheduleXML>
<![CDATA[
	 <Schedule>
      		<CustNumber>{$event['Order.identifier']}</CustNumber>
      		<Amount>$amount</Amount>
     		<TrnType>1</TrnType>
      		<AccountNumber>{$this->_config['accountNumber']}</AccountNumber>
      		<CreditCard Registered="False">
				<CardNumber>{$event['CreditCard/CardDetails.number']}</CardNumber>
				<ExpM>{$event['CreditCard/CardDetails.expiryMonth']}</ExpM>
				<ExpY>{$event['CreditCard/CardDetails.expiryYear']}</ExpY>
				<CVN>{$event['CreditCard/CardDetails.cvn']}</CVN>
				<CardHolderName>{$event['CreditCard/CardDetails.name']}</CardHolderName>
       		</CreditCard>
			<Schedule>
				<Frequency>M</Frequency>
				<StartDate>{$date}</StartDate>
			</Schedule>
			<Security>
				<UserName>{$this->_config['username']}</UserName>
				<Password>{$this->_config['password']}</Password>
       		</Security>
	</Schedule>
]]>
</dts:scheduleXML>
</dts:SubmitPaymentSchedule>
</soapenv:Body>
</soapenv:Envelope>
XML;
