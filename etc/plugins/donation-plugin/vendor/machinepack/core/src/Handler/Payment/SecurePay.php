<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class SecurePay extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (!$event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['apiBaseUrl'])
            || empty($this->_config['merchantid'])
            || empty($this->_config['password'])
            || !array_key_exists('isProductionMode', $this->_config)
        ) {
            MachinePack::log('Please add handler settings for SecurePay.', 'debug');
            return new Failure(
                'Please add handler settings for SecurePay. Full config should be:
                    {
                        config: {
                            env: <someenv>
                            <someenv>: {
                                apiBaseUrl: "..."
                                merchantid: "..."
                                password: "..."
                                isProductionMode: "..."
                            }
                        }
                    }
                '
            );
        }

        if ($event instanceof Subscription) {
            return $this->_processPayment($event, true);
        }

        return $this->_processPayment($event);
    }

    /**
     * Handle a payment request (recurring or one off)
     * @param  Payment $event       payment information
     * @param  boolean $recurring   recurring payment flag
     * @return Success|Failure
     */
    private function _processPayment(Payment $event, $recurreing = false)
    {
        try {
            $response     = $this->_createPayment($event, $recurreing);
            $tokenRespose = $this->_createPaymentToken($event);

            $formattedResponse = $this->_formatResponse($response, $tokenRespose);

            if ($formattedResponse['approved'] === 'Yes') {
                return new Success($formattedResponse);
            } else {
                return new Failure(json_encode($formattedResponse));
            }
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
        }
    }

    private function _createPayment($event, $recurring)
    {
        $amount          = (float) ($event['MonetaryAmount.value']) * 100;
        $expiry          = $event['CreditCard/CardDetails.expiryMonth']. '/'.
                            $event['CreditCard/CardDetails.expiryYear'];
        $xml_post_string = require __DIR__ . '/xml/createPayment.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl($headers, $xml_post_string);

        // user $parser to get your data out of XML response and to display it.
        return $parser;
    }

    private function _createPaymentToken($event)
    {
        $amount          = (float) ($event['MonetaryAmount.value']) * 100;
        $expiry          = $event['CreditCard/CardDetails.expiryMonth']. '/'.
                            $event['CreditCard/CardDetails.expiryYear'];
        $xml_post_string = require __DIR__ . '/xml/createCardToken.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl($headers, $xml_post_string, true);

        // user $parser to get your data out of XML response and to display it.
        return $parser;
    }

    private function _formatResponse($response, $tokenRespose)
    {
        $formattedResponse = array(
            'statusCode'        => isset($response->Status->statusCode)?
            $response->Status->statusCode->__toString():'',
            'statusDescription' => isset($response->Status->statusDescription)?
            $response->Status->statusDescription->__toString():'',
            'responseCode'      => isset($response->Payment->TxnList->Txn->responseCode)?
            $response->Payment->TxnList->Txn->responseCode->__toString():'',
            'responseText'      => isset($response->Payment->TxnList->Txn->responseText)?
            $response->Payment->TxnList->Txn->responseText->__toString():'',
            'settlementDate'    => isset($response->Payment->TxnList->Txn->settlementDate)?
            $response->Payment->TxnList->Txn->settlementDate->__toString():'',
            'txnID'             => isset($response->Payment->TxnList->Txn->txnID)?
            $response->Payment->TxnList->Txn->txnID->__toString():'',
            'purchaseOrderNo'   => isset($response->Payment->TxnList->Txn->purchaseOrderNo)?
            $response->Payment->TxnList->Txn->purchaseOrderNo->__toString():'',
            'approved'          => isset($response->Payment->TxnList->Txn->approved)?
            $response->Payment->TxnList->Txn->approved->__toString():'',
        );
        if (isset($tokenRespose->Token->TokenList->TokenItem->successful)
            && $tokenRespose->Token->TokenList->TokenItem->successful->__toString() === 'yes'
        ) {
            $formattedResponse['token'] = $tokenRespose->Token->TokenList->TokenItem->tokenValue->__toString();
        }

        return $formattedResponse;
    }

    private function _curl($headers, $xml_post_string, $isToken = false)
    {
        $url = $this->_config['apiBaseUrl'] . 'payment';
        if ($isToken) {
            $url = $this->_config['apiBaseUrl'] . 'token';
        }

        $ch = curl_init($url);
        if (false === $ch) {
            throw new \Exception('Unable to initialise curl.');
        }

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);

        // converting
        $response = curl_exec($ch);

        if (false === $response) {
            throw new \Exception(
                sprintf(
                    'Payment system appears down. Errno %s (%s).',
                    curl_errno($ch),
                    curl_error($ch)
                )
            );
        }

        curl_close($ch);

        // converting to XML
        $parser = simplexml_load_string($response);

        return $parser;
    }
}
