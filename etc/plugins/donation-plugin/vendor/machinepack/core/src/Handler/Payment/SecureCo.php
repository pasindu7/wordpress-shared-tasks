<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class SecureCo extends Handler
{

    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (!$event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['apiBaseUrl'])
            || empty($this->_config['bearer'])
            || empty($this->_config['merchantAccountId'])
            || !array_key_exists('isProductionMode', $this->_config)
        ) {
            MachinePack::log('Please add handler settings for SecureCo.', 'debug');
            return new Failure(
                'Please add handler settings for SecureCo. Full config should be:
                    {
                        config: {
                            env: <someenv>
                            <someenv>: {
                                apiBaseUrl: "..."
                                bearer: "..."
                                merchantAccountId: "..."
                                isProductionMode: "..."
                            }
                        }
                    }
                '
            );
        }

        //if update event, update donor token
        if ($event instanceof Subscription
            && array_key_exists('Product.productId', $event) //this type is iterable, will work with array_key_exists
            && $event['Product.productId'] == 'renewal'
        ) {
            return $this->_processUpdate($event);
        }

        //otherwise depending on one-off or recurring, make payment
        if ($event instanceof Subscription) {
            return $this->_processPayment($event, true);
        }

        return $this->_processPayment($event, false);
    }

    /**
     * Handle an update to an existing donor
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processUpdate(Subscription $event)
    {
        try {
            //generate the event
            $payload  = $this->_generateUpdatePaymentRequest($event);
            $url      = $this->_config['apiBaseUrl'] . '/V2/tokens/create';
            $response = $this->_callAPI('POST', $url, $payload);

            //if theres a message, there was an error
            if (isset($response->Message)) {
                MachinePack::log('Secure Co Error: ' . $response->Message, 'debug');
                return new Failure(
                    $response->Message
                );
            }

            //if response code is not set or is not 0, there was an error in taking payment
            if (!isset($response->APIResponseCode) || $response->APIResponseCode != 0) {
                MachinePack::log(
                    'Secure Co could not take payment, response code: ' .
                        $response->APIResponseCode,
                    'debug'
                );
                return new Failure(
                    'It appears your credit card has been declined. We have not processed your donation.'
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        //otherwise, payment was taken, return success
        return new Success(
            [
                'MoneyTransfer.identifier' => $response->APIData->TransactionID,
                'Intangible/APIResponse.APIResponseCode'               => $response->APIResponseCode,
                'Intangible/APIResponse.APIResponseMessage'            => $response->APIResponseMessage,
                'Intangible/APIResponse.AcquirerResponseCode'          => $response->APIData->AcquirerResponseCode,
                'Intangible/APIResponse.AcquirerResponseMessage'          => $response->APIData->AcquirerResponseMessage, //phpcs:ignore
                'Intangible/APIResponse.RequestID'                     => $response->APIData->RequestID,
                'Intangible/APIResponse.AuthorizationCode'             => $response->APIData->AuthorizationCode, //phpcs:ignore
                'Intangible/APIResponse.TransactionResponseCode'             => $response->APIData->TransactionResponse[0]->TransactionResponseCode, //phpcs:ignore
                'Intangible/APIResponse.TransactionResponseMessage'       => $response->APIData->TransactionResponse[0]->TransactionResponseMessage, //phpcs:ignore
                'Intangible/APIResponse.TokenID'       => $response->APIData->CardToken->TokenID, //phpcs:ignore
            ]
        );
    }

    /**
     * Handle a payment request (recurring or one off)
     * @param  Payment $event       payment information
     * @param  boolean $recurring   recurring payment flag
     * @return Success|Failure
     */
    private function _processPayment(Payment $event, $recurring = false)
    {
        try {
            //generate the event
            $payload  = $this->_generateSubmitPaymentRequest($event, $recurring);
            $url      = $this->_config['apiBaseUrl'] . '/V2/transactions/purchase';
            $response = $this->_callAPI('POST', $url, $payload);

            //if theres a message, there was an error
            if (isset($response->Message)) {
                MachinePack::log('Secure Co Error: ' . $response->Message, 'debug');
                return new Failure(
                    $response->Message
                );
            }

            //if response code is not set or is not 0, there was an error in taking payment
            if (!isset($response->APIResponseCode) || $response->APIResponseCode != 0) {
                MachinePack::log(
                    'Secure Co could not take payment, response code: ' .
                        $response->APIResponseCode,
                    'debug'
                );
                MachinePack::log($response->APIData->TransactionResponse, 'debug');
                return new Failure(
                    'It appears your credit card has been declined. We have not processed your donation.',
                    [
                        'MoneyTransfer.identifier' => $response->APIData->TransactionID,
                        'Intangible/APIResponse.APIResponseCode'               => $response->APIResponseCode, 'Intangible/APIResponse.APIResponseCode'               => $response->APIResponseCode, //phpcs:ignore
                        'Intangible/APIResponse.APIResponseMessage'            => $response->APIResponseMessage,
                        'Intangible/APIResponse.AcquirerResponseCode'          => $response->APIData->AcquirerResponseCode, //phpcs:ignore
                        'Intangible/APIResponse.AcquirerResponseMessage'          => $response->APIData->AcquirerResponseMessage, //phpcs:ignore
                        'Intangible/APIResponse.RequestID'                     => $response->APIData->RequestID,
                        'Intangible/APIResponse.AuthorizationCode'             => $response->APIData->AuthorizationCode, //phpcs:ignore
                        'Intangible/APIResponse.TransactionResponseCode'             => $response->APIData->TransactionResponse[0]->TransactionResponseCode, //phpcs:ignore
                        'Intangible/APIResponse.TransactionResponseMessage'       => $response->APIData->TransactionResponse[0]->TransactionResponseMessage, //phpcs:ignore
                    ]
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        //otherwise, payment was taken, return success
        return new Success(
            [
                'MoneyTransfer.identifier' => $response->APIData->TransactionID,
                'Intangible/APIResponse.APIResponseCode'               => $response->APIResponseCode,
                'Intangible/APIResponse.APIResponseMessage'            => $response->APIResponseMessage,
                'Intangible/APIResponse.AcquirerResponseCode'          => $response->APIData->AcquirerResponseCode,
                'Intangible/APIResponse.AcquirerResponseMessage'          => $response->APIData->AcquirerResponseMessage, //phpcs:ignore
                'Intangible/APIResponse.RequestID'                     => $response->APIData->RequestID,
                'Intangible/APIResponse.AuthorizationCode'             => $response->APIData->AuthorizationCode, //phpcs:ignore
                'Intangible/APIResponse.TransactionResponseCode'             => $response->APIData->TransactionResponse[0]->TransactionResponseCode, //phpcs:ignore
                'Intangible/APIResponse.TransactionResponseMessage'       => $response->APIData->TransactionResponse[0]->TransactionResponseMessage, //phpcs:ignore
                'Intangible/APIResponse.TokenID'       => $response->APIData->CardToken->TokenID, //phpcs:ignore
                'Intangible/APIResponse.MaskedAccountNumber'       => $response->APIData->CardToken->MaskedAccountNumber, //phpcs:ignore
                'Intangible/APIResponse.CompletionTimeStamp'       => $response->APIData->CompletionTimeStamp //phpcs:ignore
            ]
        );
    }

    /**
     * Get request data for sending a new donation
     *
     * @param string $requestID Unique key for request
     * @param Payment $event
     * @return array
     */
    private function _generateSubmitPaymentRequest($event, $recurring = false)
    {
        $paymentData =  array(
            "PaymentData" => array(
                "MerchantAccountID" => $this->_config['merchantAccountId'],
                "RequestedAmount" => $event['MonetaryAmount.value'] * 100, //use cents
                "Currency" => $event['MonetaryAmount.currency'],
                "RequestID" => $event['Order.identifier'], //Unique ID generated using PHP uniqueID();
                "PaymentMethod" => "creditcard",
                "AccountHolder" => array(
                    "FirstName" => $event['Person.givenName'],
                    "LastName"  => $event['Person.familyName'],
                    "Email"     => $event['Person.email'],
                    "Address"   => array(
                        "Street1" => $event['PostalAddress.streetAddress'],
                        "City" => $event['PostalAddress.addressLocality'],
                        "State" => $event['PostalAddress.addressRegion'],
                        "Postcode" => $event['PostalAddress.postalCode'],
                        "Country" => $event['PostalAddress.addressCountry'],
                    )
                ),
                "Card" => array(
                    "CardType" => $event['CreditCard/CardDetails.type'],
                    "AccountNumber" => $event['CreditCard/CardDetails.number'],
                    "ExpirationMonth" => $event['CreditCard/CardDetails.expiryMonth'],
                    "ExpirationYear" => $event['CreditCard/CardDetails.expiryYear'],
                    "CardSecurityCode" => $event['CreditCard/CardDetails.cvn']
                )
            )
        );
        if ($recurring) {
            $paymentData['PaymentData']['PeriodicType'] = 'recurring';
        }

        return $paymentData;
    }

    /**
     * Get request data for existing donor update account details and card details
     *
     * @param string $requestID Unique key for request
     * @param Payment $event
     * @return array
     */
    private function _generateUpdatePaymentRequest($event)
    {
        return array(
            "TokenData" => array(
                "MerchantAccountID" => $this->_config['merchantAccountIdUpdate'],
                "RequestID" => $event['Order.identifier'],
                "AccountHolder" => array(
                    "FirstName" => $event['Person.givenName'],
                    "LastName"  => $event['Person.familyName'],
                    "Email"     => $event['Person.email'],
                    "Address"   => array(
                        "Street1" => $event['PostalAddress.streetAddress'],
                        "City" => $event['PostalAddress.addressLocality'],
                        "State" => $event['PostalAddress.addressRegion'],
                        "Postcode" => $event['PostalAddress.postalCode'],
                        "Country" => $event['PostalAddress.addressCountry'],
                    )
                ),
                "Card" => array(
                    "CardType" => $event['CreditCard/CardDetails.type'],
                    "AccountNumber" => $event['CreditCard/CardDetails.number'],
                    "ExpirationMonth" => $event['CreditCard/CardDetails.expiryMonth'],
                    "ExpirationYear" => $event['CreditCard/CardDetails.expiryYear'],
                    "CardSecurityCode" => $event['CreditCard/CardDetails.cvn']
                )
            )
        );
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _callAPI($method, $url, $data = array())
    {
        $headers = array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->_config['bearer']
        );

        $curl      = curl_init();
        $data_json = json_encode($data);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        }

        $rawResponse = curl_exec($curl);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        $response = json_decode($rawResponse);

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $response;
    }

    /**
     * Get payment settings
     * @return array $settings
     */
    public function getPaymentSettings()
    {
        return [
            'secureco' => $this->settings[$this->settings['env']]
        ];
    }
}
