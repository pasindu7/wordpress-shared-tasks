<?php
namespace MachinePack\Core\Handler\Logic;

use MachinePack\Core\Event\Event;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Exception\Settings as SettingsException;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Collection as ResultCollection;
use MachinePack\Core\Util;

/**
 * Basic event router using templated events and conditions
 */
class Router extends Handler
{
    public function handleEvent(Event $event): Result
    {
        if (empty($this->config['routes'])) {
            throw new SettingsException(
                'Please provide "routes" mapping in settings. This looks like:
                    settings:
                        \'event.subevent.{{ template 1 }}\': |
                            {% if event.data.property == \'x\' %}
                                true
                            {% else %}
                                false
                            {% endif %}
                        \'event.subevent.{{ template 2 }}\': \'{% event condition 2 %}\'
                        ...
                        \'event.subevent.{{ template n }}\': \'{% event condition n %}\'
                '
            );
        }

        $twig_data = [
            'event' => $event,
            'settings' => $this->config
        ];
        $results   = [];
        foreach ($this->config['routes'] as $output => $condition) {
            $twig = new Util\Twig(
                [
                    'condition' => $condition,
                    'output' => $output
                ]
            );

            $should_route = boolval(trim($twig->render('condition', $twig_data)));
            $result_key   = "$output";

            if (!$should_route) {
                $results[$result_key] = new Ignored("$result_key: Condition not met");
                continue;
            }

            $routed_event         = strval(trim($twig->render('output', $twig_data)));
            $results[$result_key] = $this->forward($routed_event, $event->getData());
        }

        return new ResultCollection($results);
    }
}
