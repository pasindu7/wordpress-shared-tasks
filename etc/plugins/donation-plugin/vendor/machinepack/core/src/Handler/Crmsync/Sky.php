<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;

class Sky extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        //validate configuration variables
        if (empty($this->_config['subscription_key'])) {
            return new Failure(
                'Please provide subscription key SKY API integration. Full config should be:
                {
                    config: {
                        env: <someenv>,
                        <someenv>: {
                            subscription_key:
                        }
                    }
                }
            '
            );
        }

        if (isset($event['action']) && $event['action'] == 'get_look_up') {
            return $this->_returnConstituentLookUpID($event);
        }

        if (isset($event['action']) && $event['action'] == 'get_auth_url'
            && !isset($event['Intangible/Sky.access_token'])
        ) {
            return $this->_createAuthURL();
        }

        if (isset($event['action']) && $event['action'] == 'get_access_token'
            && isset($event['Intangible/Sky.code'])
        ) {
            return $this->_exchangeCodeForAccessToken($event);
        }

        return $this->_processGift($event);
    }

    private function _returnConstituentLookUpID($event)
    {
        $token_response      = array();
        $constituentResponse = $this->_getConstituent($event);
        if (isset($constituentResponse->statusCode) && $constituentResponse->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituentResponse = $this->_getConstituent($event, $token_response);
        }
        if (!isset($constituentResponse->statusCode)) {
            if (isset($constituentResponse->count) && $constituentResponse->count > 0) {
                return new Success(
                    [
                        'Person.identifier' => $constituentResponse->value[0]->id,
                        'Person.lookup.identifier' => $constituentResponse->value[0]->lookup_id,
                        'Intangible/Sky.refresh_token'  => isset($token_response['refresh_token'])
                        ? $token_response['refresh_token'] : '',
                        'Intangible/Sky.access_token'   => isset($token_response['access_token'])
                        ? $token_response['access_token'] : ''
                     ]
                );
            }
        } else {
            return new Failure($constituentResponse->statusCode . ' ' . $constituentResponse->message);
        }
    }

    private function _processGift($event)
    {
        $token_response      = array();
        $constituentResponse = $this->_createConstituent($event);
        if (isset($constituentResponse->statusCode) && $constituentResponse->statusCode == 401) {
            $token_response = $this->_refreshToken($event);
            if ($token_response instanceof Failure) {
                return $token_response;
            }
            $constituentResponse =  $this->_createConstituent($event, $token_response);
        }

        if ($constituentResponse instanceof Failure) {
            return $constituentResponse;
        }

        if (!empty($constituentResponse)) {
            if (isset($constituentResponse->statusCode) && $constituentResponse->statusCode == 401) {
                return new Failure(
                    $constituentResponse->message,
                    array('errorcode' => $constituentResponse->statusCode)
                );
            }
            if (is_array($constituentResponse) && !isset($constituentResponse->id)) {
                return new Failure(
                    $constituentResponse[0]->message,
                    array('errorcode' => $constituentResponse[0]->error_code)
                );
            } elseif (isset($constituentResponse->Errors)) {
                return new Failure(
                    $constituentResponse->message
                );
            }

            $constituentId = $constituentResponse->id;
            $giftResponse  = json_decode($this->_createGift($event, $constituentId, $token_response));

            if (is_array($giftResponse) && isset($giftResponse[0]->error_code)) {
                return new Failure(
                    $giftResponse[0]->message,
                    [
                        'errorcode' => $giftResponse[0]->error_code,
                        'Person.identifier' => $constituentResponse->id
                    ]
                );
            }
            return new Success(
                [
                    'Person.identifier'             => $constituentId,
                    'MoneyTransfer.identifier'      => $giftResponse->id,
                    'Intangible/Sky.refresh_token'  => isset($token_response['refresh_token'])
                    ? $token_response['refresh_token'] : '',
                    'Intangible/Sky.access_token'   => isset($token_response['access_token'])
                    ? $token_response['access_token'] : ''
                ]
            );
        } else {
            return new Failure('Error:: Invalid API response while retrieving Constituent Id.');
        }
    }

    /**
     * Create a constituent
     * @param Crmsync $event
     */
    private function _createConstituent($event, $token = false)
    {
        try {
            $auth_key = $event['Intangible/Sky.access_token'];
            if (isset($token['access_token'])) {
                $auth_key = $token['access_token'];
            }
            $headers = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . $auth_key
            );

            $data = (object) array(
                'deceased' => 'false',
                'first' => $event['Intangible/Event.payload']['address_first_name'],
                'last' => $event['Intangible/Event.payload']['address_last_name'],
                "email" => array(
                    "address" => $event['Intangible/Event.payload']['address_email'],
                    "do_not_email" => false,
                    "inactive" => false,
                    "primary" => true,
                    "type" => "Email"
                ),
                "phone" => array (
                    "do_not_call" =>  false,
                    "inactive" => false,
                    "number" => $event['Intangible/Event.payload']['address_phone'],
                    "primary" => true,
                    "type" => $event['Intangible/Event.payload']['phone_type']
                ),
                "address" => array(
                    "address_lines" => $event['Intangible/Event.payload']['address_street'],
                    "city" =>  $event['Intangible/Event.payload']['address_suburb'],
                    "state" => $event['Intangible/Event.payload']['address_state'],
                    "country" => $event['Intangible/Event.payload']['address_country'],
                    "do_not_mail" => false,
                    "postal_code" => $event['Intangible/Event.payload']['address_postcode'],
                    "preferred" => true,
                    "type" => "Home"
                ),
                "type" => $event['Intangible/Event.payload']['type']
            );

            $constituentResponse = $this->_getConstituent($event, $token);

            if (!isset($constituentResponse->statusCode)  && $constituentResponse->statusCode != 401) {
                if (isset($constituentResponse->count) && $constituentResponse->count == 0) {
                    $method = 'POST';
                    $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents';

                    $constituentResponse = json_decode(
                        $this->_skyApiCall(
                            $url,
                            $method,
                            $headers,
                            $data
                        )
                    );
                } else {
                    $constituentResponse = $constituentResponse->value[0];
                    $constituentID       = $constituentResponse->id;
                    $method              = 'PATCH';
                    $url                 = 'https://api.sky.blackbaud.com/constituent/v1/constituents/' .
                                            $constituentID;
                    $data                = (object) array(
                        "first" => $event['Intangible/Event.payload']['address_first_name'],
                        "last" => $event['Intangible/Event.payload']['address_last_name'],
                    );

                    $this->_skyApiCall(
                        $url,
                        $method,
                        $headers,
                        $data
                    );
                }
            } else {
                return $constituentResponse;
            }

            if (!empty($constituentResponse)) {
                if (isset($constituentResponse->err)) {
                    return new Failure($constituentResponse->err);
                }
            }

            return $constituentResponse;
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
            //TODO should log stacktrace
        }
    }

    private function _getConstituent($event, $token = false)
    {
        $auth_key = $event['Intangible/Sky.access_token'];
        if (isset($token['access_token'])) {
            $auth_key = $token['access_token'];
        }
        $headers = array(
            "Content-Type: application/json",
            "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
            "Authorization: Bearer " . $auth_key
        );

        if (isset($event['Intangible/Event.payload']['address_email'])
            && $event['Intangible/Event.payload']['address_email']
        ) {
            $url    = 'https://api.sky.blackbaud.com/constituent/v1/constituents/search?search_text=' .
                        $event['Intangible/Event.payload']['address_email'] . '&include_inactive=false&limit=1';
            $method = 'GET';

            return json_decode(
                $this->_skyApiCall(
                    $url,
                    $method,
                    $headers,
                    array()
                )
            );
        } else {
            return new Failure('Error - No Email provided.');
        }
    }

    /**
     * Create a gift payment
     * @param Crmsync $event
     */

    private function _createGift($event, $constituentId, $token = false)
    {
        try {
            $auth_key = $event['Intangible/Sky.access_token'];
            if (isset($token['access_token'])) {
                $auth_key = $token['access_token'];
            }
            $url = 'https://api.sky.blackbaud.com/gift/v1/gifts';

            $headers = array(
                "Content-Type: application/json",
                "Bb-Api-Subscription-Key: " . $this->_config['subscription_key'],
                "Authorization: Bearer " . $auth_key
            );

            $data = array(
                'amount' => array(
                    'value' => $event['Intangible/Event.payload']['donation_amount']
                ),
                'constituent_id' => $constituentId,
                'date' => date('Y-m-d'),
                'gift_splits' => array(
                    array (
                        'amount' => array (
                            'value' => $event['Intangible/Event.payload']['donation_amount'],
                        ),
                        'appeal_id' => $event['Intangible/Gift.appeal_id'],
                        'campaign_id' => $event['Intangible/Gift.campaign_id'],
                        'fund_id' => $event['Intangible/Gift.fund_id'],
                    ),
                ),
                'gift_status' => 'Active',
                'is_anonymous' => false,
                'payments' => array(
                    array (
                        'payment_method' => $event['Intangible/Event.payload']['gift_payment_method'],
                        'checkout_transaction_id' => $event['Intangible/Event.payload']['transaction_id'],
                        'bbps_transaction_id' => $event['Intangible/Event.payload']['transaction_id'],
                        'reason' => $event['Intangible/Event.payload']['reason']
                    ),
                ),
                'post_status' => 'DoNotPost',
                'reference' => $event['Intangible/Event.payload']['gift_reference'],
                "type" => $event['Intangible/Event.payload']['gift_payment_type']
            );

            if ($event['Intangible/Event.payload']['gift_payment_type'] === 'RecurringGift'
                || $event['Intangible/Event.payload']['gift_payment_type'] === 'RecurringGiftPayment'
            ) {
                $data['payments'][0]['bbps_configuration_id'] = $event['Intangible/Event.payload']['payment_config_id'];
                $data['payments'][0]['account_token']         = $event['Intangible/Event.payload']['card_token'];
                $data['recurring_gift_schedule']              = array(
                    'frequency'     => $event['Intangible/Event.payload']['donation_frequency'],
                    'start_date'    => isset($event['Intangible/Event.payload']['payment_date']) &&
                    $event['Intangible/Event.payload']['payment_date'] ?
                date("c", strtotime(date($event['Intangible/Event.payload']['payment_date']))) :
                date("c", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "+1 month"))
                );
            }

            $response = $this->_skyApiCall(
                $url,
                'POST',
                $headers,
                $data
            );

            return $response;
        } catch (\Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    private function _createAuthURL()
    {
        $auth_data = array(
            'client_id' => $this->_config['client_id'],
            'response_type' => 'code',
            'redirect_uri' => $this->_config['redirect_uri']
        );

        $auth_uri = 'https://oauth2.sky.blackbaud.com/authorization?' . http_build_query($auth_data);

        return new Success(
            [
                'status' => 'URL Creation',
                'url'    => $auth_uri
            ]
        );
    }

    private function _exchangeCodeForAccessToken($event)
    {
        $body = array(
            'grant_type' => 'authorization_code',
            'redirect_uri' => $this->_config['redirect_uri'],
            'code' => $event['Intangible/Sky.code']
        );

        $token = $this->_fetchTokens($body);
        if (!($token instanceof Failure)) {
            return new Success(
                [
                $token
                ]
            );
        }
        return $token;
    }

    private function _refreshToken($event)
    {
        $refresh_token = $event['Intangible/Sky.refresh_token'];
        $body          = array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $refresh_token,
            //'preserve_refresh_token' => 'true'
        );

        return $this->_fetchTokens($body);
    }

    private function _fetchTokens($body = array())
    {
        $headers = array(
            'Content-type: application/x-www-form-urlencoded',
            'Authorization: Basic ' . base64_encode($this->_config['client_id'] . ':' . $this->_config['client_secret'])
        );

        $url = "https://oauth2.sky.blackbaud.com/token";

        $response = $this->_skyApiCall($url, 'POST', $headers, $body, true);
        $token    = json_decode($response, true);

        if (isset($token['error']) && $token['error']) {
            return new Failure(
                $token['error']
            );
        }

        return $token;
    }

    /**
     * Call the API via curl
     *
     * @param string $method POST|GET|PUT
     * @param string $url
     * @param array $data
     * @return void
     */
    private function _skyApiCall($url, $method, $headers, $data, $auth = false)
    {
        $params = array();

        if ($auth) {
            $params = http_build_query($data);
        } else {
            $params = json_encode($data);
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSLVERSION, 0);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } elseif ($method == "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        } elseif ($method == "PATCH") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        }

        $response = curl_exec($curl);

        if (curl_error($curl)) {
            $error_msg = curl_error($curl);
        }

        curl_close($curl);

        if (isset($error_msg)) {
            throw new \Exception($error_msg);
        }

        if ($response === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Could not parse JSON response');
        }

        return $response;
    }
}
