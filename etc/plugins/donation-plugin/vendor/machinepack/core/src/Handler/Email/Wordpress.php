<?php
namespace MachinePack\Core\Handler\Email;

/**
 * Base email handler. Does both config and templating
 * Subclass for different send methods.
 */
class Wordpress extends Email
{
    protected function send(string $to, string $subject, string $message, string $headers = '', array $attachments = [])
    {
        return \wp_mail($to, $subject, $message, $headers, $attachments);
    }
}
