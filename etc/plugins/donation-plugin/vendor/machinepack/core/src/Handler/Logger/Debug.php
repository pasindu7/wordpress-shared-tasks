<?php
namespace MachinePack\Core\Handler\Logger;

use MachinePack\Core\Event\Event;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Result;

class Debug extends Handler
{
    /**
     * Suport for generic log templates using Twig
     *
     * @param Event $event event to log
     * @return string
     */
    private function _getMessage(Event $event)
    {
        if (!empty($this->_config['template'])) {
            try {
                $twig   = new \Twig\Environment(
                    new \Twig\Loader\ArrayLoader(
                        [
                            'log' => $this->_config['template']
                        ]
                    )
                );
                $filter = new \Twig\TwigFilter('json_decode', 'json_decode');
                $twig->addFilter($filter);
                return $twig->render(
                    'log',
                    [
                        'env' => $this->settings['env'],
                        'data' => $event->getData(),
                        'event' => $event->getEvent(),
                        'config' => $this->_config
                    ]
                );
            } catch (\Throwable $e) {
                error_log($e);
                $twig = null;
            }
        }

        if (empty($event['Intangible/Log.message'])) {
            throw new \Exception('No log message supplied and no template defined');
        }

        //display the message
        $level   = $event['Intangible/Log.level'];
        $message = (is_string($event['Intangible/Log.message'])) ?
            $event['Intangible/Log.message'] :
            print_r($event['Intangible/Log.message'], true);

        return sprintf(
            "%s:\t[%s] %s %s",
            date('c'),
            $level,
            $message,
            PHP_EOL
        );
    }

    /**
     * @see Handler\handleEvent
     *
     * @param Event $event
     * @return void
     */
    public function handleEvent(Event $event): Result
    {
        $this->_config = $this->settings[$this->settings['env']];

        $message = $this->_getMessage($event);

        // echo
        if ($this->_config['echo']) {
            echo $message;
            if (!empty($event['Intangible/Log.stackTrace'])) {
                echo $event['Intangible/Log.stackTrace'] . PHP_EOL;
            }
        }

        // log
        if ($this->_config['errorLog']) {
            error_log($message);
            if (!empty($event['Intangible/Log.stackTrace'])) {
                error_log($event['Intangible/Log.stackTrace'] . PHP_EOL);
            }
        }

        return new Success($message);
    }
}
