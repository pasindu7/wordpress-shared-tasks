<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Event\Events\Crmsync;

class SendGrid extends Handler
{
    private $_config;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        if ($this->_config['apiKey']) {
            //validate configuration variables
            if (empty($this->_config['apiKey'])) {
                return new Failure(
                    'Please provide apiKey and ClientId or add settings for Campaign Monitor. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                apiKey:
                            }
                        }
                    }
                '
                );
            }
        }

        return $this->_sendTransactionalEmail($event);
    }


    /**
     * Submit the transaction email event payload to Campaign Monitor
     * @param Crmsync $event
     */
    private function _sendTransactionalEmail(Crmsync $event)
    {
        if (!isset($event['Intangible/SendGrid.smart_id'])) {
            return new Failure('Error:: Message - Incorrect Transaction Smart Email ID.');
        }

        if (!is_array($event['EmailMessage.recipient.email'])) {
            return new Failure(
                'Error:: Message - An array of recipients.
            Must always contain the email, of a recipient.'
            );
        }

        if (!isset($event['EmailMessage.sender.email'])
            || !filter_var($event['EmailMessage.sender.email'], FILTER_VALIDATE_EMAIL)
        ) {
            return new Failure('Error:: Message - Incorrect recipient email.');
        }

        if (isset($event['Intangible/Event.payload'])
            && !is_array($event['Intangible/Event.payload'])
        ) {
            return new Failure('Error:: Message - variables must be in array.');
        }

        $url = 'https://api.sendgrid.com/v3/mail/send';

        $emailData = array (
            'from' => array (
                'name' => $event['EmailMessage.sender.name'],
                'email' => $event['EmailMessage.sender.email']
            ),
            'personalizations' => array (
                array (
                    'to' => $event['EmailMessage.recipient.email'],
                    'dynamic_template_data' => $event['Intangible/Event.payload']
                ),
            ),
            'template_id' => $event['Intangible/SendGrid.smart_id'],
        );

        $response = $this->_curl('POST', $url, $emailData);

        if (!empty($response)) {
            $response = json_decode($response);
            if (isset($response->errors)) {
                $errors = '';
                foreach ($response->errors as $error) {
                    $errors .= $error->message . ' ';
                }
                return new Failure('Error:: ' . $errors);
            }
        }

        return new Success(
            [
                'Successfully sent email'
            ]
        );
    }

    /**
     * Make a curl request
     *
     * @param string $method    GET|POST
     * @param string $url       the url to call
     * @param string $bearer    the bearer token for authentication
     * @param array $data       request payload
     * @return void
     */
    private function _curl($method, $url, $data = array())
    {
        try {
            $headers    = array(
                "Content-Type: application/json",
                "Authorization: Bearer " . $this->_config['apiKey']
            );
            $input_data = json_encode($data);

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PATCH') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            } elseif ($method == 'DELETE') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            } else {
                curl_setopt($curl, CURLOPT_HEADER, 0);
            }

            $result = curl_exec($curl);
            curl_close($curl);

            return $result;
        } catch (Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }
}
