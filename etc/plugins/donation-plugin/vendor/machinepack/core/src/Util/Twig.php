<?php
namespace MachinePack\Core\Util;

use MachinePack\Core\Exception\Settings as SettingsException;

final class Twig extends \Twig\Environment
{
    private $_twig;
    public function __construct(array $templates = [], array $paths = [])
    {
        $loaders = [];
        if (!empty($templates)) {
            $loaders[] = new \Twig\Loader\ArrayLoader(array_map('strval', $templates));
        }
        if (!empty($paths)) {
            $loaders[] = new \Twig\Loader\FilesystemLoader(array_unique(array_map('realpath', $paths)));
        }
        if (empty($loaders)) {
            throw new SettingsException('Provide at least a template or a path for Twig');
        }
        parent::__construct(new \Twig\Loader\ChainLoader($loaders));
        $this->addFilter(new \Twig\TwigFilter('json_decode', 'json_decode'));
    }
}
