<?php
namespace MachinePack\Core\Result;

class Failure extends Result
{
    public $reason;
    public $data;

    public function __construct($reason, $data = [])
    {
        parent::__construct();
        $this->reason = strval($reason);
        $this->data   = $data;
    }

    public function asHttpResponseData()
    {
        $data           = parent::asHttpResponseData();
        $data['reason'] = $this->reason;
        return $data;
    }

    public function asHttpCode()
    {
        return 500;
    }
}
