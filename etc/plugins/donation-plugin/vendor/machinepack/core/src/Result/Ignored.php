<?php
namespace MachinePack\Core\Result;

/**
 * this class represents cases where a handler doesn't care about the
 * event it was passed
 */
class Ignored extends Result
{
}
