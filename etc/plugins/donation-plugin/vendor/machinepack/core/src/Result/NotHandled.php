<?php
namespace MachinePack\Core\Result;

/**
 * If an event is sent but no handlers were actually called, this is the result.
 * This shouldn't happen if config is consistent, but prevents object being used
 * inconsistently.
 */
class NotHandled extends Result
{
}
