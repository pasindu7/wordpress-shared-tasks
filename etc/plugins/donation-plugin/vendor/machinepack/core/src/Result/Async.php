<?php
namespace MachinePack\Core\Result;

/**
 * This result represents an asynchronous process. It should always contain:
 * - a jobId that is unique across all systems (e.g., UUID)
 * - a jobStatusUrl where that ID can be checked
 */
class Async extends Result
{
    public $jobId;
    public $jobStatusUrl;

    public function __construct($jobStatusUrl, $jobId)
    {
        parent::__construct();
        $this->jobId        = strval($jobId);
        $this->jobStatusUrl = strval($jobStatusUrl);
    }

    public function asHttpResponseData()
    {
        $data                 = parent::asHttpResponseData();
        $data['jobId']        = $this->jobId;
        $data['jobStatusUrl'] = $this->jobStatusUrl;
        return $data;
    }
}
