<?php
namespace MachinePack\Core\Result;

use MachinePack\Core\Exception;

/**
 * This result represents a collection of results
 */
class Collection extends Result implements \ArrayAccess
{
    public $data;

    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (! $value instanceof Result) {
            throw new Exception\Semantic('Result collection values must be Result instances.');
        }
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    public function __construct(array $results)
    {
        parent::__construct();
        $this->data = $results;
    }

    public function asHttpResponseData()
    {
        return [
            'type' => (string) $this,
            'data' => array_map(
                function ($result) {
                    return $result->asHttpResponseData();
                },
                $this->data
            )
        ];
    }

    public function asHttpCode()
    {
        return 200;
    }
}
