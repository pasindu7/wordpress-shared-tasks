<?php
namespace MachinePack\Core;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Tag\TaggedValue;
use MachinePack\Core\Exception\Settings as SettingsException;

abstract class MachinePack
{
    /**
     * Event broker instance doing all extra methods
     * @var Event\Broker
     */
    private static $_broker;

    /**
     * All registered handlers, by alias
     * @var Array<Handler>
     */
    private static $_handlers;

    /**
     * Current config
     * @var array
     */
    private static $_config;

    /**
     * Checks for machinepack config tags and expands them
     * @var mixed|TaggedValue
     */
    private static function _parseYamlTags(&$value)
    {
        if ($value instanceof TaggedValue && substr($value->getTag(), 0, 3) === 'env') {
            $tag = $value->getTag();
            $var = $value->getValue();
        } elseif (preg_match('/^!(env\(.*\)) (\w+)$/', $value, $matches)) {
            list ($_, $tag, $var) = $matches;
        } else {
            return;
        }

        $value = $_ENV[$var] ?? $_SERVER[$var] ?? getenv($var);
        if (!preg_match('/^env\(.*\)$/', $tag)) {
            return;
        }

        parse_str(substr($tag, 4, -1), $extensions);
        if (isset($extensions['default']) && empty($value)) {
            $value = $extensions['default'];
        }
        if (isset($extensions['callback']) && is_callable($extensions['callback'])) {
            $value = call_user_func($extensions['callback'], $value);
        }
    }

    /**
     * Actually initialise this singleton object
     * @param  mixed $config config file to read or config settings to use
     */
    private static function _init($config_in = null)
    {
        self::$_broker = new Event\Broker;

        if (is_null($config_in) && defined('MACHINEPACK_CONFIG')) {
            $config_in = MACHINEPACK_CONFIG;
        }

        if (is_string($config_in) && is_file($config_in)) {
            try {
                $config = Yaml::parse(file_get_contents($config_in), Yaml::PARSE_CONSTANT | Yaml::PARSE_CUSTOM_TAGS);
                array_walk_recursive($config, array(self::class, '_parseYamlTags'));
            } catch (ParseException $e) {
                throw new SettingsException($e);
            }
        } elseif (is_array($config_in)) {
            $config = $config_in;
        } else {
            throw new SettingsException(
                sprintf(
                    'Config "%s" was either an unreadable file or not an array.',
                    $config_in
                )
            );
        }

        if (!empty($config['handlers'])) {
            foreach ($config['handlers'] as $alias => $settings) {
                $class = sprintf('\\MachinePack\\Core\\Handler\\%s', $settings['class']);
                if (!class_exists($class)) {
                    throw new \Exception(
                        sprintf(
                            'Handler %s is not available.',
                            $class
                        )
                    );
                }
                self::$_handlers[$alias] = new $class(self::$_broker, $settings['settings']);
            }
        }

        if (!empty($config['events'])) {
            foreach ($config['events'] as $event => $handlers) {
                foreach ($handlers as $alias) {
                    self::$_broker->subscribe($event, array(self::$_handlers[$alias], 'handleEvent'));
                }
            }
        }

        self::$_config = $config;
    }

    /**
     * Initialise MachinePack. This is implicitly called on the first method call.
     * Call explicitly to set custom configuration.
     * @param  mixed $config config to use (filename or array)
     */
    public static function init($config = null)
    {
        self::_init($config);
    }

    /**
     * Initialise and return current MachinePack configuration
     * @param  mixed $config optional injected config like init
     * @return Array         parsed config
     */
    public static function config($config = null)
    {
        self::_init($config);
        return self::$_config;
    }

    /**
     * Return a handler by alias
     * @param  string $alias handler alias
     * @return Handler
     */
    public static function handler($alias)
    {
        return self::$_handlers[$alias] ?? null;
    }

    /**
     * Return all registered handlers
     * @return Array<alias=>Handler>
     */
    public static function handlers()
    {
        return self::$_handlers ?? [];
    }

    /**
     * Create a log event. Shorthand function for convenience.
     *
     * @param string $level         debug,info,error
     * @param mixed $message        the message, object or type to log
     * @param string $stackTrace    optional stack trace
     * @return Result               SUCCESS|FAILURE
     */
    public static function log($message, $level = 'debug', $stackTrace = null)
    {
        return self::send(
            'log.create',
            [
                'Intangible/Log.level'=>$level,
                'Intangible/Log.message'=>$message,
                'Intangible/Log.stackTrace'=>$stackTrace
            ]
        );
    }

    /**
     * pass any extra methods to the broker, when available
     * @param  string $name      method name
     * @param  array  $arguments method arguments
     * @return mixed             broker result
     */
    public static function __callStatic(string $name, array $arguments)
    {
        if (!self::$_broker) {
            self::_init();
        }

        $callable = array(self::$_broker, $name);

        if (!is_callable($callable)) {
            throw new \Exception(
                sprintf(
                    'Method %s is not available.',
                    $name
                )
            );
        }

        return call_user_func_array($callable, $arguments);
    }
}
