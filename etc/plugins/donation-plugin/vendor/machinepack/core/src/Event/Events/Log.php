<?php
namespace MachinePack\Core\Event\Events;

use MachinePack\Core\Event\Event;

class Log extends Event
{
    protected $required_data = [
        'Intangible/Log.message'
    ];

    public function __toString()
    {
        return @sprintf(
            'Log "%s"',
            $this['Intangible/Log.message']
        );
    }
}
