<?php
namespace MachinePack\Core\Event\Events;

use MachinePack\Core\Event\Event;

class Payment extends Event
{
    protected $required_data = [
        'MonetaryAmount.currency',
        'MonetaryAmount.value'
    ];

    public function __toString()
    {
        return @"{$this['MonetaryAmount.value']} {$this['MonetaryAmount.currency']}";
    }
}
