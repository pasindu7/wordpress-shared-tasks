<?php
namespace MachinePack\Core\Event\Events;

use MachinePack\Core\Event\Event;

/**
 * This event represents plain data in transit.
 * It should have no validation.
 */
class Data extends Event
{
    public function __toString()
    {
        return @json_encode((array) $this);
    }
}
