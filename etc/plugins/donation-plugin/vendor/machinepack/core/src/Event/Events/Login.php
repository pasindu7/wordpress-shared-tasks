<?php
namespace MachinePack\Core\Event\Events;

use MachinePack\Core\Event\Event;

class Login extends Event
{
    public function __toString()
    {
        return @sprintf(
            'Person "%s" logged in',
            $this['Person.name'] ?? @"{$this['Person.givenName']} {$this['Person.familyName']}" ?? 'Unknown'
        );
    }
}
