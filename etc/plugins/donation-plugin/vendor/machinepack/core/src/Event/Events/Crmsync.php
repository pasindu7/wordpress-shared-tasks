<?php
namespace MachinePack\Core\Event\Events;

use MachinePack\Core\Event\Event;

class Crmsync extends Event
{
    protected $required_data = [
        'Intangible/Event.payload'
    ];
}
