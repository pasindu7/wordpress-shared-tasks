<?php
namespace MachinePack\Core\Event;

use MachinePack\Core\Exception\Semantic as SemanticException;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;

/**
 * Base event class that gets passed around between machinepacks
 * @var Event
 */
abstract class Event implements \ArrayAccess
{
    /**
     * Holds all event data
     * @var array
     */
    private $_data;

    /**
     * Holds event details
     * @var array
     */
    private $_event;

    /**
     * Holds required data values
     * @var array<string>
     */
    protected $required_data = array();

    /**
     * Event instantiator from string representations
     * @param  string $path full event string (e.g., payment.send, payment.success, etc)
     * @param  array  $data data payload
     * @return Event  Generated event
     */
    public static function from(string $path, array $data)
    {
        $class = self::exists($path);

        if (is_null($class)) {
            throw new SemanticException(
                sprintf(
                    'Event path doesn\'t map to a defined Event object (tried %s)',
                    $path
                )
            );
        }

        return new $class(array_slice(explode('.', $path), 1), $data);
    }

    /**
     * Check if an event exists on this system
     * @param  string $path full event string
     * @return string|null class name or null if there isn't an event
     */
    public static function exists(string $path)
    {
        $parts = explode('.', $path);

        $result = null;

        while ($parts) {
            $class = sprintf('\\MachinePack\\Core\\Event\\Events\\%s', implode('\\', array_map('ucfirst', $parts)));
            if (class_exists($class)) {
                return $class;
            }
            $parts = array_slice($parts, 0, -1);
        }

        return null;
    }

    public function __construct(array $event, array $data)
    {
        $this->_data  = $data;
        $this->_event = $event;

        $args_diff = array_diff($this->required_data, array_keys($data));

        if (!empty($args_diff)) {
            throw new MissingArgumentsException(get_class($this), $args_diff);
        }
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getEvent()
    {
        return $this->_event;
    }

    public function offsetExists($offset)
    {
        return isset($this->_data[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->_data[$offset];
    }

    public function offsetSet($offset, $data)
    {
        throw new SemanticException('Can\'t set event values after event was created.');
    }

    public function offsetUnset($offset)
    {
        throw new SemanticException('Can\'t delete event values after event was created.');
    }
}
