<?php
namespace MachinePack\Core\Event;

use MachinePack\Core\Result\Result;
use MachinePack\Core\Result\NotHandled;
use MachinePack\Core\Exception\Semantic;

/**
 * This class handles event passing and handler registration
 * @var Broker
 */
final class Broker
{
    /**
     * List of events and connected handlers
     * @var Array<string, Array<callable>>
     */
    private $_handlers = array();

    /**
     * Send a raw event with some data
     * @param  string $event_path event in dot notation (a.b.c)
     * @param  array  $data       data (k=>v)
     * @return Array<MachinePack\Core\Result\Result> a result instance with the appropriate subclass
     */
    public function send(string $event_path, array $data)
    {
        $evt     = Event::from($event_path, $data);
        $results = [];

        if (!isset($this->_handlers[$event_path])) {
            $results[] = new NotHandled;
            return $results;
        }

        foreach ($this->_handlers[$event_path] as $callable) {
            $result = call_user_func($callable, $evt);
            if (!$result instanceof Result) {
                throw new Semantic(
                    sprintf(
                        'Handler "%s" must return a Result instance',
                        (is_array($callable) && is_object($callable[0])) ?
                        get_class($callable[0]) : (is_object($callable) ?
                        get_class($callable) : gettype($callable))
                    )
                );
            }
            array_push($results, $result);
            $result = null;
        }

        return $results;
    }

    /**
     * Subscribe to an event channel
     * @param  string   $event_path full path of event to listen to
     * @param  callable $handler    handler that will be called with event instance
     */
    public function subscribe(string $event_path, callable $handler)
    {
        $this->_handlers[$event_path][] = $handler;
    }
}
