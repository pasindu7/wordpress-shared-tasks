<?php
// Help function to require libriaries in subfolders
require 'require_all.php';

//Import vendor libraries
require './vendor/autoload.php';

//Import local libraries including all subfolders
require './src/Handler/Handler.php'; //Not a folder
Require_all('./src/Event');
Require_all('./src/Exception');
Require_all('./src/Handler/Crmsync');
Require_all('./src/Handler/Logger');
Require_all('./src/Handler/Payment');
//_require_all('./src/Handler/Purchase'); //Doesn't work at the moment - not properly implemented
Require_all('./src/Handler/Webhook');
//Result class already defined in vendor libraries, requiring all other result classes
require './src/Result/Async.php';
require './src/Result/Failure.php';
require './src/Result/Ignored.php';
require './src/Result/NotHandled.php';
require './src/Result/Success.php';
Require_all('./test/');

//Run specific test
    //$cmTest = new \MachinePack\Core\Test\Unit\MachinePackBBMSTest();
    //$event  = $cmTest->testProcessPayment();
//Run specific test using phpunit
//vendor\bin\phpunit test\Unit\MachinePackCampaignMonitorTest.php

die('Done.');
