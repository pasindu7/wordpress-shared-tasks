<?php
namespace MachinePack\Core\Handler\DonationPlugin;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Payments\AuthorizationsCaptureRequest;
use PayPalCheckoutSdk\Payments\AuthorizationsGetRequest;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalHttp\HttpRequest;
use MachinePack\Core\Event\Event;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;

class CustomPayPalPlanCreateRequest extends HttpRequest
{
    private $_intervals = [
        'monthly' => ['MONTH', 1]
    ];
    // 'MonetaryAmount.value': '123',
    // 'MonetaryAmount.currency': 'AUD',
    // 'Order.identifier': null,
    // 'Order.recurring': 'MONTHLY',
    // 'Person.givenName': null,
    // 'Person.familyName': null,
    // 'Person.telephone': null,
    // 'Person.email': null,
    // 'Person.notification': 'EMAIL',
    // 'PostalAddress.streetAddress': null,
    // 'Product\/Subscription.duration': '',
    // 'Product.productId': ''

    function __construct(Event $event)
    {
        parent::__construct('/v1/billing/plans', 'POST');
        $this->headers['Content-Type'] = 'application/json';

        $value = $event['MonetaryAmount.value'];
        $currency = $event['MonetaryAmount.currency'];

        $product_id = $event['Product.productId'];
        $product_name = $event['Product.name'] ?? sprintf('Donation plan (%s %s)', $value, $currency);

        if (empty($this->_intervals[strtolower($event['Order.recurring'])])) {
            throw new \Exception('Missing or invalid Order.recurring');
        }
        // FIXME
        list ($interval_unit, $interval_count) = $this->_intervals[strtolower($event['Order.recurring'])];
        $setup_fee_value = 0;
        $setup_fee_currency = 'AUD';
        $payment_failure_threshold = 3;
        $continue_when_failed = false;
        $tax = 0;
        $tax_inclusive = false;
        $trial_period_days = isset($event['Product/Subscription.start.date'])?$event['Product/Subscription.start.date']:0;

        $this->data = [
            'product_id' => $product_id,
            'name' => $product_name,
            'status' => 'ACTIVE',
            'billing_cycles' => [
                [
                    'frequency' => [
                        'interval_unit' => 'DAY',
                        'interval_count' => $trial_period_days
                    ],
                    'tenure_type' => 'TRIAL',
                    'sequence' => 1,
                    'total_cycles' => 1,
                    'pricing_scheme' => [
                        'fixed_price' => [
                            'value' => 0,
                            'currency_code' => $currency
                        ]
                    ]
                ],
                [
                    'frequency' => [
                        'interval_unit' => $interval_unit,
                        'interval_count' => $interval_count
                    ],
                    'tenure_type' => 'REGULAR',
                    'sequence' => 2,
                    'total_cycles' => 0,
                    'pricing_scheme' => [
                        'fixed_price' => [
                            'value' => $value,
                            'currency_code' => $currency
                        ]
                    ]
                ]
            ],
            'payment_preferences' => [
                'auto_bill_outstanding' => true,
                'setup_fee' => [
                    'value' => $setup_fee_value,
                    'currency_code' => $setup_fee_currency
                ],
                'setup_fee_failure_action' => $continue_when_failed ? 'CONTINUE' : 'CANCEL',
                'payment_failure_threshold' => $payment_failure_threshold
            ],
            'taxes' => [
                'percentage' => $tax,
                'inclusive' => $tax_inclusive
            ]
        ];
        $this->body = json_encode($this->data);
    }
}
