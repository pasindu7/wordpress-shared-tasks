<?php
namespace MachinePack\Core\Handler\DonationPlugin;

use MachinePack\Core\Handler\Payment\Stripe;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Event;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

/**
 * Subscription handling using products, not plans, and
 * ad-hoc prices
 */
class CustomStripe extends Stripe
{
    const MACHINEPACK_CUSTOMER_ID = 'Intangible/Customer.id';
    private $_recurringMap = [
        'MONTHLY' => 'month',
        'YEARLY' => 'year'
    ];

    public function handleEvent(Event $event): Result
    {
        if ($event instanceof Subscription) {
            \Stripe\Stripe::setApiKey($this->config['secret']);

            if (!$event[self::MACHINEPACK_CUSTOMER_ID]) {
                try {
                    $customer = \Stripe\Customer::create([
                        'description' => sprintf("%s %s <%s>", $event['Person.givenName'], $out['Person.familyName'] ?? '', $event[Stripe::EMAIL_FIELD]),
                        'email' => $event[Stripe::EMAIL_FIELD],
                        'source'  => $event[Stripe::TOKEN_FIELD],
                        'name' => sprintf("%s %s", $event['Person.givenName'], $out['Person.familyName'] ?? ''),
                        'phone' => $out['Person.telephone'] ?? ''
                    ]);
                    $customer_id = $customer['id'];
                } catch (\Exception $e) {
                    return new Failure('Unable to create customer: ' . $e->getMessage());
                }
            } else {
                $customer_id = $event[self::MACHINEPACK_CUSTOMER_ID];
            }

            try {
                $amount = round($event['MonetaryAmount.value'] * 100.0);
                $currency = $event['MonetaryAmount.currency'];
                $product = $event[Stripe::PRODUCT_ID_FIELD];
                $start_payment_date = isset($event['Product/Subscription.start.date'])?$event['Product/Subscription.start.date']:date();

                $interval = $this->_recurringMap[strtoupper($event['Order.recurring'])] ?? 'year';
                
                $sub = \Stripe\Subscription::create([
                    'customer' => "$customer_id",
                    'trial_end' => $start_payment_date,
                    'items' => [[
                      'price_data' => [
                        'unit_amount' => $amount,
                        'currency' => $currency,
                        'product' => $product,
                        'recurring' => [
                          'interval' => $interval,
                        ],
                      ],
                    ]],
                ]);

                if(!empty($sub) && ($sub->status === 'active' || $sub->status === 'trialing')){
                    $invoice = \Stripe\Invoice::retrieve($sub->latest_invoice);
                    return new Success(
                        [
                            'MoneyTransfer.identifier'                      => $sub->id,
                            'MoneyTransfer.token'                           => $event[Stripe::TOKEN_FIELD],
                            'Intangible/APIResponse.APIResponseCode'        => $sub->status,
                            'Intangible/Customer.id'                        => $sub->customer,
                            'Intangible/Payment.id'                         => isset($invoice->charge)?$invoice->charge:'',
                            'Intangible/Payment.card.last4'                 => '',
                            'Intangible/Payment.card.fingerprint'           => ''
                        ]
                    );
                  }else{
                    return new Failure(
                        'Unable to create subscription',
                        [
                            $charge
                        ]
                    );
                  }

            } catch (\Exception $e) {
                return new Failure($e->getMessage(), $e);
            }
        } else {
            return parent::handleEvent($event);
        }
    }
}
