<?php
namespace MachinePack\Core\Handler\DonationPlugin;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Payments\AuthorizationsCaptureRequest;
use PayPalCheckoutSdk\Payments\AuthorizationsGetRequest;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalHttp\HttpRequest;
use PayPalHttp\HttpException;
use MachinePack\Core\Event\Event;
use MachinePack\Core\Event\Events\Subscription;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;

class CustomPayPal extends Handler
{
    /**
     * Convert PayPal errors to something standard
     *
     * @param Exception $exception
     * @return void
     */
    private function _processFailure($exception): Failure {
        $data = json_decode($exception->getMessage(), true);
        $data['statusCode'] = $exception->statusCode;
        $reason = 'Unknown';
        $data['continue'] = false;
        if ($data['name'] == 'UNPROCESSABLE_ENTITY'
            && (($data['details'][0]['issue'] ?? null) == 'AUTHORIZATION_ALREADY_CAPTURED')
        ) {
            $reason = 'Already captured';
            $data['continue'] = true;
        }
        return new Failure($reason, $data);
    }

    /**
     * Single payment capture
     *
     * @param PayPalHttpClient $client
     * @param Event $event
     * @return void
     */
    private function _handleOnce(PayPalHttpClient $client, Event $event) {
        $code = $event['Intangible/PayPal.paymentId'];
        try {
            $request = new OrdersGetRequest($code);
            $response = $client->execute($request);
            return new Success($response);
        } catch (\Exception $ex) {
            // bad authorization given
            return $this->_processFailure($ex);
        }

        // for authorization only mode
        // 1. fetch Authorization
        // try {
        //     $request = new AuthorizationsGetRequest($code);
        //     $response = $client->execute($request);
        // } catch (\Exception $ex) {
        //     // bad authorization given
        //     return $this->_processFailure($ex);
        // }

        // if ($response->result->status == 'CAPTURED') {
        //     return new Success('Already captured.');
        // }

        // // 2. capture Authorization
        // try {
        //     $request = new AuthorizationsCaptureRequest($code);
        //     $response = $client->execute($request);
        // } catch (\Exception $ex) {
        //     return $this->_processFailure($ex);
        // }

        return new Success($response);
    }


    /**
     * When using v2, recurring payments are done by creating the
     * product first, then passing it to the frontend handler
     *
     * @param PayPalHttpClient $client
     * @param Event $event
     * @return void
     */
    private function _handlePrepareRecurring(PayPalHttpClient $client, Event $event) {
        try {
            // return new Success($event->getData());
            $request = new CustomPayPalPlanCreateRequest($event);
            // return new Success($request->data);
            $response = $client->execute($request);
            return new Success($response);
        } catch (HttpException $ex) {
            $message = json_decode($ex->getMessage(), true);

            if (! (
                @$message['name'] == 'RESOURCE_NOT_FOUND'
                && @$message['details'][0]['issue'] == 'INVALID_RESOURCE_ID'
                && @$message['details'][0]['description'] == 'Invalid product id'
            )) {
                return $this->_processFailure($ex);
            }
        } catch (\Exception $ex) {
            return $this->_processFailure($ex);
        }

        // Product not here yet, create and retry
        try {
            $request = new CustomPayPalProductCreateRequest($event);
            $response = $client->execute($request);
        } catch (\Exception $ex) {
            // something is wrong, fail
            return $this->_processFailure($ex);
        }
        // Retry creating plan
        try {
            $request = new CustomPayPalPlanCreateRequest($event);
            $response = $client->execute($request);
            return new Success($response);
        } catch (\Exception $ex) {
            // Give up
            return $this->_processFailure($ex);
        }
    }

    /**
     * @see Handler\handleEvent
     *
     * @param Event $event
     * @return void
     */
    public function handleEvent(Event $event): Result
    {
        $environment = new SandboxEnvironment($this->config['client_id'], $this->config['secret']);
        $client = new PayPalHttpClient($environment);

        if (strtolower($event['Order.recurring']) == 'once') {
            return $this->_handleOnce($client, $event);
        }
        if (strtolower($event['Order.recurring']) == 'monthly' && $event instanceof Subscription) {
            return $this->_handlePrepareRecurring($client, $event);
        }
    }
}
