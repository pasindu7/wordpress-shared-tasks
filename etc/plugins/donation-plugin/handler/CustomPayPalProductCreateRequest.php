<?php
namespace MachinePack\Core\Handler\DonationPlugin;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Payments\AuthorizationsCaptureRequest;
use PayPalCheckoutSdk\Payments\AuthorizationsGetRequest;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalHttp\HttpRequest;
use MachinePack\Core\Event\Event;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;

class CustomPayPalProductCreateRequest extends HttpRequest
{

    function __construct(Event $event)
    {
        parent::__construct('/v1/catalogs/products', 'POST');
        $this->headers['Content-Type'] = 'application/json';

        $product_id = $event['Product.productId'];
        $product_name = $event['Product.name'] ?? 'Donation';
        $category = 'NONPROFIT'; // CHARITY ?

        $this->data = [
            'id' => $product_id,
            'name' => $product_name,
            'type' => 'SERVICE',
            'category' => $category,
        ];

        if (!empty($event['Product.imageUrl'])) {
            $this->data['image_url'] = $event['Product.imageUrl'];
        }
        if (!empty($event['Product.homeUrl'])) {
            $this->data['home_url'] = $event['Product.homeUrl'];
        }

        $this->body = json_encode($this->data);
    }
}
