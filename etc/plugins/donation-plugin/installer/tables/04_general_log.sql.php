CREATE TABLE `<?php echo $table_prefix; ?>general_log` (
	id BIGINT(9) NOT NULL AUTO_INCREMENT,
	created_at datetime DEFAULT NOW() NOT NULL,
	details JSON,
	PRIMARY KEY  (id)
) <?php echo $charset_collate; ?> ENGINE=INNODB
