CREATE TABLE `<?php echo $table_prefix; ?>transaction` (
	id BIGINT(9) NOT NULL AUTO_INCREMENT,
	created_at datetime DEFAULT NOW() NOT NULL,
	updated_at datetime DEFAULT NOW() NOT NULL ON UPDATE NOW(),
	identifier VARCHAR(255),
	contact_id BIGINT(9),
	product_id BIGINT(9),
	details JSON,
	PRIMARY KEY  (id),
	UNIQUE KEY identifier (identifier),
	FOREIGN KEY contact (contact_id) REFERENCES `<?php echo $table_prefix; ?>contact` (id) ON DELETE SET NULL,
	FOREIGN KEY product (product_id) REFERENCES `<?php echo $table_prefix; ?>product` (id) ON DELETE SET NULL
) <?php echo $charset_collate; ?> ENGINE=INNODB
