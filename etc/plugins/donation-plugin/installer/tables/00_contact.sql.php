CREATE TABLE `<?php echo $table_prefix; ?>contact` (
	id bigint(9) NOT NULL AUTO_INCREMENT,
	created_at datetime DEFAULT NOW() NOT NULL,
	updated_at datetime DEFAULT NOW() NOT NULL ON UPDATE NOW(),
	identifier VARCHAR(255),
	details JSON,
	PRIMARY KEY  (id),
	UNIQUE KEY identifier (identifier)
) <?php echo $charset_collate; ?> ENGINE=INNODB
