<?php

if ( ! defined( 'WPINC' ) ) {
	die;
}

global $wpdb;

require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

$installer_config = [
	'prefix' => require __DIR__ . '/table_prefix.php',
	'version' => '1.0'
];

$charset_collate = $wpdb->get_charset_collate();
$table_prefix = $installer_config['prefix'];

$schemas = glob ( __DIR__ . '/tables/*.sql.php' );
natsort($schemas);

foreach ($schemas as $schema_file ) {
    ob_start();
	include $schema_file;
	$sql = ob_get_clean();

    if ( WP_DEBUG ) {
        error_log("dbDelta: $schema_file: $sql");
    }
	dbDelta( $sql );
}

add_option( 'gutenberg_donations_plugin_db_version', $installer_config['version'] );
add_option( 'gutenberg_donations_plugin_settings', file_get_contents(__DIR__ . '/settings_default.yml') );

if (empty(get_option('gutenberg_donations_plugin_machinepack_config'))) {
    add_option( 'gutenberg_donations_plugin_machinepack_config', file_get_contents(__DIR__ . '/machinepack_default.yml') );
}
// die('fail');
