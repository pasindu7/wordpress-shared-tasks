/**
	* Various table utilities
	*/
export default class TableFormatter {
	/**
		* Custom labels for known columns
		*/
	private static labels = {
		id: "ID",
		created_at: "Created",
		updated_at: "Updated"
	}
	/**
		* Takes a label and makes it into a readable string
		* @param label string property name
		*/
	static processLabel(label: string) {
		return TableFormatter.labels[label] || label.replace(/[^a-zA-Z0-9]/g, " ");
	}

	/**
		* Takes a list of objects and unwraps the details column,
		* returning all column names in this data set
		* @param data Array<Object> data from an API
		*/
	static processTable(data) {
		const columns = Object.keys(
			data.reduce((map, row) => {
				Object.assign(map, row);
				Object.assign(map, row.details);
				delete map.details;
				return map;
			}, {})
		);

		data.forEach(d => Object.assign(d, d.details));

		const rows = data;
		return [columns, rows]
	}
}

