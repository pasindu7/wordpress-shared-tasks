#!/bin/bash -x

base_dir=$(pwd)
apache_log=/tmp/build.log

wp --allow-root hello

echo "127.0.0.1 local.leafcutter.com.au" >> /etc/hosts
export WP_INSTALL_URL="http://local.leafcutter.com.au/"

# start server
apache2-foreground 2>&1 >$apache_log &
PID=$!

# copy code to apache dir
mkdir -p /app
rsync -a $base_dir/ /app/

## init from template
cd /app
./task/init.sh
IN_DOCKER=1 wp --allow-root --path="./public" init
cd $base_dir

## copy any extras from existing repo
for dir in /public/wp-content/themes /public/wp-content/plugins
do
	if test -d "$base_dir/$dir"
	then
		rsync -av --checksum $base_dir/$dir/ /app/$dir/
	fi
done

# run any build step after sync
if test -x "$base_dir/bin/pipelines_build.sh"
then
	(cd /app; "$base_dir/bin/pipelines_build.sh")
fi

chown -R 33:33 /app/public

# run tests
cd /app
./task/test.sh
TEST_RESULT=$?

# cleanup
kill $PID || true

# if test -f "$apache_log"
# then
# 	cat "$apache_log" || true 
# fi
echo "TEST RESULT: $TEST_RESULT"

exit $TEST_RESULT
