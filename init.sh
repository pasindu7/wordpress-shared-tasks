#!/bin/bash -eux

INSTALL_URL=${WP_INSTALL_URL:-http://localhost/}
INSTALL_TITLE=${WP_INSTALL_TITLE:-Leafcutter}
INSTALL_ADMIN_USER=${WP_INSTALL_ADMIN_USER:-root}
INSTALL_ADMIN_PASSWORD=${WP_INSTALL_ADMIN_PASSWORD:-root}
INSTALL_ADMIN_EMAIL=${WP_INSTALL_ADMIN_EMAIL:-root@example.com}

DB_NAME=${WP_DB_NAME:-wp}
DB_USER=${WP_DB_USER:-root}
DB_PASS=${WP_DB_PASS:-root}
DB_HOST=${WP_DB_HOST:-127.0.0.1}

export WP_CLI_CACHE_DIR=/wp-cli-cache/

rm -rf /wp-cli-cache/*

cd /app

n=0
until [ $n -ge 5 ]
do
	mysql -u${DB_USER} -p${DB_PASS} -h${DB_HOST} --protocol=TCP -e "CREATE DATABASE IF NOT EXISTS ${DB_NAME}" && break

	n=$[$n+1]
	sleep 15
done

if test ! -f "./public/index.php"
then
    wp --allow-root \
        --path="./public" \
        core download \
        --force
fi

if test ! -f "./public/index.php"
then
    echo "Can't download wordpress. Exiting."
    exit 1
fi

if test ! -f "./public/wp-config.php"
then
    wp --allow-root \
        --path="./public" \
        config create \
        --dbname="${DB_NAME}" \
        --dbuser="${DB_USER}" \
        --dbpass="${DB_PASS}" \
        --dbhost="${DB_HOST}" \
        --force 
fi

wp --allow-root \
    --path="./public" \
    core install \
    --url="$INSTALL_URL" \
    --title="$INSTALL_TITLE" \
    --admin_user="$INSTALL_ADMIN_USER" \
    --admin_password="$INSTALL_ADMIN_PASSWORD" \
    --admin_email="$INSTALL_ADMIN_EMAIL" \
    --skip-email

wp --allow-root \
    --path="./public" \
    language core install en_AU

wp --allow-root \
    --path="./public" \
    site switch-language en_AU

# now included in default docker image
# php -d memory_limit=-1 "$(which wp)" --allow-root \
#     package install https://github.com/wpbullet/wp-menu-import-export-cli.git

wp --allow-root \
    --path="./public" \
    plugin install wordpress-importer --activate

cat << EOF > ./public/.htaccess
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews
    </IfModule>

    RewriteEngine On

    # Prevent access to sensitive files
    RedirectMatch 404 \.(yml|md|sh|sql|ini|lock)$

    # Prevent access to hidden files
    RedirectMatch 404 /\..*$

    # Handle Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>
EOF

mkdir -p /app/public/wp-content/uploads

if test ! -z "${FS_CHMOD_DIR:-}"
then
    find /app/public -type d -exec chmod -R "$FS_CHMOD_DIR" /app/public {} \;
fi
if test ! -z "${FS_CHMOD_FILE:-}"
then
    find /app/public -type f -exec chmod -R "$FS_CHMOD_FILE"
fi
if test ! -z "${FS_CHOWN:-}"
then
    chown -R "$FS_CHOWN" /app/public
fi
chown -R www-data:www-data /app/public/wp-content/uploads