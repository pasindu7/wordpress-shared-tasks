<?php

return [
	// silent
	[
		'return' => 'all',
		'exit_error' => false
	],
	[
		'return' => true,
		'parse' => 'json'
	],
	// default
	[]
];