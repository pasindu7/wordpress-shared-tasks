<?php 
/**
 * Make a snapshot of the leafcutter template site
 *
 * @when before_wp_load
 */
$cmd = function( $args, $assoc_args ) {
	if (!empty($assoc_args['snapshot-dir']) && is_dir($assoc_args['snapshot-dir'])) {
		$snapshot_dir = $assoc_args['snapshot-dir'];
	} else {
		$snapshot_dir = realpath(__DIR__ . '/../../etc/');
	}

	if (!empty($assoc_args['source'])) {
		$source = $assoc_args['source'];
	} else {
		$source = '@template';
	}

	require_once __DIR__ . '/../rsync.php';

	@mkdir($snapshot_dir);

	foreach ([
		'template_plugins.json' => $source . ' plugin list --format=json',
		'template_themes.json' => $source . ' theme list --format=json',
		'template_options.json' => $source . ' option list --format=json',
		'template_data.xml' => $source . ' export --stdout',
	] as $file => $cmd) {
		file_put_contents($snapshot_dir . '/' . $file, WP_CLI::runcommand($cmd, ['return' => true]));	
		WP_CLI::line("Wrote $file");
	}

	// rsync
	$aliases = WP_CLI::get_configurator()->get_aliases();

	if (empty($aliases[$source]['ssh'])) {
		WP_CLI::warning("Can't find $source in your yml file, please check it exists and has ssh settings");
		return;
	}

	$base = $aliases[$source]['ssh'];

	// menus don't support stdout
#	WP_CLI::runcommand($source . ' menu export --all --filename=template_menus.json');
#	rsync_run("$base/template_menus.json", "$snapshot_dir/template_menus.json");

	@mkdir($snapshot_dir . '/themes');
	rsync_run("$base/wp-content/themes", "$snapshot_dir/");
	WP_CLI::line('Wrote theme folder snapshot');

	@mkdir($snapshot_dir . '/plugins');
	rsync_run("$base/wp-content/plugins", "$snapshot_dir/");
	WP_CLI::line('Wrote plugin folder snapshot');

	WP_CLI::success('Snapshot complete');
};

WP_CLI::add_command( 'template-snapshot', $cmd);
