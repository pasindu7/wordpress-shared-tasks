<?php
require_once __DIR__ . '/base-command.php';
class LeafcutterResetCommand extends BaseCommand {
	private function output_handler($output ) {
		return array_reduce($output, function ($c, $i) {
			return $c . trim(str_replace(["Warning: WP_CLI_SSH_PRE_CMD found, executing the following command(s) on the remote machine:", 'export PATH=$HOME/bin:$PATH'], "", $i->stderr)) . PHP_EOL;
		}, '');
	}

	/**
	 * Set up and copy theme data from template
	 */
	private function processThemes() {
		$themes = json_decode(file_get_contents($this->snapshot_dir . '/template_themes.json'));
		$progress = WP_CLI\Utils\make_progress_bar('1. Processing themes', count($themes) + 1);
		$progress->tick();

		$output = [];
		foreach ($themes as $theme) {
			if (
				is_file("{$this->sync_dir}/wp-content/themes/{$theme->name}/style.css")
				|| is_file("{$this->sync_dir}/wp-content/themes/{$theme->name}/functions.php")
			) {
				WP_CLI::line("\nTheme '{$theme->name}' is committed locally, skipping folder sync");
			} else {
				rsync_run(
					"{$this->snapshot_dir}/themes/{$theme->name}", 
					"{$this->sync_dir}/wp-content/themes/",
					$this->verbose,
					true,
					true,
					$this->is_local
				);
			}

			$progress->tick();
			if ($theme->status == 'active') {
				$output[] = WP_CLI::runcommand(sprintf('%s theme activate %s', $this->env, $theme->name), $this->wpcli_silent);
			}
		}
		$progress->finish();
		WP_CLI::line("Messages: \n" . $this->output_handler($output));
	}

	/**
	 * Set up plugins / enable
	 */
	private function processPlugins() {
		$output = [];

		$plugins = json_decode(file_get_contents($this->snapshot_dir . '/template_plugins.json'));
		$progress = WP_CLI\Utils\make_progress_bar('2. Processing plugins', count($plugins) + 1);
		$progress->tick();
		WP_CLI::line('2. Processing plugins');
		ob_start();
		rsync_run(
			"{$this->snapshot_dir}/plugins/",
			"{$this->sync_dir}/wp-content/plugins/",
			$this->verbose,
			true,
			false,
			$this->is_local
		);
		ob_end_clean();
		foreach ($plugins as $plugin) {
			$progress->tick();
			if ($plugin->status == 'active') {
				$output[] = WP_CLI::runcommand(sprintf('%s plugin activate %s', $this->env, $plugin->name), $this->wpcli_silent);
			}
		}
		$progress->finish();
	
		WP_CLI::line("Messages: \n" . $this->output_handler($output));	
	}

	/**
	 * migrate options based on a blacklist
	 */
	private function processOptions() {
		$remap = function ($src) {
			$out = [];
			if (!empty($src)) {
				foreach ($src as $i) {
					$out[$i['option_name']] = $i['option_value'];
				}
			}
			return $out;
		};
		
		$options = $remap(json_decode(file_get_contents($this->snapshot_dir . '/template_options.json'), true));
		$progress = WP_CLI\Utils\make_progress_bar('3. Importing options', count($options), 5);
		$blacklist = [
			'siteurl', 'home', 'auth_key', 'auth_salt', 'logged_in_key', 'logged_in_salt', 'nonce_key', 'nonce_salt'
		];
		$current = WP_CLI::runcommand(sprintf('%s option list --format=json', $this->env), $this->wpcli_silent);
		$current = $remap(json_decode($current->stdout, true));
		foreach ($options as $option => $value) {
			$progress->tick();
			if (
				in_array($option, $blacklist)
				) {
					continue;
				}
				
				if (($opts = @unserialize($value)) !== false) {
					$value = $opts;
				}
				$value = json_encode($value);
				
				if (isset($current[$option]) && json_encode($current[$option]) == $value) {
					continue;
				}
				
			/**
			 * XXX known issue: whitespace doesn't work, so have to use pipe
			 * XXX update when issues below are fixed
			 * @see https://github.com/wp-cli/wp-cli/issues/1928
			 */
			$out = shell_exec($cmd = sprintf(
				'echo %s | '.
					'%s -r \'echo base64_decode(file_get_contents("php://stdin"));\' | '.
					'%s %s %s --allow-root --path=%s option update %s --format=json 2>&1',
				escapeshellarg(base64_encode($value)), 
				WP_CLI::get_php_binary(),
				WP_CLI::get_php_binary(),
				$_SERVER['PHP_SELF'],
				$this->env,
				$this->target_dir,
				$option
			));
			if ($this->verbose) {
				wp_cli::line($out);
			}
		}
		$progress->finish();
	}

	/**
	 * Copy example content from template
	 */
	private function processContent() {
		WP_CLI::line('4. Processing data');
		rsync_run(
			"{$this->snapshot_dir}/template_data.xml",
			"{$this->sync_dir}/template_data.xml",
			$this->verbose,
			false,
			false,
			$this->is_local
		);
	
		WP_CLI::line('# deleting existing posts');
		$types = WP_CLI::runcommand(sprintf('%s post-type list --field=name --format=json --path=%s', $this->env, $this->target_dir), $this->wpcli_silent);
		$types = json_decode($types->stdout, true);
		if (!empty($types)) {
			$out = WP_CLI::runcommand(sprintf('%s post list --post-type=%s --format=ids --path=%s', $this->env, implode(',', $types), $this->target_dir), $this->wpcli_silent);
		}
		$ids = trim($out->stdout);
		if (!empty($ids)) {
			WP_CLI::runcommand(sprintf('%s post delete %s --path=%s', $this->env, $out->stdout, $this->target_dir), $this->wpcli_silent);
		}

		// XXX importer not working
		WP_CLI::line('# populating demo data');
		$cmd = sprintf(
			'%s %s %s --allow-root plugin install wordpress-importer --activate --path=%s 2>&1',
			WP_CLI::get_php_binary(),
			$_SERVER['PHP_SELF'],
			$this->env,
			$this->target_dir
		);
		passthru($cmd);

		// XXX wpenging http authentication prevents attachment download
		$http_replace_source = getenv('INIT_HTTP_SOURCE');
		$http_replace_target = getenv('INIT_HTTP_TARGET');
		if (!empty($http_replace_source) && !empty($http_replace_target)) {
			$template_data_file = sprintf('%s/template_data.xml', $this->target_dir);
			$template_data_file_contents = file_get_contents($template_data_file);
			$template_data_file_replaced = str_replace($http_replace_source, $http_replace_target, $template_data_file_contents);
			file_put_contents($template_data_file, $template_data_file_replaced);
			WP_CLI::line('http authentication added');
		}
		$cmd = sprintf('sed -io %s/template_data.xml', $this->target_dir);

		$cmd = sprintf(
			'%s %s %s --allow-root import %s/template_data.xml --authors=skip --skip-themes --path=%s 2>&1',
			WP_CLI::get_php_binary(),
			$_SERVER['PHP_SELF'],
			$this->env,
			$this->target_dir,
			$this->target_dir
		);
		passthru($cmd);

		// $cmd = sprintf('%1$s --path=%2$s import %2$s/template_data.xml --authors=skip --skip-themes', $this->env, $this->target_dir);
		// WP_CLI::runcommand($cmd);
		WP_CLI::line('');
	}

	/**
	 * Import existing menus from template
	 *
	 * @return void
	 */
	private function processMenus() {
		// wp --allow-root \
    // --path="./public" \
    // package install https://github.com/wpbullet/wp-menu-import-export-cli.git

		wp_cli::line('# Processing menus');
		wp_cli::runcommand(sprintf(
			'%s menu import %s/template_menus.json --overwrite --skip-themes --path=%s', 
			$this->env, 
			$this->snapshot_dir,
			$this->target_dir
		), $this->wpcli_silent);
	}

	/**
	 * Check local environment has all preconditions to run e.g. wpconfig, public folder
	 *
	 * @return void
	 */
	protected function processPreconditions() {
		$local_env = realpath(getcwd()) . '/.env';
		if (!is_file($local_env)) {
			$msg = '# Missing a .env file, please confirm details below';
			$source = implode("\n", [
				$msg,
				file_get_contents(__DIR__ . '/../../.env.example'),
				'',
				'# current user information',
				'UID='.getmyuid(),
				'GID='.getmygid()
			]);
			$result = \WP_CLI\Utils\launch_editor_for_input($source, '.env');

			$output = trim(str_replace($msg, '', empty($result) ? $source : $result)) . "\n";

			file_put_contents($local_env, $output);
			WP_CLI::line('Wrote ' . WP_CLI::colorize( '%b.env%n'));
		}

		if (
			!is_dir($this->target_dir)
			|| !is_file("$this->target_dir/wp-config.php")
		) {
			$this->dockerRun('/app/task/init.sh');
		}
	}

	/**
	 * Initialise a wordpress install using the exiting template. Add -v to see progress, e.g.,
	 * leafcutter-init @prod -v
	 *
	 * <environment>
	 * : which environment to initialise. this must match your wp-cli.yaml contents
	 *
	 * @when before_wp_load
	 */
	public function exec($args, $assoc_args) {
		$this->initArgs($args, $assoc_args);
		list(
			$this->sync_dir, 
			$this->path_arg,
			$this->env,
			$this->target_dir
		) = $this->initEnvironment($this->env);

		list($wpcli_silent, $wpcli_decoded) = require_once __DIR__ . '/../wp_cli-runcommand-options.php';
		$this->wpcli_silent = $wpcli_silent;
		$this->wpcli_decoded = $wpcli_decoded;

		WP_CLI::line('Working on environment ' . WP_CLI::colorize( ($this->is_local ? '%blocal%n' : "%b{$this->env}%n") ));

		if ($this->is_local && !getenv('IN_DOCKER')) {
			$this->processPreconditions();
			$this->dockerRun('cd /app; IN_DOCKER=1 wp --allow-root --path="./public" reset local');
		} else {
			$this->processThemes();
			$this->processPlugins();
			$this->processOptions();
			$this->processContent();
			$this->processMenus();
		}
		WP_CLI::success('Done');
	}
}

// WP_CLI::add_command( 'leafcutter-init', $init );
WP_CLI::add_command( 'reset', ['LeafcutterResetCommand', 'exec']);
