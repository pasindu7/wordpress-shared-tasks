<?php
require_once __DIR__ . '/base-command.php';
class LeafcutterSyncCommand extends BaseCommand {
	/**
	 * Sync two environments. Use local for deployments.
	 * sync @stage @prod [--delete]
	 *
	 * <environment_from>
	 * : which environment to use as source
	 * <environment_to>
	 * : which environment to use as destination
	 * [--delete]
	 * : whether to delete target environment files (during rsync)
	 * 
	 * @when before_wp_load
	 */
	public function exec($args, $assoc_args) {
		$this->initArgs($args, $assoc_args);

		@list($env_from_in, $env_to_in, $rest) = $args;

		list($from_dir, $from_patharg, $from_env) = $this->initEnvironment($env_from_in);		
		list($to_dir, $to_patharg, $to_env) = $this->initEnvironment($env_to_in);		

		$this->to_local = empty($to_env);
		if ($this->to_local) {
			$to_env = 'local';
		}
		if (empty($from_env)) {
			$from_env = 'local';
		}

		WP_CLI::line(sprintf(
			'Syncing "%s" to "%s"',
			WP_CLI::colorize("%b{$from_env}%n"),
			WP_CLI::colorize("%b{$to_env}%n")
		));
	

		WP_CLI::line('Syncing files');
		$this->syncFiles($from_dir, $to_dir);
	
		if ($this->to_local) {
			WP_CLI::line('Syncing db');
			$this->syncDB($env_from_in);
		}

		WP_CLI::success('Done');
	}

	private function syncDB($source) {
		$dump = realpath(__DIR__ . '/../../../etc/') . '/latest.sql';

		$progress = WP_CLI\Utils\make_progress_bar('Syncing db', 5);

		WP_CLI::runcommand(sprintf('%s db export --allow-root - --no-tablespaces=true --add-drop-table > %s', $source, $dump));
		$progress->tick();

		
		$url = WP_CLI::runcommand(sprintf('%s option --allow-root get siteurl --format=json', $source), [
			'return' => true,
			'parse' => 'json'
		]);
		$host = @parse_url($url)['host'];
		$progress->tick();

		$this->dockerRun('wp --allow-root db import /snapshot/latest.sql');
		$progress->tick();

		if (empty($host)) {
			WP_CLI::warning('Could not determine siteurl from remote, you will need to search-replace manually.');
			return;
		}
		
		$this->dockerRun(sprintf(
			'wp --allow-root search-replace %s localhost --skip-columns=guid', 
			trim($host)
		));
		$progress->tick();

		$this->dockerRun(sprintf(
			'wp --allow-root search-replace %s %s --skip-columns=guid', 
			'https://localhost', 
			'http://localhost'
		));
		$progress->tick();

		$this->dockerRun(sprintf(
			'wp --allow-root user create root root@example.com --user_pass=root --role=administrator'
		));
		WP_CLI::warning('A local root / root account has been added for development, this account is only available locally.');
		$progress->tick();
	}

	/**
	 * User-friendly error handling for rsync
	 *
	 * @param number $result
	 * @return boolean
	 */
	private function isFatalError($result) {
		if ($result === 255) {
			wp_cli::error('Your SSH keys are missing from the remote server!');
		} if ($result === 23) {
			wp_cli::warning('Some files didn\'t transfer correctly, but should be safe to ignore');
			return false;
		} else if ($result !== 0) {
			wp_cli::error(sprintf('General rsync error "%d", please consult documentation', $result));
		}
		return $result !== 0;
	}

	private function syncFiles($from_dir, $to_dir) {
		if ($this->to_local) {
			$folders[] = 'wp-content';
		} else {
			$folders = [
				'wp-content/themes',
				'wp-content/plugins'
			];
		}

		foreach ($folders as $path) {
			$result = rsync_run(
				"$from_dir/$path/",
				"$to_dir/$path/",
				true,
				$this->rsync_delete_enabled,
				false,
				false,
				realpath(__DIR__ . '/../../../.rsyncignore')
			);

			if ($this->isFatalError($result)) {
				return;
			}
		}
	}
}

WP_CLI::add_command('sync', ['LeafcutterSyncCommand', 'exec']);
