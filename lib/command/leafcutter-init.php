<?php
require_once __DIR__ . '/base-command.php';
require_once __DIR__ . '/leafcutter-reset.php';

class LeafcutterInitCommand extends LeafcutterResetCommand {
	/**
	 * Initialise a wordpress install using the exiting template. Add -v to see progress, e.g.,
	 * leafcutter-init -v
	 * and add --sync=@environment to sync with an environment (environment must be in wp-cli.yml)
	 *
	 * @when before_wp_load
	 */
	public function exec($args, $assoc_args) {
		$sub_args = array_merge(['local', $args]);
		$sub_assoc_args = $assoc_args;
		unset($sub_assoc_args['sync']);
		$this->initArgs($sub_args, $sub_assoc_args);
		parent::exec($sub_args, $sub_assoc_args);
		if (isset($assoc_args['sync'])) {
			list(
				$sync_dir, 
				$path_arg,
				$env,
				$target_dir
			) = $this->initEnvironment($assoc_args['sync']);
			echo WP_CLI::runcommand(sprintf('sync %s local', $env), $this->wpcli_silent);
		}
	}
}

// WP_CLI::add_command( 'leafcutter-init', $init );
WP_CLI::add_command( 'init', ['LeafcutterInitCommand', 'exec']);
