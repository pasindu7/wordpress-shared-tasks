<?php 
/**
 * Simple sanity check for wp integration
 *
 * @when before_wp_load
 */
$cmd = function( $args, $assoc_args ) {
	require_once __DIR__ . '/../rsync.php';

	$aliases = WP_CLI::get_configurator()->get_aliases();
	if (empty($aliases)) {
		WP_CLI::error('Missing aliases in wp-cli.yml. Create for example @prod: and @stage: keys');
		return;
	}

	if (empty(rsync_version())) {
		WP_CLI::error('Missing rsync from your local install.');
		return;
	}

	WP_CLI::success('Leafcutter WordPress tools seem to be in order');
};

WP_CLI::add_command('hello', $cmd);