<?php 
/**
 * Make a snapshot of a site on a local ./etc folder 
 * Uses db dump + plugins + themes only
 *
 * @when before_wp_load
 */
$cmd = function( $args, $assoc_args ) {
	if (!empty($assoc_args['snapshot-dir']) && is_dir($assoc_args['snapshot-dir'])) {
		$snapshot_dir = $assoc_args['snapshot-dir'];
	} else {
		$snapshot_dir = realpath(__DIR__ . '/../../../etc/');
	}

	if (!empty($assoc_args['source'])) {
		$source = $assoc_args['source'];
	} else {
		$source = '@development';
	}

	require_once __DIR__ . '/../rsync.php';

	@mkdir($snapshot_dir);

	// rsync
	$aliases = WP_CLI::get_configurator()->get_aliases();

	if (empty($aliases[$source]['ssh'])) {
		WP_CLI::warning("Can't find $source in your yml file, please check it exists and has ssh settings");
		return;
	}

	$base = $aliases[$source]['ssh'];

	$progress = WP_CLI\Utils\make_progress_bar('Snapshotting data', 4);

	$progress->tick();

	WP_CLI::runcommand(sprintf('%s db export - --no-tablespaces=true --add-drop-table > %s/latest.sql', $source, $snapshot_dir));

	$progress->tick();

	@mkdir($snapshot_dir . '/themes');
	rsync_run("$base/wp-content/themes", "$snapshot_dir/");
	WP_CLI::line('Wrote theme folder snapshot');

	$progress->tick();

	@mkdir($snapshot_dir . '/plugins');
	rsync_run("$base/wp-content/plugins", "$snapshot_dir/");
	WP_CLI::line('Wrote plugin folder snapshot');

	$progress->tick();

	WP_CLI::success('Snapshot complete');
};

WP_CLI::add_command( 'snapshot', $cmd);
