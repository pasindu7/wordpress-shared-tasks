<?php
class BaseCommand {
	protected $snapshot_dir;
	protected $base_dir;
	protected $env;
	protected $verbose;
	protected $wpcli_silent;
	protected $wpcli_decoded;
	protected $target_dir;
	protected $is_local;
	protected $path_arg;

	protected $rsync_delete_enabled;

	protected $platform_mingw = false;

	protected function initArgs($args, $assoc_args) {
		@list($env, $rest) = $args;
		require_once __DIR__ . '/../rsync.php';

		$platinfo = @shell_exec('uname -a');
		$this->platform_mingw = preg_match('/MINGW64/', $platinfo);

		$this->verbose = !empty($verbose);
		if (!empty($assoc_args['snapshot-dir']) && is_dir($assoc_args['snapshot-dir'])) {
			$this->snapshot_dir = $assoc_args['snapshot-dir'];
		} else {
			$this->snapshot_dir = realpath(__DIR__ . '/../../etc/');
		}

		$this->rsync_delete_enabled = isset($assoc_args['delete']);

		$this->base_dir = realpath(__DIR__ . '/../../../');
		$this->env = $env;
		$this->is_local = $env === 'local';
	}

	protected function initEnvironment($env) {
		if ($env !== 'local') {
			$aliases = WP_CLI::get_configurator()->get_aliases();
			if (!isset($aliases[$env]['ssh'])) {
				WP_CLI::error_multi_line([
					'Environment not set up in wp-cli.yml.',
					'Make sure you set up your environment as:',
					'',
					'@stage:',
					'  ssh: username@host-stage:/path/to/public/',
					'@prod:',
					'  ssh: username@host:/path/to/public/',
					'',
				]);
				exit(1);
			}
			return [
				$aliases[$env]['ssh'],
				'',
				$env,
				explode(':', $aliases[$env]['ssh'])[1],
			];
		} else {
			return [
				realpath("{$this->base_dir}/public"),
				realpath("{$this->base_dir}/public"),
				'',
				realpath("{$this->base_dir}/public"),
			];
		}
	}

	/**
	 * Runs this command inside the web container
	 *
	 * @param string $cmd
	 * @return void
	 */
	protected function dockerRun($cmd) {
		passthru(sprintf(
			'%sdocker-compose run --rm -v $(pwd)/etc:/snapshot/ www bash -c %s',
			$this->platform_mingw ? 'winpty ' : '',
			escapeshellarg($cmd)
		));
	}
}
