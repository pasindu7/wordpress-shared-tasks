<?php 
/**
 * TODO uname -a : MINGW64 
 * 
 */

/**
 * TODO:
 * - add support for windows
 * - add support for faulty hosts like:
 * -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" 
 *
 * @param string $src source path to rsync
 * @param string $dst destination path to rsync
 * @param boolean $verbose
 * @param boolean $delete
 * @param boolean $ignore_existing
 * @param boolean $is_local
 * @return void
 */
function rsync_run($src, $dst, $verbose = false, $delete = false, $ignore_existing = false, $is_local = false, $exclude_file = null) {
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
		$rsync_cmd = __DIR__ . '/cwrsync.cmd';
	} else {
		$rsync_cmd = 'rsync';
	}
	if (!$is_local) {
		$env_key = getenv('WP_CLI_SYNC_KEY');
		$env_key_decoded = @base64_decode($env_key);

		$use_deploy_key = false;
		if (!empty($env_key_decoded)) {
			$key_file = tempnam(getcwd(), 'ssh_deploy_key');
			file_put_contents($key_file, $env_key_decoded);
			@chmod("$key_file", 0600);
			$use_deploy_key = true;
			if ($verbose) {
				error_log("Rsync: using deploy key from WP_CLI_SYNC_KEY (at '$key_file')");
			}
		}

		$cmd = sprintf(
			'%s -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null %s" -a --checksum%s',
			$rsync_cmd,
			$use_deploy_key ? '-i ' . escapeshellarg($key_file) : '',
			is_file($exclude_file) ? ' --exclude-from ' . escapeshellarg($exclude_file) : ''
		);
		if ($verbose && is_file($exclude_file)) {
			error_log('Rsync: using exclude file '.$exclude_file);
		}
	} else {
		$cmd = $rsync_cmd;
	}
	$result = null;
	if (!$verbose) {
		ob_start();
	}

	$exec = sprintf(
		'%s %s %s %s', 
		$cmd,
		implode(' ', array_filter([
			empty($verbose) ? '' : '-v',
			empty($delete) ? '' : '-r --delete',
			empty($ignore_existing) ? '' : '--ignore-existing',
		])),
		escapeshellarg($src),
		escapeshellarg($dst)
	);

	passthru($exec, $result);
	if (!$verbose) {
		ob_end_clean();
	}

	if (!empty($key_file) && is_file($key_file)) {
		@unlink($key_file);
	}

	return $result;
};

/**
 * Return the rsync version if it runs at all
 * XXX windows
 */
function rsync_version() {
	return shell_exec('rsync --version');
}

/**
 * Attempts to fix bad colon in path issue in rsync
 *
 * @param string $path
 * @return string
 */
function rsync_normalise_path($path) {
	$real = realpath($path);
	if (
		preg_match('/^([a-zA-Z])\:/', $real, $matches)
		|| preg_match('/^([a-zA-Z])\:/', $real, $matches)
	) {
		$cwd = realpath(getcwd());
		return  '/cygdrive/' . str_replace(
			[':\\', '\\'],
			['/', '/'], 
			substr($real, strlen($cwd . DIRECTORY_SEPARATOR))
		);
	}
	return $real;
}
