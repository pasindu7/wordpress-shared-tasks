#!/bin/bash -eux

# - git submodule set-url task git@bitbucket.org:leafcuttercreativedigital/wordpress-shared-tasks.git
sed -i 's~../wordpress-shared-tasks.git~git@bitbucket.org:leafcuttercreativedigital/wordpress-shared-tasks.git~' .gitmodules

ssh-agent bash -c 'ssh-add ./task/ssh/id_rsa; git submodule update --init'

