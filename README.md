# WordPress tasks

## About

This repo contains several tasks that extend `wp-cli`. To use them, add a `wp-cli.yml` file to the root of your project, then include the provided yml file:

```yaml
# base extension config
_:
  inherit: path-to-this-repo/wp-cli-leafcutter.yml
# environments
@prod:
  ssh: user@server:/home/wpe-user/sites/yoursite
@stage:
  ssh: user@server:/home/wpe-user/sites/yoursitestage
```

Then whenever you use `wp` in this your project's folder, the tasks will automatically show.

## Install

There is no install procedure, apart from having a working shell with PHP and [wp-cli](https://wp-cli.org/).
You can install it using the [instructions provided on the website](https://make.wordpress.org/cli/handbook/installing/), or run this small script below.

```bash
#!/bin/bash -eux

curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
```

## Environment variables

All operations are done using `rsync`. In case you'd like to override the SSH key used, define an environment
variable called `WP_CLI_SYNC_KEY` with the base64-encoded contents of the desired private key.

For more info check [guide in bitbucket](https://support.atlassian.com/bitbucket-cloud/docs/use-ssh-keys-in-bitbucket-pipelines/).

1. create a key
```
ssh-keygen -t rsa -b 4096 -N '' -f wp_cli_sync_key -C template_deploy_key
base64 -w 0 < wp_cli_sync_key
```
2. add key in wpengine (must have a user associated with it, not just git push)
3. set `WP_CLI_SYNC_KEY` in bitbucket pipelines to value in 1. 


## Notes

If your source site is not `@template`, you might need to install some packages.
Access your site remotely via ssh, then run wp to install these:

```
wp package install https://github.com/wpbullet/wp-menu-import-export-cli.git
```

Or if your server has memory issues:
```
php -d memory_limit=512M "$(which wp)" package install https://github.com/wpbullet/wp-menu-import-export-cli.git
```
