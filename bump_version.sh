#!/bin/bash

usage() {
	echo "Usage: $0 [-v M.m.p] [-M] [-m] [-p]" 1>&2; 
	exit 1; 
}

BUMP_TYPE=patch

while getopts ":v:Mmp" o; do
	case "${o}" in
		v)
			BUMP_VERSION=${OPTARG}
		;;
		M)
			BUMP_TYPE=major
		;;
		m)
			BUMP_TYPE=minor
		;;
		p)
			BUMP_TYPE=patch
		;;
		*)
			usage
		;;
	esac
done
shift $((OPTIND-1))

CURRENT_VERSION=$(git tag | sort --version-sort | tail -1 | sed 's/^v//')
MAJOR=$(echo $CURRENT_VERSION | cut -f1 -d\.)
MINOR=$(echo $CURRENT_VERSION | cut -f2 -d\.)
PATCH=$(echo $CURRENT_VERSION | cut -f3 -d\.)

if test ! -z "$BUMP_VERSION"
then
	NEW_VERSION="$BUMP_VERSION"
elif test "${BUMP_TYPE}" == "major"
then
	MAJOR=$(($MAJOR + 1))
	NEW_VERSION="${MAJOR}.0.0"
elif test "${BUMP_TYPE}" == "minor"
then
	MINOR=$(($MINOR + 1))
	NEW_VERSION="${MAJOR}.${MINOR}.0"
else
	PATCH=$(($PATCH + 1))
	NEW_VERSION="${MAJOR}.${MINOR}.${PATCH}"
fi

if test $? -eq 0
then
	git tag "v$NEW_VERSION"
	echo "Bumped plugin from $CURRENT_VERSION to $NEW_VERSION"
else
	echo "FAIL"
fi
